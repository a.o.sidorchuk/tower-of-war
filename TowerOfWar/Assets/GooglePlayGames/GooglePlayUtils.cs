﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using Newtonsoft.Json;
using System;
using System.Text;
using UniRx.Async;
using UnityEngine;

public class GooglePlayUtils
{
    public static async UniTask GooglePlayServicesConnect()
    {
        GooglePlayServicesInit();

        var process = true;

        PlayGamesPlatform.Instance.Authenticate(SignInInteractivity.CanPromptOnce, (result) =>
        {
            Debug.Log($"<b>[GooglePlayUtils]</b> - Authenticate Status - {result}");
            process = false;
        });

        while (process) await UniTask.Yield();
    }

    private static void GooglePlayServicesInit()
    {
        var config = new PlayGamesClientConfiguration.Builder()
            .EnableSavedGames()
            .RequestServerAuthCode(false)
            .RequestIdToken()
            .Build();

        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
    }

    public static async UniTask SaveData<T>(T data, string fileName, TimeSpan totalPlaytime)
    {
        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            var process = true;
            var savedGameClient = PlayGamesPlatform.Instance.SavedGame;

            savedGameClient.OpenWithAutomaticConflictResolution(fileName, DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, (status, game) =>
            {
                if (status == SavedGameRequestStatus.Success)
                {
                    var metaData = new SavedGameMetadataUpdate.Builder()
                        .WithUpdatedPlayedTime(totalPlaytime)
                        .WithUpdatedDescription("Saved game at " + totalPlaytime)
                        .Build();

                    var dataJson = JsonConvert.SerializeObject(data);
                    var dataBytes = Encoding.Default.GetBytes(dataJson);

                    savedGameClient.CommitUpdate(game, metaData, dataBytes, (s, m) => { });
                }

                process = false;
            });

            while (process) await UniTask.Yield();
        }
    }

    public static async UniTask<T> ReadData<T>(string fileName)
    {
        var json = "";

        if (PlayGamesPlatform.Instance.IsAuthenticated())
        {
            var process = true;

            PlayGamesPlatform.Instance.SavedGame.OpenWithAutomaticConflictResolution(fileName, DataSource.ReadCacheOrNetwork, ConflictResolutionStrategy.UseLongestPlaytime, (status, game) =>
            {
                if (status == SavedGameRequestStatus.Success)
                {
                    ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
                    savedGameClient.ReadBinaryData(game, (SavedGameRequestStatus readStatus, byte[] data) =>
                    {
                        if (readStatus == SavedGameRequestStatus.Success)
                        {
                            json = Encoding.Default.GetString(data);
                        }

                        process = false;
                    });
                }
                else
                {
                    process = false;
                }
            });

            while (process) await UniTask.Yield();
        }

        return string.IsNullOrEmpty(json) ? default : JsonConvert.DeserializeObject<T>(json);
    }
}
