﻿using Data.Models.Economy;
using Data.Models.Level;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Data.Models.Player
{
    public enum FractionType
    {
        /// <summary>Все неигровые юниты</summary>
        None,
        /// <summary>Орда</summary>
        H,
        /// <summary>Альянс</summary>
        A
    }

    #region отряды игрока
    /// <summary> Единица отряда (хранит Id собранных юинтов в виде списка) </summary>
    public class SquadItem
    {
        /// <summary> список unitId данного отряда </summary>
        [JsonProperty] public List<int> Units { get; private set; } = new List<int>();
        /// <summary> количество юнитов в отряде </summary>
        [JsonIgnore] public int Count => Units.Count;

        public SquadItem(List<int> units) => Units = units;

        public void SetSquadUnits(List<int> newUnits) => Units = newUnits;
    }

    #endregion

    #region фракции
    /// <summary> Данные о прогрессе выбранной фрации </summary>
    public class FractionProgress
    {
        /// <summary> Индекс выбранного отряда </summary>
        [JsonProperty] public int CurrentSquadId { get; private set; }

        /// <summary> Все отряды игрока </summary>
        [JsonProperty] public List<SquadItem> SquadList { get; private set; } = new List<SquadItem>();

        /// <summary> Текущий отряд игрока </summary>
        [JsonIgnore] public SquadItem CurrentSquad => SquadList.Count > 0 ? SquadList[CurrentSquadId] : null;

        /// <summary> Получить подробный список юнитов в текущей команде </summary>
        [JsonIgnore] public Dictionary<int, int> CurrentSquadUnits
        {
            get
            {
                if (SquadList.Count > 0 && OpenedUnits.Count > 0)
                {
                    var currentSquadUnits = CurrentSquad.Units;
                    return OpenedUnits.Where(x => currentSquadUnits.Any(uid => x.Key == uid)).ToDictionary(x => x.Key, x => x.Value);
                }

                return new Dictionary<int, int>();
            }
        }

        [JsonIgnore] public bool GetCurrentSquadSize => true;//SquadList.Count > 0 ? SquadList[CurrentSquadId].Count : 0;

        /// <summary> Открытые юниты в коллекции и их уровни </summary>
        /// <remarks> { unitId, unitLevel } </remarks>
        [JsonProperty] public Dictionary<int, int> OpenedUnits { get; private set; } = new Dictionary<int, int>();

        /// <summary> Прогресс прохождения подземелий </summary>
        [JsonProperty] public List<LevelSaveData> CompleteLevelData { get; private set; } = new List<LevelSaveData>();


        #region управление отрядами
        /// <summary> Создать новую команду и выбрать ее в качестве основной </summary>
        public void CreateAndSelectSquad(List<int> units)
        {
            SquadList.Add(new SquadItem(units));
            SelectSquad(SquadList.Count - 1);
        }

        /// <summary> Выбрать команду </summary>
        private void SelectSquad(int squadId)
        {
            CurrentSquadId = Mathf.Clamp(squadId, 0, SquadList.Count - 1);
        }

        /// <summary> Получить список юнитов в текущую команду </summary>
        public void SetSuadUnits(List<int> newUnits) => CurrentSquad.SetSquadUnits(newUnits);

        /// <summary> Добавить юнит в текущую команду </summary>
        public void AddUnitInSquad(int unitId) => CurrentSquad.Units.Add(unitId);

        /// <summary> Удалить юнит из текущей команды </summary>
        public void RemoveUnitFromSquad(int unitId) => CurrentSquad.Units.Remove(unitId);
        #endregion

        #region управление коллекцией
        /// <summary> Добавить новый юнит в коллекцию или обновить уровень открытого </summary>
        public void InsertOrUpdateUnit(int unitId, int unitLevel)
        {
            if (!OpenedUnits.ContainsKey(unitId))
                OpenedUnits.Add(unitId, unitLevel);
            else
                OpenedUnits[unitId] = unitLevel;
        }

        /// <summary> Добавить новых юнитов в коллекцию и/или обновить их уровень </summary>
        public void InsertOrUpdateUnits(Dictionary<int, int> units)
        {
            foreach (var unit in units) InsertOrUpdateUnit(unit.Key, unit.Value);
        }

        /// <summary> Удалить юнит из коллекции </summary>
        public void RemoveUnit(int unitId)
        {
            OpenedUnits.Remove(unitId);
        }
        #endregion
    }
    #endregion

    /// <summary> Общие данные игрока </summary>
    public class PlayerData
    {
        const int _maxLevel = 60;

        [JsonProperty] public int CurrentLevel { get; private set; } = 1;
        [JsonProperty] public int CurrentExperienseAmount { get; private set; } = 0;
        [JsonProperty] public int NextLevelExperienseAmount { get; private set; } = 1000; // _TODO рассчитать таблицу необходимого опыта на уровень и подставить значение

        [JsonProperty] public Currency Gold { get; private set; }

        [JsonProperty] public FractionType FractionType { get; private set; }
        [JsonProperty] public Dictionary<FractionType, FractionProgress> Fractions = new Dictionary<FractionType, FractionProgress>();

        [JsonIgnore] public FractionProgress CurrentFractionProgress
        {
            get
            {
                Fractions.TryGetValue(FractionType, out FractionProgress progress);
                return progress;
            }
        }

        public PlayerData()
        {
            Gold = new Currency(250);
        }

        public float GetPlayerLevelPercent() => CurrentExperienseAmount * 1.0f / NextLevelExperienseAmount;

        public void IncreasePlayerLevel(int exp)
        {
            var newExp = CurrentExperienseAmount + exp;
            var newLevels = Mathf.FloorToInt(newExp / 1000f);
            var remainderExp = newExp - newLevels * 1000;

            CurrentLevel = Mathf.Clamp(CurrentLevel + newLevels, 1, _maxLevel);
            CurrentExperienseAmount = remainderExp;
            NextLevelExperienseAmount = 1000;
        }

        public void IncreaseUnitLevel(int unitId)
        {
            if (CurrentFractionProgress.OpenedUnits.ContainsKey(unitId))
                CurrentFractionProgress.OpenedUnits[unitId] += 1;
            else
                CurrentFractionProgress.OpenedUnits.Add(unitId, 1);
        }

        public void CreateOrSelectFraction(FractionType fractionType)
        {
            FractionType = fractionType;

            if (!Fractions.ContainsKey(FractionType))
            {
                var newFraction = new FractionProgress();

                // Добавление базовых юнитов в коллекцию
                // 1 башня мага и 1 солдат
                newFraction.InsertOrUpdateUnits(new Dictionary<int, int> { { 2, 1 }, { 3, 1 } });

                // Создать первый отряд из базовых юнитов
                newFraction.CreateAndSelectSquad(new List<int> { 2, 3 });

                Fractions.Add(FractionType, newFraction);
            }
        }

        public void InsertOrUpdateLevelProgress(int levelId, FinishCriteria criterias)
        {
            var levelData = CurrentFractionProgress.CompleteLevelData.FirstOrDefault(x => x.LevelId == levelId);

            if (levelData == null)
            {
                CurrentFractionProgress.CompleteLevelData.Add(levelData = LevelSaveData.CreateNew(levelId));
            }

            levelData.AddCriteria(criterias);
        }
    }
}