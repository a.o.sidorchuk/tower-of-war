﻿using Data.Abstractions;
using Data.Models.Effects;
using Data.Models.Units;
using UnityEngine;

namespace Data.Models.Abilities
{
    /// <summary> Тип воздействия на других юнитов </summary>
    public enum ImpactType
    {
        None,
        Attack,
        Healing,
        Buff,
        Debuff
    }

    /// <summary> Тип цели способности юнита </summary>
    public enum TargetType
    {
        None,
        Enemy,
        Friendly
    }

    /// <summary> Типы воздействий способностей </summary>
    public enum SchoolType
    {
        None,
        Physical,
        Fire,
        Cold,
        Earth,
        Electricity,
        Darkness,
        Light
    }

    /// <summary> Типы напраление и областей применения способности юнита </summary>
    [System.Flags]
    public enum DirectionType
    {
        None,
        Self = 1 << 0,
        Target = 1 << 1,
        Area = 1 << 2,
        Chain = 1 << 3,
        Cone = 1 << 4
    }

    /// <summary> Параметры дополнительных эффектов способностей юнитов </summary>
    [System.Serializable]
    public class EffectParam
    {
        [SerializeField] EffectData _baseEffect;
        [SerializeField] [Range(0, 1)] float _procChance;

        #region public fields
        public EffectData BaseEffect => _baseEffect;
        public float ProcChance => _procChance;
        #endregion
    }

    /// <summary> Данные способностей юнитов </summary>
    public class AbilityData : BaseScriptableObjectEntity<AbilityData>
    {
        [Space]
        [SerializeField] ImpactType _impactType;
        [SerializeField] TargetType _targetType;
        [SerializeField] UnitType _unitTargetType;
        [Space]
        [SerializeField] float _range;
        [SerializeField] float _castTime;
        [SerializeField] SchoolType _schoolType;
        [Space]
        [SerializeField] DirectionType _directionType;
        [SerializeField] int _targetCount;
        [SerializeField] float _raduis;
        [Space]
        [SerializeField] EffectParam _effectParam;

        #region public fields
        public ImpactType ImpactType => _impactType;
        public TargetType TargetType => _targetType;
        public UnitType UnitTargetType => _unitTargetType;
        public float Range => _range;
        public float CastTime => _castTime;
        public SchoolType SchoolType => _schoolType;
        public DirectionType DirectionType => _directionType;
        public int TargetCount => _targetCount;
        public float Raduis => _raduis;
        public EffectParam EffectParam => _effectParam;
        #endregion

        public bool DirectionCheck(DirectionType type) => (_directionType & type) != 0;
    }
}