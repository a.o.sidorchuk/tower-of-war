﻿using Newtonsoft.Json;
using System;
using UnityEngine;

namespace Data.Models.Economy
{
    /// <summary> Базовый класс для управления валютами </summary>
    public class Currency
    {
        public event Action currencyChanged;

        [JsonProperty] public float CurrentCurrency { get; private set; }
        [JsonProperty] public int MaxValue { get; private set; } = 0;

        public Currency(int startingCurrency) => ChangeCurrency(startingCurrency);

        public void UpdateMaxValue(int maxValue) => MaxValue = maxValue;

        public bool CanAfford(int cost) => CurrentCurrency >= cost;

        public void AddCurrency(float increment) => ChangeCurrency(increment);

        public bool TryPurchase(int cost)
        {
            if (!CanAfford(cost)) return false;
            ChangeCurrency(-cost);
            return true;
        }

        protected void ChangeCurrency(float increment)
        {
            if (increment != 0)
            {
                if (MaxValue == 0)
                    CurrentCurrency += increment;
                else
                    CurrentCurrency = Mathf.Clamp(CurrentCurrency + increment, 0, MaxValue);

                currencyChanged?.Invoke();
            }
        }
    }
}