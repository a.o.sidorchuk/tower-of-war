﻿using Data.Models.Abilities;
using Data.Abstractions;
using UnityEngine;

namespace Data.Models.Effects
{
    /// <summary> Тип механики эффекта </summary>
    public enum EffectType
    {
        Passive,
        BlockingActions,
        Protection,
        TakingDamage
    }

    /// <summary> Данные для дополнительных эффектов способностей юнитов </summary>
    public class EffectData : BaseScriptableObjectEntity<EffectData>
    {
        [SerializeField] EffectType _effectType;
        [SerializeField] ImpactType _impactType;
        [Tooltip("Может ли этот эффект суммироваться с эффектами того же типа?")]
        [SerializeField] bool _addUp;   

        #region public fields
        public EffectType EffectType => _effectType;
        public ImpactType ImpactType => _impactType;
        public bool AddUp => _addUp;
        #endregion
    }
}