﻿using Data.Abstractions;
using UnityEngine;

namespace Data.Models.Units
{
    /// <summary> Редкость юнита </summary>
    public enum RarityType
    {
        None,
        Normal,
        Rare,
        Legendary
    }

    /// <summary> Тип юнита </summary>
    [System.Flags]
    public enum UnitType
    {
        None,
        Air = 1 << 0,
        Ground = 1 << 1,
        Building = 1 << 2,
        BuildingModule = 1 << 3
    }

    /// <summary> Роль юнита в отряде </summary>
    [System.Flags]
    public enum RoleType
    {
        None,
        Tank = 1 << 0,
        MDD = 1 << 1,
        RDD = 1 << 2,
        Support = 1 << 3
    }

    /// <summary> Скорость передвижения юнита </summary>
    public enum MovementSpeedType
    {
        None,
        Slowly,
        Usually,
        Fast
    }

    /// <summary> Тип ресурсов необходимых для призыва юнита в бою </summary>
    public enum ResourcesType
    {
        None,
        Mana,
        WarResources
    }

    /// <summary> Основные данные юнита </summary>
    public class UnitData : BaseScriptableObjectEntity<UnitData>
    {
        [Space]
        [SerializeField] RarityType _rarityType;
        [SerializeField] UnitType _unitType;
        [SerializeField] RoleType _roleType;
        [Space]
        [SerializeField] MovementSpeedType _movementSpeedType;
        [SerializeField] int _health;
        [SerializeField] int _strength;
        [SerializeField] int _criticalStrike;
        [Space]
        [SerializeField] ResourcesType _resourcesType;
        [SerializeField] int _cost;
        [SerializeField] float _reloadingTime;
        [SerializeField] int _recruitsNumber;
        [Space]
        [SerializeField] int _trainingCost;

        #region public fields
        public RarityType RarityType => _rarityType;
        public UnitType UnitType => _unitType;
        public RoleType RoleType => _roleType;
        public MovementSpeedType MovementSpeedType => _movementSpeedType;
        public int Health => _health;
        public int Strength => _strength;
        public int CriticalStrike => _criticalStrike;
        public int Cost => _cost;
        public int RecruitsNumber => _recruitsNumber;
        public ResourcesType ResourcesType => _resourcesType;
        public float ReloadingTime => _reloadingTime;
        public int TrainingCost => _trainingCost;
        #endregion

        public bool UnitTypeCheck(UnitType unitType) => (_unitType & unitType) != 0;
    }
}