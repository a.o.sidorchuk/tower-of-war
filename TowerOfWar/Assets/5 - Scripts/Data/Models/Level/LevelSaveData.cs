﻿using Newtonsoft.Json;

namespace Data.Models.Level
{
    public enum FinishCriteria
    {
        None,
        TowerUndamaged = 1 << 0,
        LevelComplete = 1 << 1,
        TimeLimit = 1 << 2
    }

    /// <summary> Общая информация о текущем игровом уровне </summary>
    public class LevelSaveData
    {
        [JsonProperty] public int LevelId { get; private set; }
        [JsonProperty] public FinishCriteria FinishCriterias { get; private set; }

        public void AddCriteria(FinishCriteria criteria)
        {
            FinishCriterias = FinishCriterias != FinishCriteria.None ? (FinishCriterias | criteria) : criteria;
        }

        public bool CheckFinishCriteria(FinishCriteria criteria) => (FinishCriterias & criteria) != 0;

        public static LevelSaveData CreateNew(int levelId)
        {
            return new LevelSaveData
            {
                LevelId = levelId
            };
        }
    }
}