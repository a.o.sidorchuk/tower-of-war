﻿using Data.Models.Economy;

namespace Data.Models.Level
{
    /// <summary> Команды игроков </summary>
    public enum SquadName
    {
        None,
        A,
        B
    }

    /// <summary> Состояние игрока во время игровой сессии </summary>
    public enum PlayerState
    {
        None,
        PreLoaded,
        Ready,
        Loaded
    }

    /// <summary> Состояние игрового уровня </summary>
    public enum LevelState
    {
        SearchMode,
        CheckReady,
        Ready,
        PreLoaded,
        Intro,
        Started,
        Finished
    }

    /// <summary> Информация о ходе игровой сессии хранимая в Store </summary>
    public class GameSessionData
    {
        public int SquadId { get; private set; }
        public LevelState LevelState { get; private set; }

        public Currency Mana { get; private set; } = new Currency(0);
        public Currency ResourceWar { get; private set; } = new Currency(180);

        public Currency GoldDrop { get; private set; } = new Currency(0);
        public Currency ExpDrop { get; private set; } = new Currency(0);

        public void SetSquadId(int squadId) => SquadId = squadId;
        public void SetLevelState(LevelState levelState) => LevelState = levelState;
    }
}