﻿using UnityEngine;

namespace Data.Abstractions
{
    public abstract class BaseScriptableObjectEntity<T> : ScriptableObject, IEntity
        where T : BaseScriptableObjectEntity<T>
    {
        [SerializeField] int _id = -1;

        public int Id => _id;

        protected virtual void OnEnable()
        {
#if UNITY_EDITOR
            if (Id != -1) return;

            var maxId = 0;
            var brushAssets = UnityEditor.AssetDatabase.FindAssets($"t:{typeof(T).Name}");

            foreach (var brushGUID in brushAssets)
            {
                var brush = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(UnityEditor.AssetDatabase.GUIDToAssetPath(brushGUID));
                if (brush.Id > maxId) maxId = brush.Id;
            }

            _id = ++maxId;

            Debug.Log($"SO: {typeof(T).Name} || ID: {Id}");
#endif
        }
    }
}