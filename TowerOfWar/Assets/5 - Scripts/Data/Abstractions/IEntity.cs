﻿namespace Data.Abstractions
{
    public interface IEntity
    {
        int Id { get; }
    }
}