﻿using UnityEngine;

namespace Data.Abstractions
{
    public class BaseEntity : IEntity
    {
        [SerializeField] int _id = -1;
        public int Id => _id;
    }
}