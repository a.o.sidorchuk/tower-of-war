﻿using Core.Health;
using UnityEngine;

namespace ActionGameFramework.Health
{
    /// <summary> Базовая реализация зоны получающей урон </summary>
    public abstract class DamageZone : MonoBehaviour
    {
        [SerializeField] protected DamageableBehaviour _damageableBehaviour;

        /// <summary>Коэффициент изменения входящего урона</summary>
        [SerializeField] float _damageScale = 1f;

        protected float ScaleDamage(float damage) => _damageScale * damage;

        protected virtual void Awake()
        {
            if (_damageableBehaviour != null) return;
            _damageableBehaviour = GetComponent<DamageableBehaviour>();
        }
    }
}