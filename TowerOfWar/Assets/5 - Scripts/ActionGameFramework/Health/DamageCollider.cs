﻿using UnityEngine;

namespace ActionGameFramework.Health
{
	/// <summary> Колайдер для реализации получения урона </summary>
	public class DamageCollider : DamageZone
	{
		protected void OnCollisionEnter(Collision c)
		{
			var damager = c.gameObject.GetComponent<Damager>();
			if (damager == null) return; 
			
			var scaledDamage = ScaleDamage(damager.Damage);
			var collisionPosition = ConvertContactsToPosition(c.contacts);

			_damageableBehaviour.TakeDamage(scaledDamage, collisionPosition, damager.SquadId);
			
			damager.HasDamaged(collisionPosition, _damageableBehaviour.Configuration.SquadId);
		}

		/// <summary> Определение позиции попадания </summary>
		protected Vector3 ConvertContactsToPosition(ContactPoint[] contacts)
		{
			var output = Vector3.zero;
			var length = contacts.Length;

			if (length == 0) return output; 

			for (var i = 0; i < length; i++)
			{
				output += contacts[i].point;
			}

			return output / length;
		}
	}
}