﻿using UnityEngine;

namespace ActionGameFramework.Health
{
    /// <summary> Триггер для реализации получения урона </summary>
    [RequireComponent(typeof(Collider))]
    public class DamageTrigger : DamageZone
    {
        protected void OnTriggerEnter(Collider triggeredCollider)
        {
            var damager = triggeredCollider.GetComponent<Damager>();
            if (damager == null) return;

            var scaledDamage = ScaleDamage(damager.Damage);
            var collisionPosition = triggeredCollider.ClosestPoint(damager.transform.position);

            _damageableBehaviour.TakeDamage(scaledDamage, collisionPosition, damager.SquadId);

            damager.HasDamaged(collisionPosition, _damageableBehaviour.Configuration.SquadId);
        }
    }
}