﻿using System;
using UnityEngine;

namespace ActionGameFramework.Health
{
    /// <summary> Базовый класс реализации повреждения других объектов </summary>
    public abstract class Damager : MonoBehaviour
    {
        public event Action<Vector3> hasDamaged;

        public int SquadId { get; private set; }

        public abstract float Damage { get; }

        public virtual void Init(int squadId) => SquadId = squadId;

        public virtual void HasDamaged(Vector3 point, int otherSquadId) => hasDamaged?.Invoke(point);
    }
}