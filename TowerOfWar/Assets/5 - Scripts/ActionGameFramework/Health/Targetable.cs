using Core.Health;
using UnityEngine;

namespace ActionGameFramework.Health
{
	/// <summary> ������� ��������� ��� ���������� ���� ��� ������ �������� </summary>
	public class Targetable : DamageableBehaviour
	{
		[Space]
		[Tooltip("��������� �� ������� ��������� ������ ����")]
		[SerializeField] protected Transform _targetTransform;

		protected Vector3 _currentPosition, _previousPosition;

		public Transform TargetableTransform => _targetTransform == null ? transform : _targetTransform;
		public override Vector3 Position => TargetableTransform.position;

		public virtual Vector3 Velocity { get; protected set; }
		public int Priority { get; protected set; } = 0;

		public override void Init(int squadId)
		{
			base.Init(squadId);
			ResetPositionData();
		}

		protected void ResetPositionData()
		{
			_currentPosition = Position;
			_previousPosition = Position;
		}

		void FixedUpdate()
		{
			_currentPosition = Position;
			Velocity = (_currentPosition - _previousPosition) / Time.fixedDeltaTime;
			_previousPosition = _currentPosition;
		}
	}
}