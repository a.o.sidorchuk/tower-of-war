﻿using ActionGameFramework.Projectiles;
using UnityEngine;

namespace ActionGameFramework.Helpers
{
	/// <summary> Вспомогательный класс для решения общих задач баллистики снарядов </summary>
	public static class Ballistics
	{
		/// <summary>
		/// Вычисляет начальную скорость линейного снаряда, направленного в заданную мировую координату.
		/// </summary>
		/// <param name="firePosition">Starting point of the projectile.</param>
		/// <param name="targetPosition">Intended target point of the projectile.</param>
		/// <param name="launchSpeed">Initial speed of the projectile.</param>
		/// <returns>Vector3 describing initial velocity for this projectile. Vector3.zero if no solution.</returns>
		public static Vector3 CalculateLinearFireVector(Vector3 firePosition, Vector3 targetPosition,
		                                                float launchSpeed)
		{
			if (Mathf.Abs(launchSpeed) < float.Epsilon) launchSpeed = 0.001f; 

			return (targetPosition - firePosition).normalized * launchSpeed;
		}

		/// <summary>
		/// Вычисляет время, необходимое линейному снаряду для достижения заданного места 
		/// назначения с заданной начальной скоростью и ускорением.
		/// </summary>
		/// <param name="firePosition">Starting point of the projectile.</param>
		/// <param name="targetPosition">Intended target point of the projectile.</param>
		/// <param name="launchSpeed">Initial speed of the projectile.</param>
		/// <param name="acceleration">Post-firing acceleration of the projectile.</param>
		/// <returns>Time in seconds to complete flight to target.</returns>
		public static float CalculateLinearFlightTime(Vector3 firePosition, Vector3 targetPosition,
		                                              float launchSpeed, float acceleration)
		{
			var flightDistance = (targetPosition - firePosition).magnitude;

			// v^2 = u^2 + 2as
			var endV = Mathf.Sqrt((launchSpeed * launchSpeed) + (2 * acceleration * flightDistance));

			// t = 2s/(u+v)
			return (2f * flightDistance) / (launchSpeed + endV);
		}

		/// <summary>
		/// Вычисляет целевую точку, которая гарантирует, что линейный снаряд ударит по движущейся цели.
		/// Предполагается, что цель имеет постоянную скорость. Точность можно регулировать.
		/// </summary>
		/// <param name="firePosition">Starting point of the projectile.</param>
		/// <param name="targetPosition">The current position of the intended target.</param>
		/// <param name="targetVelocity">Vector representing the velocity of the intended target.</param>
		/// <param name="launchSpeed">Initial speed of the projectile.</param>
		/// <param name="acceleration">Post-firing acceleration of the projectile.</param>
		/// <param name="precision">Number of iterations to approximate the correct position. Higher precision is better for faster targets.</param>
		/// <returns>Vector3 representing the leading target point.</returns>
		public static Vector3 CalculateLinearLeadingTargetPoint(Vector3 firePosition, Vector3 targetPosition,
		                                                        Vector3 targetVelocity, float launchSpeed, float acceleration,
		                                                        int precision = 2)
		{
			if (precision <= 0) return targetPosition; 

			var testPosition = targetPosition;

			for (var i = 0; i < precision; i++)
			{
				var impactTime = CalculateLinearFlightTime(firePosition, testPosition, launchSpeed, acceleration);

				testPosition = targetPosition + (targetVelocity * impactTime);
			}

			return testPosition;
		}

		/// <summary>
		/// Вычисляет скорость запуска параболического снаряда для попадания в заданную целевую точку при выстреле под заданным углом.
		/// </summary>
		/// <param name="firePosition">Position from which the projectile is fired.</param>
		/// <param name="targetPosition">Intended target position.</param>
		/// <param name="launchAngle">Angle at which the projectile is to be fired.</param>
		/// <param name="gravity">Gravitational constant (Vertical only. Positive = down)</param>
		/// <returns>Vector3 representing launch velocity to hit the target. Vector3.zero if no solution.</returns>
		public static Vector3 CalculateBallisticFireVectorFromAngle(Vector3 firePosition, Vector3 targetPosition,
		                                                            float launchAngle, float gravity)
		{
			var target = targetPosition;
			target.y = firePosition.y;
			var toTarget = target - firePosition;
			var targetDistance = toTarget.magnitude;
			var shootingAngle = launchAngle;
			var relativeY = firePosition.y - targetPosition.y;

			var theta = Mathf.Deg2Rad * shootingAngle;
			var cosTheta = Mathf.Cos(theta);
			var num = targetDistance * Mathf.Sqrt(gravity) * Mathf.Sqrt(1 / cosTheta);
			var denom = Mathf.Sqrt((2 * targetDistance * Mathf.Sin(theta)) + (2 * relativeY * cosTheta));

			if (denom > 0)
			{
				var v = num / denom;

				var aimVector = toTarget / targetDistance;
				aimVector.y = 0;

				var rotAxis = Vector3.Cross(aimVector, Vector3.up);
				var rotation = Quaternion.AngleAxis(shootingAngle, rotAxis);

				aimVector = rotation * aimVector.normalized;

				return aimVector * v;
			}

			return Vector3.zero;
		}

		/// <summary>
		/// Вычисляет скорость пуска снаряда с параболической траекторией для попадания в заданную целевую точку 
		/// при выстреле под заданным углом. Использует вертикальную гравитационную постоянную, 
		/// определенную в настройках физики проекта.
		/// </summary>
		/// <param name="firePosition">Position from which the projectile is fired.</param>
		/// <param name="targetPosition">Intended target position.</param>
		/// <param name="launchAngle">Angle at which the projectile is to be fired.</param>
		/// <returns>Vector3 representing launch velocity to hit the target. Vector3.zero if no solution.</returns>
		public static Vector3 CalculateBallisticFireVectorFromAngle(Vector3 firePosition, Vector3 targetPosition,
		                                                            float launchAngle)
		{
			return CalculateBallisticFireVectorFromAngle(firePosition, targetPosition, launchAngle, Mathf.Abs(Physics.gravity.y));
		}

		/// <summary>
		/// Вычисляет скорость пуска снаряда с параболической траекторией для попадания в заданную целевую точку, когда
		/// стреляли с заданной скоростью.
		/// </summary>
		/// <param name="firePosition">Position from which the projectile is fired.</param>
		/// <param name="targetPosition">Intended target position.</param>
		/// <param name="launchSpeed">The speed that the projectile is launched at.</param>
		/// <param name="arcHeight">Preference between parabolic ("underhand") or direct ("overhand") projectile arc.</param>
		/// <param name="gravity">Gravitational constant (Vertical only. Positive = down)</param>
		/// <returns>Vector3 representing launch launchSpeed to hit the target. Vector3.zero if no solution.</returns>
		public static Vector3 CalculateBallisticFireVectorFromVelocity(Vector3 firePosition, Vector3 targetPosition,
		                                                               float launchSpeed, BallisticArcHeight arcHeight,
		                                                               float gravity)
		{
			var theta = CalculateBallisticFireAngle(firePosition, targetPosition, launchSpeed, arcHeight, gravity);

			if (float.IsNaN(theta)) return Vector3.zero;

			var target = targetPosition;
			target.y = firePosition.y;

			var toTarget = target - firePosition;

			var targetDistance = toTarget.magnitude;

			var aimVector = Vector3.forward;

			if (targetDistance > 0f)
			{
				aimVector = toTarget / targetDistance;
				aimVector.y = 0;
			}

			var rotAxis = Vector3.Cross(aimVector, Vector3.up);
			var rotation = Quaternion.AngleAxis(theta, rotAxis);
			aimVector = rotation * aimVector.normalized;

			return aimVector * launchSpeed;
		}

		/// <summary>
		/// Вычисляет скорость запуска параболического снаряда для попадания в заданную целевую точку 
		/// при выстреле с заданной скоростью. Использует вертикальную гравитационную постоянную, 
		/// определенную в настройках физики проекта.
		/// </summary>
		/// <param name="firePosition">Position from which the projectile is fired.</param>
		/// <param name="targetPosition">Intended target position.</param>
		/// <param name="launchSpeed">The speed that the projectile is launched at.</param>
		/// <param name="arcHeight">Preference between parabolic ("underhand") or direct ("overhand") projectile arc.</param>
		/// <returns>Vector3 representing launch launchSpeed to hit the target. Vector3.zero if no solution.</returns>
		public static Vector3 CalculateBallisticFireVectorFromVelocity(Vector3 firePosition, Vector3 targetPosition,
		                                                               float launchSpeed, BallisticArcHeight arcHeight)
		{
			return CalculateBallisticFireVectorFromVelocity(firePosition, targetPosition, launchSpeed, arcHeight,
			                                                Mathf.Abs(Physics.gravity.y));
		}

		/// <summary>
		/// Вычисляет угол, под которым должен быть пущен снаряд с заданной начальной скоростью, чтобы поразить цель.
		/// </summary>
		/// <param name="firePosition">Position from which the projectile is fired</param>
		/// <param name="targetPosition">Intended target position.</param>
		/// <param name="launchSpeed">The speed that the projectile is launched at.</param>
		/// <param name="arcHeight">Preference between parabolic ("underhand") or direct ("overhand") projectile arc.</param>
		/// <param name="gravity">Gravitational constant (Vertical only. Positive = down)</param>
		/// <returns>The required launch angle in degrees. NaN if no valid solution.</returns>
		public static float CalculateBallisticFireAngle(Vector3 firePosition, Vector3 targetPosition,
		                                                float launchSpeed, BallisticArcHeight arcHeight, float gravity)
		{
			var target = targetPosition;
			target.y = firePosition.y;

			var toTarget = target - firePosition;
			var targetDistance = toTarget.magnitude;
			var relativeY = targetPosition.y - firePosition.y;
			var vSquared = launchSpeed * launchSpeed;

			// If the distance to our target is zero, we can assume it's right on top of us (or that we're our own target).
			if (Mathf.Approximately(targetDistance, 0f))
			{
				// If we're preferring a high-angle shot, we just fire straight up.
				if (arcHeight == BallisticArcHeight.UseHigh || arcHeight == BallisticArcHeight.PreferHigh)
				{
					return 90f;
				}

				// If we're doing a low-angle direct shot, we tweak our angle based on relative height of target.
				if (relativeY > 0)
				{
					return 90f;
				}

				if (relativeY < 0)
				{
					return -90f;
				}
			}

			var b = Mathf.Sqrt((vSquared * vSquared) -
			                     (gravity * ((gravity * (targetDistance * targetDistance)) + (2 * relativeY * vSquared))));

			// The "underarm", parabolic arc angle
			var theta1 = Mathf.Atan((vSquared + b) / (gravity * targetDistance));

			// The "overarm", direct arc angle
			var theta2 = Mathf.Atan((vSquared - b) / (gravity * targetDistance));

			var theta1Nan = float.IsNaN(theta1);
			var theta2Nan = float.IsNaN(theta2);

			// If both are invalid, we early-out with a NaN to indicate no solution.
			if (theta1Nan && theta2Nan) return float.NaN;

			// We'll init with the parabolic arc.
			var returnTheta = theta1;

			// If we want to return the direct arc
			if (arcHeight == BallisticArcHeight.UseLow) returnTheta = theta2; 

			// If we want to return theta1 wherever valid, but will settle for theta2 if theta1 is invalid
			if (arcHeight == BallisticArcHeight.PreferHigh) returnTheta = theta1Nan ? theta2 : theta1; 

			// If we want to return theta2 wherever valid, but will settle for theta1 if theta2 is invalid
			if (arcHeight == BallisticArcHeight.PreferLow) returnTheta = theta2Nan ? theta1 : theta2; 

			return returnTheta * Mathf.Rad2Deg;
		}

		/// <summary>
		/// Вычисляет угол, под которым должен быть пущен снаряд с заданной начальной скоростью, чтобы поразить цель. 
		/// Использует вертикальную гравитационную постоянную, определенную в настройках физики проекта.
		/// </summary>
		/// <param name="firePosition">Position from which the projectile is fired</param>
		/// <param name="targetPosition">Intended target position.</param>
		/// <param name="launchSpeed">The speed that the projectile is launched at.</param>
		/// <param name="arcHeight">Preference between parabolic ("underhand") or direct ("overhand") projectile arc.</param>
		/// <returns>The required launch angle in degrees. NaN if no valid solution.</returns>
		public static float CalculateBallisticFireAngle(Vector3 firePosition, Vector3 targetPosition,
		                                                float launchSpeed, BallisticArcHeight arcHeight)
		{
			return CalculateBallisticFireAngle(firePosition, targetPosition, launchSpeed, arcHeight,
			                                   Mathf.Abs(Physics.gravity.y));
		}

		/// <summary>
		/// Вычисляет количество времени, которое потребуется снаряду, чтобы завершить свою дугу.
		/// </summary>
		/// <param name="firePosition">Position from which the projectile is fired</param>
		/// <param name="targetPosition">Intended target position.</param>
		/// <param name="launchSpeed">The speed that the projectile is launched at.</param>
		/// <param name="fireAngle">The angle in degrees that the projectile was fired at.</param>
		/// <param name="gravity">Gravitational constant (Vertical only. Positive = down)</param>
		/// <returns>Time in seconds to complete arc to target. NaN if no valid solution.</returns>
		public static float CalculateBallisticFlightTime(Vector3 firePosition, Vector3 targetPosition, float launchSpeed,
		                                                 float fireAngle, float gravity)
		{
			var relativeY = firePosition.y - targetPosition.y;

			var targetVector = targetPosition - firePosition;

			targetVector.y = 0;

			var targetDistance = targetVector.magnitude;

			fireAngle *= Mathf.Deg2Rad;

			var sinFireAngle = Mathf.Sin(fireAngle);

			var a = (launchSpeed * Mathf.Sin(fireAngle)) / gravity;
			var b = Mathf.Sqrt((launchSpeed * launchSpeed * (sinFireAngle * sinFireAngle)) + (2 * gravity * relativeY)) / gravity;

			var flightTime1 = a + b;
			var flightTime2 = a - b;

			var flightDistance1 = launchSpeed * Mathf.Cos(fireAngle) * flightTime1;
			var flightDistance2 = launchSpeed * Mathf.Cos(fireAngle) * flightTime2;

			if (flightTime2 > 0)
			{
				if (Mathf.Abs(targetDistance - flightDistance2) < Mathf.Abs(targetDistance - flightDistance1))
				{
					return flightTime2;
				}
			}

			return flightTime1;
		}

		/// <summary>
		/// Вычисляет количество времени, которое потребуется снаряду, чтобы завершить свою дугу. 
		/// Использует вертикальную гравитационную постоянную, определенную в настройках физики проекта.
		/// </summary>
		/// <param name="firePosition">Position from which the projectile is fired</param>
		/// <param name="targetPosition">Intended target position.</param>
		/// <param name="launchSpeed">The speed that the projectile is launched at.</param>
		/// <param name="fireAngle">The angle in degrees that the projectile was fired at.</param>
		/// <returns>Time in seconds to complete arc to target. NaN if no valid solution.</returns>
		public static float CalculateBallisticFlightTime(Vector3 firePosition, Vector3 targetPosition,
		                                                 float launchSpeed, float fireAngle)
		{
			return CalculateBallisticFlightTime(firePosition, targetPosition, launchSpeed, fireAngle,
			                                    Mathf.Abs(Physics.gravity.y));
		}

		/// <summary>
		/// Вычисляет приблизительную переднюю точку цели, чтобы гарантировать, что баллистический снаряд ударит 
		/// по движущейся цели, предполагая заданную скорость запуска. Предполагает постоянную скорость цели и 
		/// постоянную скорость снаряда после запуска. Точность можно регулировать параметрически.
		/// </summary>
		/// <param name="firePosition">Starting point of the projectile.</param>
		/// <param name="targetPosition">The current position of the intended target.</param>
		/// <param name="targetVelocity">Vector representing the velocity of the intended target.</param>
		/// <param name="launchSpeed">Initial speed of the projectile.</param>
		/// <param name="arcHeight">Preference between parabolic ("underhand") or direct ("overhand") projectile arc.</param>
		/// <param name="precision">Number of iterations to approximate the correct position. Higher precision is better for faster targets.</param>
		/// <param name="gravity">Gravitational constant (Vertical only. Positive = down)</param>
		/// <returns>Vector3 representing the leading target point. Vector3.zero if no solution.</returns>
		public static Vector3 CalculateBallisticLeadingTargetPointWithSpeed(Vector3 firePosition, Vector3 targetPosition,
		                                                                    Vector3 targetVelocity, float launchSpeed,
		                                                                    BallisticArcHeight arcHeight, float gravity,
		                                                                    int precision = 2)
		{
			// No precision means no leading, so we early-out.
			if (precision <= 1) return targetPosition; 

			var testPosition = targetPosition;

			for (var i = 0; i < precision; i++)
			{
				var fireAngle = CalculateBallisticFireAngle(firePosition, testPosition, launchSpeed, arcHeight, gravity);
				var impactTime = CalculateBallisticFlightTime(firePosition, testPosition, launchSpeed, fireAngle, gravity);

				if (float.IsNaN(fireAngle) || float.IsNaN(impactTime)) return Vector3.zero; 

				testPosition = targetPosition + (targetVelocity * impactTime);
			}

			return testPosition;
		}

		/// <summary>
		/// Вычисляет приблизительную переднюю точку цели, чтобы гарантировать, что баллистический снаряд ударит 
		/// по движущейся цели, предполагая заданную скорость запуска. Предполагает постоянную скорость цели и 
		/// постоянную скорость снаряда после запуска. Точность можно регулировать параметрически. 
		/// Использует вертикальную гравитационную постоянную, определенную в настройках физики проекта.
		/// </summary>
		/// <param name="firePosition">Starting point of the projectile.</param>
		/// <param name="targetPosition">The current position of the intended target.</param>
		/// <param name="targetVelocity">Vector representing the velocity of the intended target.</param>
		/// <param name="launchSpeed">Initial speed of the projectile.</param>
		/// <param name="arcHeight">Preference between parabolic ("underhand") or direct ("overhand") projectile arc.</param>
		/// <param name="precision">Number of iterations to approximate the correct position. Higher precision is better for faster targets.</param>
		/// <returns>Vector3 representing the leading target point. Vector3.zero if no solution.</returns>
		public static Vector3 CalculateBallisticLeadingTargetPointWithSpeed(Vector3 firePosition, Vector3 targetPosition,
		                                                                    Vector3 targetVelocity, float launchSpeed,
		                                                                    BallisticArcHeight arcHeight, int precision = 2)
		{
			return CalculateBallisticLeadingTargetPointWithSpeed(firePosition, targetPosition, targetVelocity, launchSpeed,
			                                                     arcHeight, Mathf.Abs(Physics.gravity.y), precision);
		}

		/// <summary>
		/// Вычисляет приблизительную переднюю точку цели, чтобы гарантировать, что баллистический снаряд ударит 
		/// по движущейся цели, принимая заданный угол пуска. Предполагает постоянную скорость цели и 
		/// постоянную скорость снаряда после запуска. Точность можно регулировать параметрически. 
		/// Использует вертикальную гравитационную постоянную, определенную в настройках физики проекта.
		/// </summary>
		/// <param name="firePosition">Starting point of the projectile.</param>
		/// <param name="targetPosition">The current position of the intended target.</param>
		/// <param name="targetVelocity">Vector representing the velocity of the intended target.</param>
		/// <param name="launchAngle">The angle at which the projectile is to be launched.</param>
		/// <param name="arcHeight">Preference between parabolic ("underhand") or direct ("overhand") projectile arc.</param>
		/// <param name="gravity">Gravitational constant (Vertical only. Positive = down)</param>
		/// <param name="precision">Number of iterations to approximate the correct position. Higher precision is better for faster targets.</param>
		/// <returns>Vector3 representing the leading target point. Vector3.zero if no solution.</returns>
		public static Vector3 CalculateBallisticLeadingTargetPointWithAngle(Vector3 firePosition,
		                                                                    Vector3 targetPosition,
		                                                                    Vector3 targetVelocity, float launchAngle,
		                                                                    BallisticArcHeight arcHeight, float gravity,
		                                                                    int precision = 2)
		{
			// No precision means no leading, so we early-out.
			if (precision <= 1) return targetPosition; 

			var testPosition = targetPosition;

			for (var i = 0; i < precision; i++)
			{
				var launchSpeed = CalculateBallisticFireVectorFromAngle(firePosition, testPosition, launchAngle, gravity)
					.magnitude;

				var impactTime = CalculateBallisticFlightTime(firePosition, testPosition, launchSpeed, launchAngle, gravity);

				if (float.IsNaN(launchSpeed) || float.IsNaN(impactTime)) return Vector3.zero; 

				testPosition = targetPosition + (targetVelocity * impactTime);
			}

			return testPosition;
		}

		/// <summary>
		/// Вычисляет приблизительную переднюю точку цели, чтобы гарантировать, что баллистический снаряд ударит 
		/// по движущейся цели, принимая заданный угол пуска. Предполагает постоянную скорость цели и 
		/// постоянную скорость снаряда после запуска. Точность можно регулировать параметрически. 
		/// Использует вертикальную гравитационную постоянную, определенную в настройках физики проекта.
		/// </summary>
		/// <param name="firePosition">Starting point of the projectile.</param>
		/// <param name="targetPosition">The current position of the intended target.</param>
		/// <param name="targetVelocity">Vector representing the velocity of the intended target.</param>
		/// <param name="launchAngle">The angle at which the projectile is to be launched.</param>
		/// <param name="arcHeight">Preference between parabolic ("underhand") or direct ("overhand") projectile arc.</param>
		/// <param name="precision">Number of iterations to approximate the correct position. Higher precision is better for faster targets.</param>
		/// <returns>Vector3 representing the leading target point. Vector3.zero if no solution.</returns>
		public static Vector3 CalculateBallisticLeadingTargetPointWithAngle(Vector3 firePosition,
		                                                                    Vector3 targetPosition,
		                                                                    Vector3 targetVelocity, float launchAngle,
		                                                                    BallisticArcHeight arcHeight, int precision = 2)
		{
			return CalculateBallisticLeadingTargetPointWithAngle(firePosition, targetPosition, targetVelocity,
			                                                     launchAngle, arcHeight, Mathf.Abs(Physics.gravity.y),
			                                                     precision);
		}
	}
}