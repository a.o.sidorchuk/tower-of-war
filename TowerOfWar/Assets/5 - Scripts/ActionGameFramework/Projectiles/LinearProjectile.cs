﻿using System;
using ActionGameFramework.Helpers;
using UnityEngine;

namespace ActionGameFramework.Projectiles
{
    /// <summary> Базовая реализация снаряда летящего по прямой линии </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class LinearProjectile : MonoBehaviour, IProjectile
    {
        public event Action fired;

        public float acceleration;
        public float startSpeed;

        protected bool _fired;
        protected Rigidbody _rigidbody;

        public virtual void FireAtPoint(Vector3 startPoint, Vector3 targetPoint)
        {
            transform.position = startPoint;

            Fire(Ballistics.CalculateLinearFireVector(startPoint, targetPoint, startSpeed));
        }

        public virtual void FireInDirection(Vector3 startPoint, Vector3 fireVector)
        {
            transform.position = startPoint;

            if (Math.Abs(startSpeed) < float.Epsilon) startSpeed = 0.001f;

            Fire(fireVector.normalized * startSpeed);
        }

        public void FireAtVelocity(Vector3 startPoint, Vector3 fireVelocity)
        {
            transform.position = startPoint;

            startSpeed = fireVelocity.magnitude;

            Fire(fireVelocity);
        }

        protected virtual void Awake() => _rigidbody = GetComponent<Rigidbody>();

        protected virtual void Update()
        {
            if (!_fired) return;

            if (Math.Abs(acceleration) >= float.Epsilon)
            {
                _rigidbody.velocity += transform.forward * acceleration * Time.deltaTime;
            }
        }

        protected virtual void Fire(Vector3 firingVector)
        {
            _fired = true;

            transform.rotation = Quaternion.LookRotation(firingVector);

            _rigidbody.velocity = firingVector;

            fired?.Invoke();
        }
    }
}