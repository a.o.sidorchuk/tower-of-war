﻿using UnityEngine;

namespace ActionGameFramework.Projectiles
{
	/// <summary> Снаряд с самонаведением, летящего не по прямой дуге </summary>
	public class WobblingHomingProjectile : HomingLinearProjectile
	{
		protected enum State
		{
			Wobbling,
			Turning,
			Targeting
		}

		public Vector2 wobbleTimeRange = new Vector2(1, 2);
		public float wobbleDirectionChangeSpeed = 4;
		public float wobbleMagnitude = 7;
		public float turningTime = 0.5f;

		State _state;

		protected float _currentWobbleTime;
		protected float _wobbleDuration;
		protected float _currentTurnTime;
		protected float _wobbleChangeTime;

		protected Vector3 _wobbleVector, _targetWobbleVector;

		protected override void Update()
		{
			// regular HomingLinearProjectile behaviour, handles a null homing target
			if (_homingTarget == null || _state == State.Targeting)
			{
				base.Update();
				return;
			}

			switch (_state)
			{
				// wobble the projectile
				case State.Wobbling:
					_currentWobbleTime += Time.deltaTime;
					if (_currentWobbleTime >= _wobbleDuration)
					{
						_state = State.Turning;
						_currentTurnTime = 0;
					}

					_wobbleChangeTime += Time.deltaTime * wobbleDirectionChangeSpeed;
					if (_wobbleChangeTime >= 1)
					{
						_wobbleChangeTime = 0;
						_targetWobbleVector = new Vector3(Random.Range(-wobbleMagnitude, wobbleMagnitude),
						                                   Random.Range(-wobbleMagnitude, wobbleMagnitude), 0);
						_wobbleVector = Vector3.zero;
					}
					_wobbleVector = Vector3.Lerp(_wobbleVector, _targetWobbleVector, _wobbleChangeTime);
					_rigidbody.velocity = Quaternion.Euler(_wobbleVector) * _rigidbody.velocity;

					_rigidbody.rotation = Quaternion.LookRotation(_rigidbody.velocity);
					break;
				// turn the projectile to face the homing target
				case State.Turning:
					_currentTurnTime += Time.deltaTime;
					Quaternion aimDirection = Quaternion.LookRotation(GetHeading());

					_rigidbody.rotation = Quaternion.Lerp(_rigidbody.rotation, aimDirection, _currentTurnTime / turningTime);
					_rigidbody.velocity = transform.forward * _rigidbody.velocity.magnitude;

					if (_currentTurnTime >= turningTime)
					{
						_state = State.Targeting;
					}
					break;
			}
		}

		protected override void Fire(Vector3 firingVector)
		{
			_targetWobbleVector = new Vector3(Random.Range(-wobbleMagnitude, wobbleMagnitude),
			                                   Random.Range(-wobbleMagnitude, wobbleMagnitude), 0);
			_wobbleDuration = Random.Range(wobbleTimeRange.x, wobbleTimeRange.y);

			base.Fire(firingVector);

			_state = State.Wobbling;
			_currentWobbleTime = 0.0f;
		}
	}
}