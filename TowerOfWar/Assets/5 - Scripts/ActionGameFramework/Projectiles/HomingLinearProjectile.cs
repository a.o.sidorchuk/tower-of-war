﻿using ActionGameFramework.Health;
using ActionGameFramework.Helpers;
using Core.Health;
using UnityEngine;

namespace ActionGameFramework.Projectiles
{
    /// <summary>
    /// Основное переопределение линейного снаряда, которое позволяет корректировать свой путь в полете 
    /// для перехвата назначенной цели.
    /// </summary>
    public class HomingLinearProjectile : LinearProjectile
    {
        public int leadingPrecision = 2;
        public bool leadTarget;

        protected Targetable _homingTarget;

        Vector3 _targetVelocity;

        public void SetHomingTarget(Targetable target) => _homingTarget = target;

        protected virtual void FixedUpdate()
        {
            if (_homingTarget == null) return;
            _targetVelocity = _homingTarget.Velocity;
        }

        protected override void Update()
        {
            if (!_fired) return; 

            if (_homingTarget == null)
            {
                _rigidbody.rotation = Quaternion.LookRotation(_rigidbody.velocity);
                return;
            }

            var aimDirection = Quaternion.LookRotation(GetHeading());

            _rigidbody.rotation = aimDirection;
            _rigidbody.velocity = transform.forward * _rigidbody.velocity.magnitude;

            base.Update();
        }

        protected Vector3 GetHeading()
        {
            if (_homingTarget == null)
            {
                return Vector3.zero;
            }
            Vector3 heading;
            if (leadTarget)
            {
                heading = Ballistics.CalculateLinearLeadingTargetPoint(transform.position, _homingTarget.Position,
                                                                       _targetVelocity, _rigidbody.velocity.magnitude,
                                                                       acceleration,
                                                                       leadingPrecision) - transform.position;
            }
            else
            {
                heading = _homingTarget.Position - transform.position;
            }

            return heading.normalized;
        }

        protected override void Fire(Vector3 firingVector)
        {
            if (_homingTarget == null)
            {
                Debug.LogError("Homing target has not been specified. Aborting fire.");
                return;
            }
            _homingTarget.removed += OnTargetDied;

            base.Fire(firingVector);
        }

        void OnTargetDied(DamageableBehaviour targetable)
        {
            targetable.removed -= OnTargetDied;
            _homingTarget = null;
        }
    }
}