﻿namespace ActionGameFramework.Projectiles
{
	/// <summary> Предпочтения при выборе баллистической дуги </summary>
	public enum BallisticArcHeight
	{
		UseHigh,
		UseLow,
		PreferHigh,
		PreferLow
	}
}