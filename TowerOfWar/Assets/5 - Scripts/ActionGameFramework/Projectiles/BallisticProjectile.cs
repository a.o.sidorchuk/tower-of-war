﻿using System;
using System.Collections.Generic;
using ActionGameFramework.Helpers;
using UnityEngine;

namespace ActionGameFramework.Projectiles
{
    /// <summary>
    /// Базовая реализация снаряда летящего по параболической дуге
    /// </summary>
    [RequireComponent(typeof(Rigidbody))]
    public class BallisticProjectile : MonoBehaviour, IProjectile
    {
        public event Action fired;

        public BallisticArcHeight arcPreference;
        public BallisticFireMode fireMode;

        [Range(-90, 90)]
        public float firingAngle;
        public float startSpeed;

        public float collisionIgnoreTime = 0.35f;

        protected bool _fired, _ignoringCollsions;
        protected float _collisionIgnoreCount = 0;
        protected Rigidbody _rigidbody;
        protected List<Collider> _collidersIgnoring = new List<Collider>();

        /// <summary>все коллайдеры этого объекта</summary>
        protected Collider[] _colliders;


        /// <summary>
        /// Запускает этот снаряд из заданной начальной точки в заданную мировую координату.
        /// Автоматически устанавливает угол стрельбы в соответствии со скоростью запуска.
        /// </summary>
        /// <param name="startPoint">Start point of the flight.</param>
        /// <param name="targetPoint">Target point to fly to.</param>
        public virtual void FireAtPoint(Vector3 startPoint, Vector3 targetPoint)
        {
            Vector3 firingVector;
            transform.position = startPoint;

            switch (fireMode)
            {
                case BallisticFireMode.UseLaunchSpeed:
                    firingVector = Ballistics.CalculateBallisticFireVectorFromVelocity(startPoint, targetPoint, startSpeed, arcPreference);
                    firingAngle = Ballistics.CalculateBallisticFireAngle(startPoint, targetPoint, startSpeed, arcPreference);
                    break;
                case BallisticFireMode.UseLaunchAngle:
                    firingVector = Ballistics.CalculateBallisticFireVectorFromAngle(startPoint, targetPoint, firingAngle);
                    startSpeed = firingVector.magnitude;
                    break;
                default: throw new ArgumentOutOfRangeException();
            }

            Fire(firingVector);
        }

        /// <summary>
        /// Запускает этот снаряд в заданном направлении со скоростью пуска.
        /// </summary>
        /// <param name="startPoint">Start point of the flight.</param>
        /// <param name="fireVector">Vector representing launch direction.</param>
        public virtual void FireInDirection(Vector3 startPoint, Vector3 fireVector)
        {
            transform.position = startPoint;
            Fire(fireVector.normalized * startSpeed);
        }

        /// <summary>
        /// Запускает этот снаряд переопределяя начальную скорость.
        /// </summary>
        /// <param name="startPoint">Start point of the flight.</param>
        /// <param name="fireVelocity">Vector3 representing launch velocity.</param>
        public void FireAtVelocity(Vector3 startPoint, Vector3 fireVelocity)
        {
            transform.position = startPoint;
            startSpeed = fireVelocity.magnitude;

            Fire(fireVelocity);
        }

        /// <summary>
        /// Игнорирование столкновений с данными колайдерами в течении заданного времени
        /// </summary>
        /// <param name="collidersToIgnore">Colliders to ignore</param>
        public void IgnoreCollision(Collider[] collidersToIgnore)
        {
            if (collisionIgnoreTime > 0)
            {
                _ignoringCollsions = true;
                _collisionIgnoreCount = 0.0f;
                foreach (Collider colliderToIgnore in collidersToIgnore)
                {
                    if (_collidersIgnoring.Contains(colliderToIgnore)) continue;

                    foreach (Collider projectileCollider in _colliders)
                    {
                        Physics.IgnoreCollision(colliderToIgnore, projectileCollider, true);
                    }

                    _collidersIgnoring.Add(colliderToIgnore);
                }
            }
        }

        protected virtual void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _colliders = GetComponentsInChildren<Collider>();
        }

        protected virtual void Update()
        {
            if (!_fired) return;

            // Если мы игнорируем столкновения, увеличьте счетчик.
            // Если счетчик завершен, то можно снова включить коллизии
            if (_ignoringCollsions)
            {
                _collisionIgnoreCount += Time.deltaTime;
                if (_collisionIgnoreCount >= collisionIgnoreTime)
                {
                    _ignoringCollsions = false;

                    foreach (var colliderIgnoring in _collidersIgnoring)
                    {
                        foreach (var projectileCollider in _colliders)
                        {
                            Physics.IgnoreCollision(colliderIgnoring, projectileCollider, false);
                        }
                    }

                    _collidersIgnoring.Clear();
                }
            }

            transform.rotation = Quaternion.LookRotation(_rigidbody.velocity);
        }

        protected virtual void Fire(Vector3 firingVector)
        {
            transform.rotation = Quaternion.LookRotation(firingVector);

            _rigidbody.velocity = firingVector;

            _fired = true;

            _collidersIgnoring.Clear();

            fired?.Invoke();
        }

#if UNITY_EDITOR
        protected virtual void OnValidate()
        {
            if (Mathf.Abs(firingAngle) >= 90f)
            {
                firingAngle = Mathf.Sign(firingAngle) * 89.5f;
                Debug.LogWarning("Clamping angle to under +- 90 degrees to avoid errors.");
            }
        }
#endif
    }
}