﻿using System;
using UnityEngine;

namespace ActionGameFramework.Projectiles
{
	public interface IProjectile
	{
		event Action fired;

		/// <summary>
		/// Запускает этот снаряд из заданной начальной точки в заданную мировую координату.
		/// </summary>
		/// <param name="startPoint">Start point of the flight.</param>
		/// <param name="targetPoint">Target point to fly to.</param>
		void FireAtPoint(Vector3 startPoint, Vector3 targetPoint);

		/// <summary>
		/// Запускает этот снаряд в определенном направлении.
		/// </summary>
		/// <param name="startPoint">Start point of the flight.</param>
		/// <param name="fireVector">Vector representing direction of flight.</param>
		void FireInDirection(Vector3 startPoint, Vector3 fireVector);

		/// <summary>
		/// Запускает этот снаряд переопределяя начальную скорость.
		/// </summary>
		/// <param name="startPoint">Start point of the flight.</param>
		/// <param name="fireVelocity">Vector3 representing launch velocity.</param>
		void FireAtVelocity(Vector3 startPoint, Vector3 fireVelocity);
	}
}