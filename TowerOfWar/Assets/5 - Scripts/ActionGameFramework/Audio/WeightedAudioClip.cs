﻿using System;
using UnityEngine;

namespace ActionGameFramework.Audio
{
	/// <summary> Звук с определением веса (для вычисления вероятности воспроизведения)< /summary>
	[Serializable]
	public class WeightedAudioClip
	{
		public AudioClip clip;
		public int weight = 1;
	}
}