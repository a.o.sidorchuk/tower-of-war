﻿using UnityEngine;

namespace ActionGameFramework.Audio
{
    /// <summary> Помощник для воспроизведения случайного звука из набора (в соответствии с "весом") </summary>
    [RequireComponent(typeof(AudioSource))]
    public class RandomAudioSource : MonoBehaviour
    {
        [SerializeField] WeightedAudioList _clips;
        [SerializeField] bool _playOnEnabled;

        protected AudioSource _source;

        protected virtual void OnEnable()
        {
            if (_source == null) _source = GetComponent<AudioSource>();

            if (_playOnEnabled) PlayRandomClip();
        }

        public virtual void PlayRandomClip()
        {
            if (_source == null) _source = GetComponent<AudioSource>();

            PlayRandomClip(_source);
        }

        public virtual void PlayRandomClip(AudioSource source)
        {
            if (source == null) return; 

            var clip = _clips.WeightedSelection();
            if (clip == null) return; 

            source.clip = clip;
            source.Play();
        }
    }
}