﻿using System;
using System.Linq;
using Core.Extensions;
using UnityEngine;

namespace ActionGameFramework.Audio
{
    /// <summary> Список аудио воиспроизводимый в случаном порядке </summary>
    [Serializable]
    public class WeightedAudioList
    {
        public WeightedAudioClip[] weightedItems;

        protected int m_WeightSum = -1;

        public int WeightSum
        {
            get
            {
                if (m_WeightSum < 0) CalculateWeightSum();
                return m_WeightSum;
            }
        }

        public AudioClip WeightedSelection()
        {
            if (weightedItems.Length == 0) return null;

            return weightedItems.WeightedSelection(WeightSum, t => t.weight).clip;
        }

        protected void CalculateWeightSum() => m_WeightSum = weightedItems.Sum(x => x.weight);
    }
}