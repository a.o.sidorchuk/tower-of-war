﻿using System.Collections.Generic;
using Core.Health;
using UnityEngine;

namespace ActionGameFramework.Audio
{
    /// <summary> Помощник для воспроизведения звуков при изменении состояния здоровья </summary>
    [RequireComponent(typeof(AudioSource))]
    public class HealthChangeAudioSource : MonoBehaviour
    {
        [SerializeField] HealthChangeSoundSelector soundSelector;

        protected AudioSource _source;

        protected virtual void Awake()
        {
            _source = GetComponent<AudioSource>();
        }

        public virtual void PlaySound() => _source.Play();

        public virtual void PlayHealthChangeSound(HealthChangeInfo info)
        {
            if (soundSelector != null && soundSelector.IsSetUp)
            {
                var newClip = soundSelector.GetClipFromHealthChangeInfo(info);
                if (newClip != null) _source.clip = newClip;
            }

            _source.Play();
        }

        public void Sort() => soundSelector?.healthChangeSounds?.Sort(new HealthChangeSoundComparer());
    }

    public class HealthChangeSoundComparer : IComparer<HealthChangeSound>
    {
        public int Compare(HealthChangeSound first, HealthChangeSound second)
        {
            if (first.healthChange == second.healthChange) return 0;
            if (first.healthChange < second.healthChange) return -1;
            return 1;
        }
    }
}