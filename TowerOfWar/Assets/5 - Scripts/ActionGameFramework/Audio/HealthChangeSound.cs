﻿using System;
using UnityEngine;

namespace ActionGameFramework.Audio
{
	/// <summary> Звук изменения здоровья на указоном промежутке </summary>
	[Serializable]
	public class HealthChangeSound
	{
		[Tooltip("Пороговые значения должны быть в порядке возрастания")]
		public float healthChange;
		public AudioClip sound;
	}
}