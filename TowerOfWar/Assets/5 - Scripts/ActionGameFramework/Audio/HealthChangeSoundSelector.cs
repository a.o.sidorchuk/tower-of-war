﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Health;
using UnityEngine;

namespace ActionGameFramework.Audio
{
	[Serializable]
	public class HealthChangeSoundSelector
	{
		[Tooltip("Пороговые значения должны быть в порядке возрастания")]
		public List<HealthChangeSound> healthChangeSounds;

		public bool IsSetUp => healthChangeSounds.Count > 0;

		public virtual AudioClip GetClipFromHealthChangeInfo(HealthChangeInfo info)
		{
			return healthChangeSounds.FirstOrDefault(x => info.AbsHealthDifference <= x.healthChange)?.sound;
		}
	}
}