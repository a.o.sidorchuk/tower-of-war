﻿using UnityEditor;
using UnityEngine;

namespace ActionGameFramework.Audio.Editor
{
	/// <summary> Сортировка звуков в редакторе по значению озвучиваемого здоровья </summary>
	[CustomEditor(typeof(HealthChangeAudioSource))]
	public class HealthChangeAudioSourceEditor : UnityEditor.Editor
	{
		protected const string HELP_MSG =
			"This list needs to be sorted in order " +
			"for sounds to be played correctly" +
			"\nList will sort automatically when this component is deselected." +
			"\nYou can also press the \'Sort\' button once you are done editing the sound list.";

		protected HealthChangeAudioSource _source;

		protected void OnEnable() => _source = target as HealthChangeAudioSource;

		protected void OnDisable() => Sort();

		protected void Sort()
		{
			if (_source != null)
			{
				_source.Sort();
				EditorUtility.SetDirty(_source);
			}
		}

		public override void OnInspectorGUI()
		{
			EditorGUILayout.HelpBox(HELP_MSG, MessageType.Info);

			base.OnInspectorGUI();

			if (GUILayout.Button("Sort")) Sort(); 
		}
	}
}