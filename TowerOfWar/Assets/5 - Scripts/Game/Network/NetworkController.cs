﻿using Photon.Pun;
using Core.Utilities;
using UnityEngine;
using Game.Data.Level;
using Core.Network;
using Photon.Realtime;
using ExitGames.Client.Photon;
using Core.SceneControl;
using Game.Data;
using Newtonsoft.Json;
using Data.Models.Level;
using System.Linq;
using UniRx;
using System;
using UniRx.Async;
using UnityEngine.AddressableAssets;
using System.Collections.Generic;
using Data.Models.Player;

namespace Game.Network
{
    /// <summary>
    /// Основной контроллер для работы с Photon Network
    /// </summary>
    /// <remarks>
    /// Обеспечивает все виды подключений и инициализаций необходимых для запуска игровой сессии.
    /// Используется так же и для сингл версии игры.
    /// </remarks>
    [RequireComponent(typeof(PunCallbacksWrapper))]
    public class NetworkController : MonoSingleton<NetworkController>
    {
        public class HashKey
        {
            // room props
            public const string levelId = "lid";
            public const string levelState = "ls";
            public const string levelStartTime = "lst";
            public const string squadNames = "sn";
            // player props
            public const string squad = "s";
            public const string fraction = "f";
            public const string playerState = "ps";
        }

        PunCallbacksWrapper _punCallbacks;

        ContentLevelInfo _currentLevelInfo;
        GameSessionData _currentGameSession;

        float _startGameTime;
        public float Time => Convert.ToSingle(PhotonNetwork.Time) - _startGameTime;

        protected override void Awake()
        {
            base.Awake();

            PhotonNetwork.PrefabPool = new PunPrefabPool();
            _punCallbacks = GetComponent<PunCallbacksWrapper>();

            _punCallbacks.onConnectedToServer += ConnectedToServerEvent;

            _punCallbacks.onJoinedRoom += JoinedRoomEvent;
            _punCallbacks.onLeftRoom += LeftRoomEvent;
            _punCallbacks.onJoinRandomFailed += JoinRandomFailedEvent;

            _punCallbacks.onPlayerEnteredRoom += PlayerEnteredRoomEvent;
            _punCallbacks.onPlayerLeftRoom += PlayerLeftRoomEvent;

            _punCallbacks.onRoomPropertiesUpdate += RoomPropertiesUpdateEvent;
            _punCallbacks.onPlayerPropertiesUpdate += PlayerPropertiesUpdateEvent;

            MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.LoadLevelComplete)
                .Subscribe(LoadGameLevelCompleteEvent);
        }


        public void CreateDungeonRoom() => Connect(offlineMode: true);
        public void CreateOrJoinArenaRoom() => Connect(offlineMode: false);

        private void Connect(bool offlineMode)
        {
            Debug.Log($"<b>[NetworkController]</b>: Connect (mode: {offlineMode})");

            _currentLevelInfo = GameManager.Instance.CurrentLevelInfo;
            _currentGameSession = GameManager.Instance.CurrentGameSession;

            PhotonNetwork.AutomaticallySyncScene = false;

            // _TODO подставить пользовательское имя из Store
            var localUser = Social.localUser;
            PhotonNetwork.LocalPlayer.NickName = localUser.authenticated ?
                localUser.userName : $"<Player_{UnityEngine.Random.Range(10, 99)}>";

            var squadData = GameManager.Instance.Store.PlayerData.CurrentFractionProgress.CurrentSquadUnits;
            var squadJson = JsonConvert.SerializeObject(squadData);

            UpdatePlayerProperty(new Hashtable
            {
                { HashKey.squad, squadJson },
                { HashKey.fraction, (int)GameManager.Instance.Store.PlayerData.FractionType },
                { HashKey.playerState, PlayerState.None }
            });

            if (offlineMode)
                PhotonNetwork.OfflineMode = true;
            else
                PhotonNetwork.ConnectUsingSettings(); // _TODO добавить проверки если подключение не удалось

            _startGameTime = 0;
        }

        private void ConnectedToServerEvent()
        {
            if (PhotonNetwork.OfflineMode)
            {
                PhotonNetwork.CreateRoom(null, new RoomOptions
                {
                    MaxPlayers = 1,
                    CustomRoomProperties = new Hashtable
                    {
                        { HashKey.levelId, _currentLevelInfo.Id },
                        { HashKey.levelState, _currentGameSession.LevelState },
                        { HashKey.levelStartTime, 0 }
                    }
                });
            }
            else
            {
                var userLevel = GameManager.Instance.Store.PlayerData.CurrentLevel;

                // фильтрация комнат игроков в диапазоне +/- 1 уровня от текущего
                var sqlLobby = new TypedLobby("myLobby", LobbyType.SqlLobby);
                var sqlLobbyFilter = $"C0 < {userLevel + 2} AND C0 > {userLevel - 2}";

                PhotonNetwork.JoinRandomRoom(null, 0, MatchmakingMode.SerialMatching, sqlLobby, sqlLobbyFilter);
            }
        }

        private void JoinRandomFailedEvent(short code, string message)
        {
            var userLevel = GameManager.Instance.Store.PlayerData.CurrentLevel;
            var sqlLobby = new TypedLobby("myLobby", LobbyType.SqlLobby);
            var roomOptions = new RoomOptions
            {
                CustomRoomProperties = new Hashtable()
                {
                    { "C0", userLevel },
                    { HashKey.levelId, _currentLevelInfo.Id },
                    { HashKey.levelState, _currentGameSession.LevelState },
                    { HashKey.squadNames, "" },
                    { HashKey.levelStartTime, 0 }
                },
                CustomRoomPropertiesForLobby = new string[] { "C0" }
            };

            PhotonNetwork.CreateRoom(null, roomOptions, sqlLobby);
        }

        private void JoinedRoomEvent()
        {
            Debug.Log($"<b>[NetworkController]</b>: JoinedRoomEvent");

            if (PhotonNetwork.LocalPlayer.IsMasterClient && PhotonNetwork.OfflineMode)
            {
                var squadNamesJson = JsonConvert.SerializeObject(new Hashtable
                {
                    { PhotonNetwork.LocalPlayer.ActorNumber, (int)SquadName.A }
                });

                UpdateRoomProperty(new Hashtable
                {
                    { HashKey.levelState, LevelState.CheckReady },
                    { HashKey.squadNames, squadNamesJson }
                });
            }
        }

        private void LeftRoomEvent()
        {
            Debug.Log($"<b>[NetworkController]</b>: LeftRoomEvent");

            PhotonNetwork.Disconnect();

            // удаление всех объектов пула перед выходом из игровой сцены
            if (PoolManager.InstanceExists) Destroy(PoolManager.Instance.gameObject);

            if (_currentGameSession.LevelState == LevelState.Intro ||
                _currentGameSession.LevelState == LevelState.Started ||
                _currentGameSession.LevelState == LevelState.Finished)
            {
                SceneController.Instance.StartMenu();
            }
        }

        private void PlayerEnteredRoomEvent(Player newPlayer)
        {
            Debug.Log($"<b>[NetworkController]</b>: PlayerEnteredRoomEvent - {newPlayer.ActorNumber}:{newPlayer.NickName}");

            if (PhotonNetwork.LocalPlayer.IsMasterClient)
            {
                var players = PhotonNetwork.CurrentRoom.Players;
                if (PhotonNetwork.LocalPlayer.IsMasterClient && players.Count == 2)
                {
                    PhotonNetwork.CurrentRoom.IsOpen = false;

                    var squadNamesJson = JsonConvert.SerializeObject(new Hashtable
                    {
                        { players.ElementAt(0).Value.ActorNumber, SquadName.A },
                        { players.ElementAt(1).Value.ActorNumber, SquadName.B }
                    });

                    UpdateRoomProperty(new Hashtable
                    {
                        { HashKey.levelState, LevelState.CheckReady },
                        { HashKey.squadNames, squadNamesJson }
                    });
                }
            }
        }

        private void PlayerLeftRoomEvent(Player otherPlayer)
        {
            Debug.Log($"<b>[NetworkController]</b>: PlayerLeftRoomEvent - gs:{_currentGameSession.LevelState} - p:{otherPlayer.ActorNumber}:{otherPlayer.NickName}");

            if (_currentGameSession.LevelState == LevelState.CheckReady)
            {
                UpdateRoomProperty(HashKey.levelState, LevelState.SearchMode);
            }
            else if (_currentGameSession.LevelState == LevelState.Started ||
                     _currentGameSession.LevelState == LevelState.Intro)
            {
                // _TODO если другой игрок вышел до окончания матча 
                // => завершить матч с авто-победой для оставшегося игрока
                PhotonNetwork.LeaveRoom();
            }
        }

        private void PlayerPropertiesUpdateEvent(Player player, Hashtable properties)
        {
            Debug.Log($"<b>[NetworkController]</b>: PlayerPropertiesUpdateEvent - p:{player.ActorNumber}:{player.NickName}");

            // если был изменен параметр состояния игрока
            if (properties.TryGetValue(HashKey.playerState, out object playerState))
            {
                var data = new Tuple<int, PlayerState>(player.ActorNumber, (PlayerState)playerState);
                MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.PlayerStateChange, data, null));
            }

            // проверяем готовности игроков
            if (PhotonNetwork.LocalPlayer.IsMasterClient)
            {
                var players = PhotonNetwork.CurrentRoom.Players;

                switch (_currentGameSession.LevelState)
                {
                    case LevelState.CheckReady:
                        // проверка статуса готовности всех игроков в комнате
                        var readyPlayerCount = players
                            .Count(x => (PlayerState)x.Value.CustomProperties[HashKey.playerState] == PlayerState.Ready);

                        if (readyPlayerCount == players.Count)
                        {
                            UpdateRoomProperty(HashKey.levelState, LevelState.Ready);
                        }
                        break;
                    case LevelState.Ready:
                        // проверка статуса предварительной загрузки ресурсов
                        var preLoadedPlayerCount = players
                            .Count(x => (PlayerState)x.Value.CustomProperties[HashKey.playerState] == PlayerState.PreLoaded);

                        if (preLoadedPlayerCount == players.Count)
                        {
                            UpdateRoomProperty(HashKey.levelState, LevelState.PreLoaded);
                        }
                        break;
                    case LevelState.Intro:
                        // проверка статуса загрузки уровней всех игроков в комнате
                        var loadedPlayerCount = players
                            .Count(x => (PlayerState)x.Value.CustomProperties[HashKey.playerState] == PlayerState.Loaded);

                        if (loadedPlayerCount == players.Count)
                        {
                            UpdateRoomProperty(new Hashtable
                            {
                                { HashKey.levelState, LevelState.Started },
                                { HashKey.levelStartTime, Convert.ToSingle(PhotonNetwork.Time) }
                            });
                        }
                        break;
                }
            }
        }

        private void RoomPropertiesUpdateEvent(Hashtable properties)
        {
            Debug.Log($"<b>[NetworkController]</b>: RoomPropertiesUpdateEvent");

            if (properties.TryGetValue(HashKey.levelState, out object value))
            {
                var levelState = (LevelState)value;
                var oldLevelState = _currentGameSession.LevelState;

                Debug.Log($"<b>[NetworkController]</b>: RoomPropertiesUpdateEvent - ls: {oldLevelState} -> {levelState}");

                switch (levelState)
                {
                    case LevelState.SearchMode:
                        if (oldLevelState == LevelState.CheckReady)
                        {
                            PhotonNetwork.CurrentRoom.IsOpen = true;
                        }

                        SetState(LevelState.SearchMode);
                        break;
                    case LevelState.CheckReady:
                        if (properties.TryGetValue(HashKey.squadNames, out object squadNames))
                        {
                            var table = JsonConvert.DeserializeObject<Hashtable>(squadNames.ToString());

                            table.TryGetValue(PhotonNetwork.LocalPlayer.ActorNumber.ToString(), out object squadIdObj);

                            var squadId = Convert.ToInt32(squadIdObj);

                            _currentGameSession.SetSquadId(squadId);
                        }

                        SetState(LevelState.CheckReady);

                        if (PhotonNetwork.OfflineMode)
                        {
                            UpdatePlayerProperty(HashKey.playerState, PlayerState.Ready);
                        }
                        break;
                    case LevelState.Ready:
                        // Создание пула объектов у всех клиентов (_TODO вынести в другое место)
                        UniTask.ToCoroutine(async () =>
                        {
                            try
                            {
                                var poolManagerPrefab = await AddressableExtensions
                                    .CustomLoadAssetAsync<GameObject>("PoolManager");

                                Instantiate(poolManagerPrefab);

                                // создать пул объектов
                                var playerData = GameManager.Instance.Store.PlayerData;
                                var poolables = new List<Poolable>();
                                var unitPrefabs = new List<AssetReferenceGameObject>();

                                unitPrefabs.AddRange(GameManager.Instance.GetUnitPrefabs(playerData.CurrentFractionProgress.CurrentSquad.Units, playerData.FractionType));

                                if (PhotonNetwork.OfflineMode)
                                {
                                    unitPrefabs.AddRange(GameManager.Instance.CurrentLevelInfo.GetEnemyUnitPrefabs());
                                }
                                else
                                {
                                    var opponent = PhotonNetwork.CurrentRoom.Players.FirstOrDefault(x => x.Key != PhotonNetwork.LocalPlayer.ActorNumber).Value;

                                    var opponentSquad = JsonConvert.DeserializeObject<Dictionary<int, int>>(opponent.CustomProperties[HashKey.squad].ToString());
                                    var opponentFraction = (int)opponent.CustomProperties[HashKey.fraction];

                                    unitPrefabs.AddRange(GameManager.Instance.GetUnitPrefabs(opponentSquad.Keys.ToList(), (FractionType)opponentFraction));
                                }

                                var uniqueUnitPrefabs = unitPrefabs.Distinct(new AssetReferenceComparer()).ToList();

                                foreach (var prefab in uniqueUnitPrefabs)
                                {
                                    var go = await prefab.CustomLoadAssetAsync<GameObject>();
                                    go.name = prefab.RuntimeKey.ToString();

                                    var poolHelpers = go.GetComponentsInChildren<IPoolHelper>();
                                    foreach (var poolHelper in poolHelpers)
                                    {
                                        poolables.AddRange(poolHelper.GetPoolables());
                                    }
                                }

                                poolables = poolables.Distinct(new PoolableComparer()).ToList();

                                PoolManager.Instance.Init(poolables);

                                SetState(LevelState.Ready);

                                UpdatePlayerProperty(HashKey.playerState, PlayerState.PreLoaded);
                            }
                            catch (Exception ex)
                            {
                                Debug.LogError(ex);
                                PhotonNetwork.LeaveRoom();
                            }
                        });
                        break;
                    case LevelState.PreLoaded:

                        SetState(LevelState.PreLoaded);

                        if (PhotonNetwork.LocalPlayer.IsMasterClient)
                        {
                            UpdateRoomProperty(HashKey.levelState, LevelState.Intro);
                        }
                        break;
                    case LevelState.Intro:
                        SetState(LevelState.Intro);

                        SceneController.Instance.StartGame(_currentLevelInfo);
                        break;
                    case LevelState.Started:
                        properties.TryGetValue(HashKey.levelStartTime, out object time);
                        _startGameTime = Convert.ToSingle(time);

                        SetState(LevelState.Started);
                        break;
                    case LevelState.Finished:
                        SetState(LevelState.Finished);
                        break;
                }
            }
        }

        private void LoadGameLevelCompleteEvent(GameMessage obj)
        {
            Debug.Log($"<b>[NetworkController]</b>: LoadGameLevelCompleteEvent");

            UpdatePlayerProperty(HashKey.playerState, PlayerState.Loaded);
        }

        private void SetState(LevelState levelState)
        {
            Debug.Log($"<b>[NetworkController]</b>: SetState - {levelState}");

            _currentGameSession.SetLevelState(levelState);
            MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.LevelStateChange, levelState, null));
        }

        public void UpdatePlayerProperty(Hashtable hashtable) => PhotonNetwork.LocalPlayer.SetCustomProperties(hashtable);
        public void UpdateRoomProperty(Hashtable hashtable) => PhotonNetwork.CurrentRoom.SetCustomProperties(hashtable);
        public void UpdatePlayerProperty(object key, object value) => UpdatePlayerProperty(new Hashtable { { key, value } });
        public void UpdateRoomProperty(object key, object value) => UpdateRoomProperty(new Hashtable { { key, value } });
    }

    class AssetReferenceComparer : IEqualityComparer<AssetReferenceGameObject>
    {
        public bool Equals(AssetReferenceGameObject x, AssetReferenceGameObject y) => x.RuntimeKey.Equals(y.RuntimeKey);
        public int GetHashCode(AssetReferenceGameObject x) => x.RuntimeKey.GetHashCode();
    }
    class PoolableComparer : IEqualityComparer<Poolable>
    {
        public bool Equals(Poolable x, Poolable y) => x.name.Equals(y.name);
        public int GetHashCode(Poolable x) => x.name.GetHashCode();
    }
}