﻿using System;
using Core.Data;
using Data.Models.Level;
using Data.Models.Player;
using Game.Data.Units;
using Newtonsoft.Json;
using UniRx;

namespace Game.Data
{
    public class GameStore : BaseGameStore
    {
        [JsonProperty] public DateTime? LastSaveDate { get; private set; }
        [JsonProperty] public PlayerData PlayerData { get; private set; } = new PlayerData();

        [JsonIgnore] public GameSessionData GameSessionData { get; private set; }

        public void UpdateLastSaveDate() => LastSaveDate = DateTime.UtcNow;
        public bool IsActualSave(DateTime lastDate) => LastSaveDate.HasValue ? LastSaveDate.Value >= lastDate : false;

        public void CreateGameSession() => GameSessionData = new GameSessionData();

        public bool TryIncreaseUnitLevel(UnitViewItem unitViewItem)
        {
            var result = PlayerData.Gold.TryPurchase(unitViewItem.CurrentTrainingCost);

            if (result)
            {
                var newUnit = unitViewItem.Level == 0;

                unitViewItem.IncreaseUnitLevel();
                PlayerData.IncreaseUnitLevel(unitViewItem.UnitId);

                if (newUnit)
                {
                    MessageBroker.Default.Publish(MenuMessage.Create(MenuMessage.Type.RecruitUnitEvent, unitViewItem, null));
                }
            }

            return result;
        }
    }
}
