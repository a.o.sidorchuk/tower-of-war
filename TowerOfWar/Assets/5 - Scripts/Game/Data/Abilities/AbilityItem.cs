﻿using Data.Models.Abilities;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.Data.Ability
{
    [CreateAssetMenu(menuName = "Abilities/Ability Config", order = 1)]
    public class AbilityItem : AbilityData
    {
        [Space]
        [SerializeField] string _title;
        [SerializeField] [TextArea(3, 7)] string _description;
        [SerializeField] AssetReferenceSprite _iconSprite;

        #region public fields
        public string Title => _title;
        public string Description => _description;
        public AssetReferenceSprite IconSprite => _iconSprite;
        #endregion
    }
}