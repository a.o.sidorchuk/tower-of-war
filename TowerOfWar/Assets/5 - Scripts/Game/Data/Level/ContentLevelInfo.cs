﻿using Core.SceneControl;
using Data.Models.Player;
using Game.Data.Units;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.Data.Level
{
    [CreateAssetMenu(menuName = "Levels/Content Level", order = 1)]
    public class ContentLevelInfo : SceneInfo
    {
        [SerializeField] string _title;
        [SerializeField] [TextArea(3, 7)] string _description;
        [Tooltip("Cписок юнитов для подземелий")]
        [SerializeField] List<UnitItem> _enemies = new List<UnitItem>();

        #region public fields
        public string Title => _title;
        public string Description => _description;
        public List<UnitItem> Enemies => _enemies;
        #endregion

        public List<AssetReferenceGameObject> GetEnemyUnitPrefabs()
        {
            var result = new List<AssetReferenceGameObject>();

            foreach (var unit in _enemies)
            {
                result.Add(unit.BasePrefab);
                result.Add(unit.UnitDescriptionByFractionList[FractionType.None].VisualPrefab);
            }

            return result;
        }

        public List<UnitViewItem> GetEnemyUnitViewItems(int playerLevel)
        {
            var minLevel = playerLevel - 1;
            var maxLevel = playerLevel + 1;

            if (minLevel < 1) minLevel = 1;

            return _enemies
                .Select(unit => new UnitViewItem(Random.Range(minLevel, maxLevel), unit, FractionType.None))
                .ToList();
        }
    }
}