﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Data.Level
{
	[CreateAssetMenu(fileName = "LevelList", menuName = "Levels/Level List", order = 3)]
	public class LevelList : ScriptableObject
	{
		[SerializeField] List<ContentLevelInfo> _levels = new List<ContentLevelInfo>();

		public List<ContentLevelInfo> Levels => _levels;

		public int Count => _levels.Count;

		public ContentLevelInfo GetRandomLevel() => _levels[Random.Range(0, Count)];

		public ContentLevelInfo this[int id] => _levels.FirstOrDefault(x => x.Id == id);
	}
}