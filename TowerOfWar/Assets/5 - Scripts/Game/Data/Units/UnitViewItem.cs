﻿using Data.Models.Player;
using Data.Models.Units;
using System;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.Data.Units
{
    public class UnitViewItem
    {
        public event Action onLevelUp;

        UnitItem _unit;
        UnitDescriptionItem _unitDescription;
        FractionType _currentFractionType;

        public int UnitId => _unit.Id;
        public UnitType UnitType => _unit.UnitType;

        public int Cost => _unit.Cost;
        public int RecruitsNumber => _unit.RecruitsNumber;
        public ResourcesType ResourcesType => _unit.ResourcesType;
        public RarityType RarityType => _unit.RarityType;
        public MovementSpeedType SpeedType => _unit.MovementSpeedType;

        public float ReloadingTime => _unit.ReloadingTime;

        public AssetReferenceSprite IconSprite => UnitDescription.UnitIconSprite;
        public AssetReferenceGameObject VisualPrefab => UnitDescription.VisualPrefab;
        public AssetReferenceGameObject BasePrefab => _unit.BasePrefab;


        public int Level { get; private set; }

        public int CurrentHealth { get; private set; }
        public int CurrentStrength { get; private set; }

        public int CurrentCriticalStrike { get; private set; }
        public int CurrentCriticalChance { get; private set; }
        public int CurrentTrainingCost { get; private set; }

        public UnitDescriptionItem UnitDescription
        {
            get
            {
                if (_unitDescription == null)
                    _unitDescription = _unit.UnitDescriptionByFractionList[_currentFractionType];

                return _unitDescription;
            }
        }

        public UnitViewItem(int level, UnitItem unit, FractionType playerFraction)
        {
            Level = level;

            _unit = unit;
            _currentFractionType = playerFraction;

            CalcParameters();
        }

        public void IncreaseUnitLevel()
        {
            Level++;

            CalcParameters();

            onLevelUp?.Invoke();
        }

        private void CalcParameters()
        {
            CurrentHealth = _unit.Health;
            CurrentStrength = _unit.Strength;
            CurrentCriticalStrike = _unit.CriticalStrike;
            CurrentTrainingCost = _unit.TrainingCost;

            if (Level > 1)
            {
                var coef1 = Mathf.Pow(1.05f, Level - 1);
                CurrentHealth = Mathf.RoundToInt(coef1 * CurrentHealth);
                CurrentStrength = Mathf.RoundToInt(coef1 * CurrentStrength);

                CurrentTrainingCost = Mathf.RoundToInt(Level * CurrentTrainingCost);

                var coef2 = Mathf.Pow(1.25f, Level - 1);
                CurrentCriticalStrike = Mathf.RoundToInt(coef2 * CurrentCriticalStrike);
                CurrentCriticalChance = Mathf.RoundToInt(0.08f + CurrentCriticalStrike / 2000f);
            }
        }

        public bool UnitTypeCheck(UnitType unitType) => _unit.UnitTypeCheck(unitType);
    }
}