﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Data.Units
{
	[CreateAssetMenu(fileName = "UnitList", menuName = "Units/Unit List", order = 3)]
	public class UnitList : ScriptableObject
	{
		[SerializeField] List<UnitItem> _units = new List<UnitItem>();

		public List<UnitItem> Units => _units;

		public int Count => _units.Count;

		public UnitItem this[int id] => _units.FirstOrDefault(x => x.Id == id);
	}
}