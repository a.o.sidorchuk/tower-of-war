﻿using Data.Models.Player;
using Data.Models.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.Data.Units
{
    [CreateAssetMenu(menuName = "Units/Unit Config", order = 1)]
    public class UnitItem : UnitData
    {
        [SerializeField] AssetReferenceGameObject _basePrefab;
        [Space]
        [Header("Описание юнита по фракциям")]
        [SerializeField] UnitDescriptionByFractionList _unitDescriptionByFractionList = new UnitDescriptionByFractionList();

        #region public fields
        public UnitDescriptionByFractionList UnitDescriptionByFractionList => _unitDescriptionByFractionList;
        public AssetReferenceGameObject BasePrefab => _basePrefab;
        #endregion
    }

    /// <summary>
    /// Элемент для описания юнита принадлежащего определенной фракции
    /// </summary>
    [Serializable]
    public class UnitDescriptionItem
    {
        [SerializeField] FractionType _fractionType;
        [Space]
        [SerializeField] string _title;
        [SerializeField] [TextArea(3, 7)] public string _description;
        [Space]
        [SerializeField] AssetReferenceSprite _unitIconSprite;
        [SerializeField] AssetReferenceGameObject _visualPrefab;

        #region public fields
        public FractionType FractionType => _fractionType;
        public string Title => _title;
        public string Description => _description;
        public AssetReferenceSprite UnitIconSprite => _unitIconSprite;
        public AssetReferenceGameObject VisualPrefab => _visualPrefab;
        #endregion
    }

    /// <summary>
    /// Элемент для хранения описания юнита для разных фракций
    /// </summary>
    [Serializable]
    public class UnitDescriptionByFractionList
    {
        [SerializeField] List<UnitDescriptionItem> _unitDescriptions = new List<UnitDescriptionItem>();
        public UnitDescriptionItem this[FractionType fractionType] => _unitDescriptions.FirstOrDefault(x => x.FractionType == fractionType);
        public bool ContainsFraction(FractionType fractionType) => _unitDescriptions.Any(x => x.FractionType == fractionType);
    }
}