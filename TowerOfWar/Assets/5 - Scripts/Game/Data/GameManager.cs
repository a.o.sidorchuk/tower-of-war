﻿using Core.Data;
using Core.SceneControl;
using Data.Models.Level;
using Data.Models.Player;
using Game.Data.Level;
using Game.Data.Units;
using Game.Network;
using System.Collections.Generic;
using System.Linq;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.Data
{
    public class GameManager : BaseGameManager<GameManager, GameStore>
    {
        [Header("Levels")]
        [SerializeField] LevelList _dungeons;
        [SerializeField] LevelList _arenas;

        [Header("Units")]
        [SerializeField] UnitList _units;

        #region public feilds
        public LevelList Dungeons => _dungeons;
        public LevelList Arenas => _arenas;

        public UnitList Units => _units;
        public List<UnitViewItem> UnitViewItems { get; private set; }

        public ContentLevelInfo CurrentLevelInfo { get; private set; }
        public GameSessionData CurrentGameSession => Store.GameSessionData;
        #endregion

        protected override async UniTask LoadData()
        {
            await base.LoadData();

            if (Social.localUser.authenticated)
            {
                // _TODO включить сохранение в облако

                //var data = await GooglePlayUtils.ReadData<GameStore>(_userDataFileName);

                //if (!_store.IsActualSave(data.LastSaveDate.Value))
                //{
                //    _store = data;
                //}

                // _TODO запись ника из социалки в Store и его сохранение
            }

            // кеширование юнитов
            UnitViewItemsInit();
        }

        public async UniTask GetReward(bool isWinner)
        {
            var gameSession = Store.GameSessionData;

            Store.PlayerData.Gold.AddCurrency(isWinner ? gameSession.GoldDrop.CurrentCurrency : 0);

            var exp = Mathf.RoundToInt(isWinner ? gameSession.ExpDrop.CurrentCurrency : gameSession.ExpDrop.CurrentCurrency * 0.05f);
            Store.PlayerData.IncreasePlayerLevel(exp);

            await SaveData();
        }

        public override async UniTask SaveData()
        {
            _store.UpdateLastSaveDate();

            // _TODO включить сохранение в облако
            // await GooglePlayUtils.SaveData(_store, _userDataFileName, TimeSpan.FromTicks(_store.LastSaveDate.Value.Ticks));

            await base.SaveData();
        }

        public void UnitViewItemsInit() => UnitViewItems = GetAllUnits();

        public async UniTask CreateOrSelectFraction(FractionType fractionType)
        {
            Store.PlayerData.CreateOrSelectFraction(fractionType);

            UnitViewItemsInit();

            await SaveData();
        }

        public async UniTask SaveNewSquad(List<int> newUnits)
        {
            Store.PlayerData.CurrentFractionProgress.SetSuadUnits(newUnits);
            await SaveData();
        }

        public List<AssetReferenceGameObject> GetUnitPrefabs(List<int> squadUnits, FractionType fractionType)
        {
            var result = new List<AssetReferenceGameObject>();
            var units = Units.Units.Where(x => squadUnits.Contains(x.Id)).ToList();

            foreach (var unit in units)
            {
                result.Add(unit.BasePrefab);
                result.Add(unit.UnitDescriptionByFractionList[fractionType].VisualPrefab);
            }

            return result;
        }

        public List<UnitViewItem> GetUnitViewItems(Dictionary<int, int> squadUnits, FractionType fractionType)
        {
            return Units.Units.Where(x => squadUnits.ContainsKey(x.Id)).Select(unit =>
            {
                squadUnits.TryGetValue(unit.Id, out int unitLevel);
                return new UnitViewItem(unitLevel, unit, fractionType);
            }).ToList();
        }

        public List<UnitViewItem> GetAllUnits()
        {
            var fractionType = Store.PlayerData.FractionType;
            var playerUnits = Store.PlayerData.CurrentFractionProgress.OpenedUnits;

            return Units.Units
                .Where(x => x.UnitDescriptionByFractionList.ContainsFraction(fractionType))
                .Select(unit =>
                {
                    playerUnits.TryGetValue(unit.Id, out int unitLevel);
                    return new UnitViewItem(unitLevel, unit, fractionType);
                }).ToList();
        }

        public List<UnitViewItem> GetOpenedUnits()
        {
            var playerUnits = Store.PlayerData.CurrentFractionProgress.OpenedUnits;
            return UnitViewItems.Where(x => playerUnits.ContainsKey(x.UnitId)).ToList();
        }

        public List<UnitViewItem> GetSquadUnits()
        {
            var currenSquadUnits = Store.PlayerData.CurrentFractionProgress.CurrentSquadUnits;
            return UnitViewItems.Where(x => currenSquadUnits.ContainsKey(x.UnitId)).ToList();
        }

        public void UpdateLevelState(LevelState levelState)
        {
            NetworkController.Instance.UpdateRoomProperty(NetworkController.HashKey.levelState, levelState);
        }

        public void CompleteLevel(int levelId, FinishCriteria criterias)
        {
            Store.PlayerData.InsertOrUpdateLevelProgress(levelId, criterias);

            UniTask.ToCoroutine(SaveData);
        }

        public void CreateDungeonGameSession(ContentLevelInfo levelInfo) => UniTask.ToCoroutine(async () =>
        {
            CurrentLevelInfo = levelInfo;

            await ScreenFader.FadeSceneOut(FadeType.Loading);

            Store.CreateGameSession();

            NetworkController.Instance.CreateDungeonRoom();
        });

        public void CreateArenaGameSession() => UniTask.ToCoroutine(async () =>
        {
            CurrentLevelInfo = _arenas.GetRandomLevel();

            Store.CreateGameSession();

            NetworkController.Instance.CreateOrJoinArenaRoom();
        });
    }
}