﻿using Data.Models.Effects;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.Data.Effects
{
    [CreateAssetMenu(menuName = "Abilities/Effect Config", order = 2)]
    public class EffectItem : EffectData
    {
        [Space]
        [SerializeField] string _title;
        [SerializeField] [TextArea(3, 7)] string _description;
        [SerializeField] AssetReferenceSprite _iconSprite;

        #region public fields
        public string Title => _title;
        public string Description => _description;
        public AssetReferenceSprite IconSprite => _iconSprite;
        #endregion
    }
}