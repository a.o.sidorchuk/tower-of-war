﻿using ActionGameFramework.Health;
using Core.Utilities;
using Data.Models.Abilities;
using Game.Data.Units;
using Game.Units.Effects;
using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Units.Abilities
{
    /// <summary>
    /// Базовый компоненит для реализации способностей юнитов
    /// </summary>
    public abstract class BaseAbility : Damager, IPoolHelper
    {
        [SerializeField] GameObject _abilityEffectPrefab;
        [Space]
        [SerializeField] GameObject _collisionParticlesPrefab;
        [SerializeField] [Range(0, 1)] float _chanceToSpawnCollisionPrefab = 1.0f;
        PhotonView _photonView;

        protected UnitViewItem CasterData { get; set; }
        protected AbilityData AbilityData { get; set; }

        public override float Damage => CasterData.CurrentStrength;// _TODO добавить расчет крита

        public int MaxTargetCount => AbilityData.TargetCount;
        public float Range => AbilityData.Range;
        public float SplashRadius => AbilityData.Raduis;

        protected virtual void Awake() => _photonView = GetComponent<PhotonView>();

        public bool DirectionTypeCheck(DirectionType type) => AbilityData.DirectionCheck(type);

        public void SetData(UnitViewItem unitCasterData, AbilityData abilityData)
        {
            CasterData = unitCasterData;
            AbilityData = abilityData;
        }

        public virtual bool ApplyAbility(Targetable target)
        {
            var result = true;

            switch (AbilityData.ImpactType)
            {
                // _TODO воспроизвести анимацию получения урона
                case ImpactType.Attack: result = target.TakeDamage(Damage, target.Position, SquadId); break;
                case ImpactType.Healing: result = target.IncreaseHealth(Damage, target.Position, SquadId); break;
            }

            if (result && _abilityEffectPrefab != null)
            {
                // возврат в пул управляется через IAbilityEffect
                var abilityEffect = Poolable
                    .TryGetPoolableNet(_abilityEffectPrefab.name, target.transform)
                    .GetComponent<IAbilityEffect>();

                abilityEffect.ApplyEffect(target, CasterData, AbilityData);
            }

            return result;
        }

        protected virtual void OnTriggerEnter(Collider other) => CastAbilityVisualEffect();

        protected void CastAbilityVisualEffect()
        {
            if (_collisionParticlesPrefab == null || Random.value > _chanceToSpawnCollisionPrefab) return;
            _photonView.RPC("RpcCastAbilityVisualEffect", RpcTarget.All);
        }

        [PunRPC]
        protected void RpcCastAbilityVisualEffect()
        {
            var particles = Poolable.TryGetPoolable<ParticleSystem>(_collisionParticlesPrefab);
            particles.transform.SetParent(null);
            particles.transform.position = transform.position;
            particles.Play();
        }

        public List<Poolable> GetPoolables()
        {
            var result = new List<Poolable>() { GetComponent<Poolable>() };
            if (_abilityEffectPrefab) result.Add(_abilityEffectPrefab.GetComponent<Poolable>());
            if (_collisionParticlesPrefab) result.Add(_collisionParticlesPrefab.GetComponent<Poolable>());
            return result;
        }
    }
}