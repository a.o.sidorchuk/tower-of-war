﻿using ActionGameFramework.Health;
using Photon.Pun;
using UnityEngine;

namespace Game.Units.Abilities
{
    /// <summary> Способность действующая в конусе из позиции юнита </summary>
    public class FrontSplashAbility : HitscanAbility
    {
        static readonly Collider[] _targets = new Collider[64];

        [SerializeField] LayerMask _targetMask;

        public override bool ApplyAbility(Targetable target)
        {
            var targetCount = Physics.OverlapSphereNonAlloc(_origin.position, SplashRadius, _targets, _targetMask);

            targetCount = Mathf.Clamp(targetCount, 0, MaxTargetCount);
            var currentCount = 0;

            foreach (var sTarget in _targets)
            {
                if (sTarget == null) break;

                var damageable = sTarget.GetComponentInParent<Targetable>();
                if (damageable == null) continue;

                if (Vector3.Angle(_origin.forward, damageable.Position - _origin.position) > 90f) continue;

                var result = base.ApplyAbility(damageable);

                if (result) currentCount++;

                if (currentCount == targetCount) break;
            }

            CastAbilityVisualEffect();

            return currentCount != 0;
        }
    }
}