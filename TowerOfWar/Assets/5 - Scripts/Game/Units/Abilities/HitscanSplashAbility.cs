﻿using ActionGameFramework.Health;
using Data.Models.Abilities;
using UnityEngine;

namespace Game.Units.Abilities
{
    /// <summary> Прмое действие на область вокруг таргета </summary>
    public class HitscanSplashAbility : HitscanAbility
    {
        static readonly Collider[] _targets = new Collider[64];

        [SerializeField] LayerMask _targetMask;

        public override bool ApplyAbility(Targetable target)
        {
            var toSelfPosition = DirectionTypeCheck(DirectionType.Self);
            var targetCount = Physics.OverlapSphereNonAlloc(toSelfPosition ? _origin.position : _target.Position, SplashRadius, _targets, _targetMask);

            targetCount = Mathf.Clamp(targetCount, 0, MaxTargetCount);
            var currentCount = 0;

            foreach (var sTarget in _targets)
            {
                if (sTarget == null) break;

                var damageable = sTarget.GetComponentInParent<Targetable>();
                if (damageable == null) continue;

                var result = base.ApplyAbility(damageable);

                if (result) currentCount++;

                if (currentCount == targetCount) break;
            }

            CastAbilityVisualEffect();

            return currentCount != 0;
        }
    }
}