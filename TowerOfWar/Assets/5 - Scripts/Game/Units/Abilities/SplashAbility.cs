﻿using ActionGameFramework.Health;
using Core.Utilities;
using UnityEngine;

namespace Game.Units.Abilities
{
    /// <summary> Действие по области в текущей позиции снаряда </summary>
    public class SplashAbility : BaseAbility
    {
        static readonly Collider[] _targets = new Collider[64];

        [SerializeField] LayerMask _targetMask;

        private void OnCollisionEnter(Collision collision)
        {
            // если попадание не по триггеру юнита (пример: снаряд упал на землю)
            if (collision.gameObject.layer == 0) CreateSplash(collision.collider);
        }

        protected override void OnTriggerEnter(Collider otherTargetter)
        {
            var damageable = otherTargetter.GetComponentInParent<Targetable>();
            if (damageable == null) return;

            // _TODO по типу атаки определить нужно ли нанести урон или похилить
            if (!damageable.Configuration.CanDamage(SquadId)) return;

            CreateSplash(otherTargetter);
        }

        private void CreateSplash(Collider other)
        {
            var targetCount = Physics.OverlapSphereNonAlloc(transform.position, SplashRadius, _targets, _targetMask);

            targetCount = Mathf.Clamp(targetCount, 0, MaxTargetCount);
            var currentCount = 0;

            foreach (var target in _targets)
            {
                if (target == null) break;

                var targetable = target.GetComponentInParent<Targetable>();
                if (targetable == null) continue;

                var result = ApplyAbility(targetable);

                if (result) currentCount++;

                if (currentCount == targetCount) break;
            }

            CastAbilityVisualEffect();

            Poolable.TryPoolNet(gameObject);
        }
    }
}