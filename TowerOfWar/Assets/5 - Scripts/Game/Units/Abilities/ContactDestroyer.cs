﻿using Core.Utilities;
using UnityEngine;

namespace Game.Units.Abilities
{
    /// <summary>
    /// Компонент объект разрушаемых при контакте с другими объектами или падением ниже заданного уровня "земли"
    /// </summary>
    public class ContactDestroyer : MonoBehaviour
    {
        protected float yDestroyPoint { get; private set; } = -5;

        protected virtual void Update()
        {
            if (transform.position.y < yDestroyPoint) ReturnToPool(); 
        }

        void OnCollisionEnter(Collision other) => ReturnToPool();

        void ReturnToPool()
        {
            if (gameObject.activeInHierarchy) Poolable.TryPoolNet(gameObject);
        }
    }
}