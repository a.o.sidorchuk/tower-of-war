﻿using ActionGameFramework.Health;
using Core.Utilities;
using UnityEngine;

namespace Game.Units.Abilities
{
    /// <summary> Способность с прямым воздействием на таргет </summary>
    public class HitscanAbility : BaseAbility
    {
        [Tooltip("Время через которое враг \"почувствует\" воздействие, s")]
        [SerializeField] float _delay = 0.01f; // имитация - время "полета" снаряда

        protected NetTimer _timer;
        protected bool _pauseTimer = true;

        protected Targetable _target;
        protected Transform _origin;

        protected virtual void Update()
        {
            if (!_pauseTimer && _timer != null) _timer.Tick();
        }

        public void ApplyAbility(Transform origin, Targetable target)
        {
            _origin = origin;
            _target = target;

            if (_target == null) return;

            _timer = new NetTimer(_delay, TimerEvent);
            _pauseTimer = false;
        }

        private void TimerEvent()
        {
            _pauseTimer = true;

            ApplyAbility(_target);
            CastAbilityVisualEffect();

            Poolable.TryPoolNet(gameObject);
        }
    }
}