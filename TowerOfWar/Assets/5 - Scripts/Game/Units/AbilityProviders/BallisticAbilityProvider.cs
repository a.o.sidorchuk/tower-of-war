﻿using ActionGameFramework.Health;
using ActionGameFramework.Helpers;
using ActionGameFramework.Projectiles;
using Data.Models.Abilities;
using Game.Data.Units;
using UnityEngine;

namespace Game.Units.AbilitiesProviders
{
	/// <summary>
	/// Провайдер для запуска способности летящей по дуге к цели
	/// </summary>
	public class BallisticAbilityProvider : BaseAbilityProvider
    {
        public override void ApplyAbility(Targetable enemy, GameObject abilityPrefab, Transform firingPoint, int squadId, UnitViewItem unit, AbilityData abilityData)
        {
			var startPosition = firingPoint.position;

			var ballisticProjectile = abilityPrefab.GetComponent<BallisticProjectile>();

			if (ballisticProjectile == null)
			{
				Debug.LogError($"Необходимо назначить хотя бы один BallisticProjectile на {abilityPrefab.name}");
				DestroyImmediate(abilityPrefab);
				return;
			}

			Vector3 targetPoint;
			if (ballisticProjectile.fireMode == BallisticFireMode.UseLaunchSpeed)
			{
				targetPoint = Ballistics.CalculateBallisticLeadingTargetPointWithSpeed(
					startPosition, enemy.Position, enemy.Velocity,
					ballisticProjectile.startSpeed, ballisticProjectile.arcPreference, Physics.gravity.y, 4);
			}
			else
			{
				targetPoint = Ballistics.CalculateBallisticLeadingTargetPointWithAngle(
					startPosition, enemy.Position, enemy.Velocity, ballisticProjectile.firingAngle,
					ballisticProjectile.arcPreference, Physics.gravity.y, 4);
			}

			ballisticProjectile.FireAtPoint(startPosition, targetPoint);

			PlayParticles(startPosition, targetPoint);
		}
    }
}