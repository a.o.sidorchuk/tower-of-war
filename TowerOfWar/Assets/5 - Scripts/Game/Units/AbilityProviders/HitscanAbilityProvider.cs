﻿using ActionGameFramework.Health;
using Data.Models.Abilities;
using Game.Data.Units;
using Game.Units.Abilities;
using UnityEngine;

namespace Game.Units.AbilitiesProviders
{
    /// <summary>
    /// Провадейр способностей воздействующих на цель напрямую
    /// </summary>
    public class HitscanAbilityProvider : BaseAbilityProvider
    {
        public override void ApplyAbility(Targetable enemy, GameObject abilityPrefab, Transform firingPoint, int squadId, UnitViewItem unit, AbilityData abilityData)
        {
            var hitscanAbility = abilityPrefab.GetComponent<HitscanAbility>();
            if (hitscanAbility == null) return;

            hitscanAbility.transform.position = firingPoint.position;

            hitscanAbility.ApplyAbility(firingPoint, enemy);

            PlayParticles(firingPoint.position, enemy.Position);
        }
    }
}