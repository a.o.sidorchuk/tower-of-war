﻿using ActionGameFramework.Health;
using Data.Models.Abilities;
using Game.Data.Units;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Units.AbilitiesProviders
{
	public interface IAbilityProvider
	{
		/// <summary> Атака по врагу </summary>
		/// <param name="enemy"> Текущая цель </param>
		/// <param name="abilityPrefab"> Шаблон способности </param>
		/// <param name="firingPoint"> Точка откуда производится атака </param>
		/// <param name="squadId"> Команда к которой принадлежит инициатор </param>
		/// <param name="caster"> Данные юнита производящего атаку </param>
		/// <param name="abilityData"> Данные способности которую использовал юнит </param>
		void ApplyAbility(Targetable enemy, GameObject abilityPrefab, Transform firingPoint, int squadId, UnitViewItem caster, AbilityData abilityData);

		/// <summary> Атака по врагу из нескольких точек </summary>
		/// <param name="enemy"> Текущая цель </param>
		/// <param name="abilityPrefab"> Шаблон поведения </param>
		/// <param name="firingPoints"> Список точек из которых производится атака </param>
		/// <param name="squadId"> Команда к которой принадлежит инициатор </param>
		/// <param name="caster"> Данные юнита производящего атаку </param>
		/// <param name="abilityData"> Данные способности которую использовал юнит </param>
		void ApplyAbility(Targetable enemy, GameObject abilityPrefab, Transform[] firingPoints, int squadId, UnitViewItem caster, AbilityData abilityData);

		/// <summary> Атака по нескольким врагам из нескольких точек </summary>
		/// <param name="enemies"> Текущие цели </param>
		/// <param name="abilityPrefab"> Шаблон поведения </param>
		/// <param name="firingPoints"> Список точек из которых производится атака </param>
		/// <param name="squadId"> Команда к которой принадлежит инициатор </param>
		/// <param name="caster"> Данные юнита производящего атаку </param>
		/// <param name="abilityData"> Данные способности которую использовал юнит </param>
		void ApplyAbility(List<Targetable> enemies, GameObject abilityPrefab, Transform[] firingPoints, int squadId, UnitViewItem caster, AbilityData abilityData);
	}
}