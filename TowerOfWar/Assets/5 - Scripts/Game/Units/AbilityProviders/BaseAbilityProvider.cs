﻿using ActionGameFramework.Health;
using Core.Utilities;
using Data.Models.Abilities;
using Game.Data.Units;
using Game.Units.Abilities;
using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Units.AbilitiesProviders
{
    /// <summary>
    /// Базовый компонент для реализации провайдера способностей юнита
    /// </summary>
    public abstract class BaseAbilityProvider : MonoBehaviour, IAbilityProvider, IPoolHelper
    {
        [SerializeField] GameObject _abilityParticlePrefab;
        PhotonView _photonView;

        protected virtual void Awake() => _photonView = GetComponent<PhotonView>();

        public abstract void ApplyAbility(Targetable enemy, GameObject abilityPrefab, Transform firingPoint, int squadId, UnitViewItem unit, AbilityData abilityData);

        public void ApplyAbility(Targetable enemy, GameObject abilityPrefab, Transform[] firingPoints, int squadId, UnitViewItem unit, AbilityData abilityData)
        {
            var firingPoint = GetRandomTransform(firingPoints);

            var abilityComponent = Poolable.TryGetPoolableNet<BaseAbility>(abilityPrefab.name, firingPoint.position, firingPoint.rotation);

            abilityComponent.Init(squadId);
            abilityComponent.SetData(unit, abilityData);

            ApplyAbility(enemy, abilityComponent.gameObject, firingPoint, squadId, unit, abilityData);
        }

        public void ApplyAbility(List<Targetable> enemies, GameObject abilityPrefab, Transform[] firingPoints, int squadId, UnitViewItem unit, AbilityData abilityData)
        {
            var count = abilityData.TargetCount == 0 ? enemies.Count : Mathf.Clamp(enemies.Count, 0, abilityData.TargetCount);
            var currentFiringPointIndex = 0;
            var firingPointLength = firingPoints.Length;

            for (var i = 0; i < count; i++)
            {
                var enemy = enemies[i];

                var firingPoint = firingPoints[currentFiringPointIndex];
                currentFiringPointIndex = (currentFiringPointIndex + 1) % firingPointLength;

                var abilityComponent = Poolable.TryGetPoolableNet<BaseAbility>(abilityPrefab.name, firingPoint.position, firingPoint.rotation);

                abilityComponent.Init(squadId);
                abilityComponent.SetData(unit, abilityData);

                ApplyAbility(enemy, abilityComponent.gameObject, firingPoint, squadId, unit, abilityData);
            }
        }

        public Transform GetRandomTransform(Transform[] launchPoints) => launchPoints[Random.Range(0, launchPoints.Length)];

        public void PlayParticles(Vector3 origin, Vector3 lookPosition)
        {
            if (_abilityParticlePrefab == null) return;
            _photonView.RPC("RpcPlayParticles", RpcTarget.All, origin, lookPosition);
        }

        [PunRPC]
        protected void RpcPlayParticles(Vector3 origin, Vector3 lookPosition)
        {
            var particles = Poolable.TryGetPoolable<ParticleSystem>(_abilityParticlePrefab);
            particles.transform.SetParent(null);
            particles.transform.position = origin;
            particles.transform.LookAt(lookPosition);
            particles.Play();
        }

        public List<Poolable> GetPoolables()
        {
            var result = new List<Poolable>();
            if (_abilityParticlePrefab) result.Add(_abilityParticlePrefab.GetComponent<Poolable>());
            return result;
        }
    }
}