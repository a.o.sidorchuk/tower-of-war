﻿using ActionGameFramework.Health;
using ActionGameFramework.Helpers;
using ActionGameFramework.Projectiles;
using Data.Models.Abilities;
using Game.Data.Units;
using UnityEngine;

namespace Game.Units.AbilitiesProviders
{
	/// <summary>
	/// Провайдер самонаводящихяся способностей летящих по догу к цели
	/// </summary>
	public class HomingAbilityProvider : BaseAbilityProvider
    {
        public override void ApplyAbility(Targetable enemy, GameObject abilityPrefab, Transform firingPoint, int squadId, UnitViewItem unit, AbilityData abilityData)
        {
			var homingProjectile = abilityPrefab.GetComponent<HomingLinearProjectile>();

			if (homingProjectile == null)
			{
				Debug.LogError($"Необходимо назначить хотябы один HomingLinearProjectile на {abilityPrefab}");
				DestroyImmediate(abilityPrefab);
				return;
			}

			var startingPoint = firingPoint.position;
			var targetPoint = Ballistics.CalculateLinearLeadingTargetPoint(
				startingPoint, enemy.Position, enemy.Velocity, homingProjectile.startSpeed,
				homingProjectile.acceleration);

			homingProjectile.SetHomingTarget(enemy);

			homingProjectile.FireAtPoint(startingPoint, targetPoint);

			PlayParticles(startingPoint, targetPoint);
		}
    }
}