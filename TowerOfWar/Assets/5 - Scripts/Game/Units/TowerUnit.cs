﻿using Game.Data.Units;
using System.Linq;
using UnityEngine;

namespace Game.Units
{
    /// <summary> Базовая реализация юнита "Башня" </summary>
    public class TowerUnit : BaseUnit
    {
        Transform[] _moduleSpawnPoints;

        public Transform RandomModuleSpawnPoint
        {
            get
            {
                if (_moduleSpawnPoints == null) return null;
                var freePoints = _moduleSpawnPoints.Where(x => x.childCount == 0);
                var pointTransform = freePoints.ElementAt(Random.Range(0, freePoints.Count()));
                return pointTransform;
            }
        }

        public override void Init(int squadId, UnitViewItem unit)
        {
            base.Init(squadId, unit);

            _moduleSpawnPoints = VisualComponent.ModulePoints;

            SetEffectorState(false);
        }

        public bool ApplyTowerAbility()
        {
            var effector = Effectors.FirstOrDefault(x => x.Targetter.Target != null);

            if (effector == null) return false;

            var target = effector.Targetter.Target;
            var distance = Vector3.Distance(transform.position, target.transform.position);

            if (distance > effector.Targetter.Range) return false;

            effector.CastEffect();

            Animator.SetApplyAbilityState();

            return true;
        }
    }
}