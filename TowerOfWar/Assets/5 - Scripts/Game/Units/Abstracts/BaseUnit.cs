﻿using ActionGameFramework.Health;
using Core.Utilities;
using Game.Data.Units;
using Game.Economy;
using Game.Effectors;
using Game.Playables;
using Photon.Pun;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;

namespace Game.Units
{
    /// <summary> Базовая реализация юнита </summary>
    public abstract class BaseUnit : Targetable, IPoolHelper
    {
        [SerializeField] Transform _visualModelPoint;
        [Space]
        [SerializeField] bool _expDrop;
        [SerializeField] bool _goldDrop;
        [SerializeField] bool _resourceWarDrop;
        [Space]
        [Tooltip("Смещение позиции для применяемого эффекта")]
        [SerializeField] Vector3 _appliedEffectOffset;
        [SerializeField] float _appliedEffectScale = 1;

        protected bool _effectorsEnabled { get; private set; }
        public Effector[] Effectors { get; private set; }

        public UnitVisualComponent VisualComponent { get; private set; }
        public Transform VisualModelPoint => _visualModelPoint;

        public CharacterAnimation Animator => VisualComponent.Animator;

        public UnitViewItem UnitData { get; private set; }

        public Vector3 AppliedEffectOffset => _appliedEffectOffset;
        public float AppliedEffectScale => _appliedEffectScale;

        protected PhotonView _photonView;

        protected virtual void Awake()
        {
            _photonView = GetComponent<PhotonView>();
            Effectors = GetComponentsInChildren<Effector>();

            if (_goldDrop) gameObject.AddComponent<GoldDrop>();
            if (_expDrop) gameObject.AddComponent<ExpDrop>();
            if (_resourceWarDrop) gameObject.AddComponent<ResourceWarDrop>();
        }

        public virtual void SetActive(bool state)
        {
            foreach (var e in Effectors) e.gameObject.SetActive(state);
        }

        public virtual void Init(int squadId, UnitViewItem unit)
        {
            UnitData = unit;

            // подгрузить 3d модельку (с синхроном по сети)
            VisualComponent = Poolable.TryGetPoolableNet<UnitVisualComponent>(
                unit.VisualPrefab.RuntimeKey.ToString(),
                _visualModelPoint.position,
                _visualModelPoint.rotation,
                new object[] { _photonView.ViewID });

            base.Init(squadId);

            Priority = (int)UnitData.UnitType;
            Configuration.SetMaxHealth(UnitData.CurrentHealth);

            foreach (var effect in Effectors) effect.Init(squadId, unit, VisualComponent.CastPoints);

            SetEffectorState(true);
        }

        public void SetVisualComponent(UnitVisualComponent visualComponent)
        {
            VisualComponent = visualComponent;
            var transform = VisualModelPoint.transform;
            VisualComponent.transform.position = transform.position;
            VisualComponent.transform.SetParent(transform);
        }

        public void SetEffectorState(bool state)
        {
            _effectorsEnabled = state;
            foreach (var effect in Effectors) effect.enabled = state;
        }

        public List<Poolable> GetPoolables() => new List<Poolable> { GetComponent<Poolable>() };

        public override void Remove()
        {
            base.Remove();

            VisualComponent.SetDeathState();

            Poolable.TryPoolNet(gameObject);
        }
    }
}