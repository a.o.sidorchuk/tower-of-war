﻿using System;
using System.Linq;
using Data.Models.Units;
using Game.Data.Units;
using Game.Nodes;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Units
{
    /// <summary> Тип юнита способный передвигаться по карте при помощи NavMesh </summary>
    [RequireComponent(typeof(NavMeshAgent))]
    public abstract class NavUnit : BaseUnit
    {
        /// <summary> Юнит дошел до конца своего пути </summary>
        public event Action<Node> destinationReached;

        public enum State
        {
            /// <summary> Юнит в пути, путь свободен </summary>
            OnCompletePath,
            /// <summary> Юнит в пути, но путь заблокирован или встретил врага </summary>
            OnPartialPath,
            /// <summary> Юнит достиг объект который преграждает путь </summary>
            ApplyAbility,
            /// <summary> Для летающих юнитов, когда они перемещаются через препятствия на земле </summary>
            PushingThrough,
            /// <summary> Юнит завершил свой путь до базы врага </summary>
            PathComplete
        }

        public State CurrentState { get; protected set; }

        public float OriginalMovementSpeed { get; private set; }
        public float CurrentSpeed => NavMeshAgent.speed;
        public override Vector3 Velocity => NavMeshAgent.velocity;
        public int NavMeshMask => NavMeshAgent.areaMask;

        protected virtual bool IsAtDestination => NavMeshAgent.remainingDistance <= NavMeshAgent.stoppingDistance;

        protected Node _currentNode;
        protected Vector3 _destination;

        protected NavMeshAgent NavMeshAgent { get; private set; }

        protected virtual bool TargetFound
        {
            get
            {
                var targetFound = Effectors.Any(x => x.Targetter.Target != null);
                return NavMeshAgent.pathStatus == NavMeshPathStatus.PathPartial || targetFound;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            NavMeshAgent = GetComponent<NavMeshAgent>();
            SetMovementActive(false);
        }

        public override void Init(int squadId, UnitViewItem unit)
        {
            base.Init(squadId, unit);

            CurrentState = TargetFound ? State.OnPartialPath : State.OnCompletePath;

            OriginalMovementSpeed = 1.5f * (int)UnitData.SpeedType;

            SetMovementActive(OriginalMovementSpeed != 0);
            SetSpeed(OriginalMovementSpeed);
            SetDestination(transform.position);

            StartMovement();

            Animator.SetMovementState(UnitData.SpeedType);
        }

        public override void SetActive(bool state)
        {
            base.SetActive(state);

            if (!IsDead)
            {
                if (state)
                {
                    if (!TargetFound) Animator.SetMovementState(UnitData.SpeedType);
                }
                else
                {
                    Animator.SetMovementState(MovementSpeedType.None);
                }
            }

            SetMovementActive(state);
        }

        protected virtual void Update()
        {
            if (Configuration.IsDead) return;

            PathUpdate();

            var _validStalePath = NavMeshAgent.isOnNavMesh && NavMeshAgent.enabled &&
                                  (!NavMeshAgent.hasPath && !NavMeshAgent.pathPending);

            if (_validStalePath)
            {
                var squareStoppingDistance = NavMeshAgent.stoppingDistance * NavMeshAgent.stoppingDistance;
                if (Vector3.SqrMagnitude(_destination - transform.position) < squareStoppingDistance && NextNode() != null)
                {
                    GetNextNode(_currentNode);
                }
                else
                {
                    SetDestination(_destination);
                }
            }
        }

        protected abstract void PathUpdate();
        protected abstract void OnPartialPathUpdate();
        protected virtual void OnCompletePathUpdate()
        {
            if (TargetFound) CurrentState = State.OnPartialPath;
        }
        public virtual void HandleDestinationReached()
        {
            CurrentState = State.PathComplete;
            destinationReached?.Invoke(_currentNode);
        }

        public void StartMovement()
        {
            if (!NavMeshAgent.enabled || !NavMeshAgent.isStopped || Configuration.IsDead) return;

            NavMeshAgent.isStopped = false;

            Animator.SetMovementState(UnitData.SpeedType);
        }

        public void StopMovement()
        {
            if (!NavMeshAgent.enabled || NavMeshAgent.isStopped || Configuration.IsDead) return;

            NavMeshAgent.isStopped = true;

            Animator.SetState(Playables.CharacterState.M_Stand);
        }

        public void SetMovementActive(bool state) => NavMeshAgent.enabled = state;

        public void SetSpeed(float speed) => NavMeshAgent.speed = speed;
        protected void SetDestination(Vector3 destination)
        {
            _destination = destination;
            if (NavMeshAgent.enabled) NavMeshAgent.SetDestination(destination);
        }

        public virtual void SetNode(Node node) => _currentNode = node;

        public void GetNextNode(Node node)
        {
            if (_currentNode != node) return;

            if (_currentNode == null)
            {
                Debug.LogError("Узел пути не задан");
                return;
            }

            var nextNode = NextNode();
            if (nextNode == null)
            {
                StopMovement();
                HandleDestinationReached();
                return;
            }

            SetNode(nextNode);

            MoveToNode();
        }

        public Node NextNode()
        {
            if (!_currentNode) return null;
            switch (Configuration.SquadId)
            {
                case 1: return _currentNode.GetNextNode();
                case 2: return _currentNode.GetPrevNode();
                default: return null;
            }
        }

        public Node PrevNode()
        {
            if (!_currentNode) return null;
            switch (Configuration.SquadId)
            {
                case 1: return _currentNode.GetPrevNode();
                case 2: return _currentNode.GetNextNode();
                default: return null;
            }
        }

        public virtual void MoveToNode()
        {
            if (!_currentNode) return;

            var nodePosition = _currentNode.GetRandomPointInNodeArea();
            nodePosition.y = _currentNode.transform.position.y;

            SetDestination(nodePosition);
        }

        public override void Remove()
        {
            base.Remove();

            SetMovementActive(false);
        }
    }
}