﻿using ActionGameFramework.Health;
using Core.Utilities;
using Data.Models.Abilities;
using Game.Data.Units;

namespace Game.Units.Effects
{
    public class StunEffect : AbilityEffect<StunEffect>
    {
        NetTimer _timer;

        public override bool ApplyEffect(Targetable targetable, UnitViewItem caster, AbilityData ability, params object[] parameters)
        {
            if (!base.ApplyEffect(targetable, caster, ability, parameters)) return false;

            var time = 0.5f * Ability.CastTime;

            CurrentUnit.SetActive(false);

            _timer = new NetTimer(time, RemoveEffect);

            return true;
        }

        private void Update()
        {
            if (_timer != null) _timer.Tick();
        }

        protected override void RemoveEffect()
        {
            _timer = null;
            if (CurrentUnit) CurrentUnit.SetActive(true);

            base.RemoveEffect();
        }
    }
}