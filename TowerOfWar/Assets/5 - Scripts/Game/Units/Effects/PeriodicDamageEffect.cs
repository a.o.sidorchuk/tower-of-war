﻿using ActionGameFramework.Health;
using Core.Health;
using Data.Models.Abilities;
using Game.Data.Units;
using System.Threading;
using UniRx.Async;
using UnityEngine;

namespace Game.Units.Effects
{
    public class PeriodicDamageEffect : AbilityEffect<PeriodicDamageEffect>
    {
        CancellationTokenSource _cancellationToken;

        public override bool ApplyEffect(Targetable targetable, UnitViewItem caster, AbilityData ability, params object[] parameters)
        {
            if (!base.ApplyEffect(targetable, caster, ability, parameters)) return false;

            _cancellationToken = new CancellationTokenSource();

            StartPeriodicDemage();

            return true;
        }

        private void StartPeriodicDemage() => UniTask.ToCoroutine(async () =>
        {
            var time = Ability.CastTime;

            while (!_cancellationToken.IsCancellationRequested && (time -= Time.deltaTime) > 0)
            {
                if (CurrentUnit.IsDead) break;

                var damage = Caster.CurrentStrength * Time.deltaTime / Ability.CastTime;
                CurrentUnit.Configuration.TakeDamage(damage, -1, out HealthChangeInfo info);

                await UniTask.Yield();
            }

            if (!_cancellationToken.IsCancellationRequested) RemoveEffect();
        });

        protected override void RemoveEffect()
        {
            if (_cancellationToken != null) _cancellationToken.Cancel();

            base.RemoveEffect();
        }
    }
}