﻿using ActionGameFramework.Health;
using Core.Health;
using Data.Models.Abilities;
using Game.Data.Units;

namespace Game.Units.Effects
{
    public class MagicShieldEffect : AbilityEffect<MagicShieldEffect>
    {
        float _currentShieldValue;

        public override bool ApplyEffect(Targetable targetable, UnitViewItem caster, AbilityData ability, params object[] parameters)
        {
            if (!base.ApplyEffect(targetable, caster, ability, parameters)) return false;

            _currentShieldValue = caster.CurrentStrength * 2f;

            CurrentUnit.Configuration.SetDamageScale(.85f);
            CurrentUnit.Configuration.damaged += DamagedEvent;

            return true;
        }

        private void DamagedEvent(HealthChangeInfo obj)
        {
            _currentShieldValue -= obj.AbsHealthDifference;

            if (_currentShieldValue <= 0) RemoveEffect();
        }

        protected override void RemoveEffect()
        {
            if (CurrentUnit)
            {
                CurrentUnit.Configuration.SetDamageScale(1);
                CurrentUnit.Configuration.damaged -= DamagedEvent;
            }

            base.RemoveEffect();
        }
    }
}