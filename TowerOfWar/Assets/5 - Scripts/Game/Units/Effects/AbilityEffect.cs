﻿using ActionGameFramework.Health;
using Core.Health;
using Core.Utilities;
using Data.Models.Abilities;
using Game.Data.Units;
using Photon.Pun;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Units.Effects
{
    public interface IAbilityEffect
    {
        bool ApplyEffect(Targetable targetable, UnitViewItem caster, AbilityData ability, params object[] parameters);
    }

    /// <summary> Базовый компонент для эффектов воздействующих на юнит </summary>
    public abstract class AbilityEffect<TAbilityEffect> : MonoBehaviour, IPoolHelper, IAbilityEffect
        where TAbilityEffect : AbilityEffect<TAbilityEffect>
    {
        [SerializeField] GameObject _particleSystemPrefab;
        ParticleSystem _particles;
        PhotonView _photonView;

        protected BaseUnit CurrentUnit { get; private set; }

        protected UnitViewItem Caster { get; private set; }
        protected AbilityData Ability { get; private set; }

        protected EffectParam Effect { get; private set; }

        protected virtual void Awake() => _photonView = GetComponent<PhotonView>();

        public virtual bool ApplyEffect(Targetable targetable, UnitViewItem caster, AbilityData ability, params object[] parameters)
        {
            if (Random.value > ability.EffectParam.ProcChance)
            {
                RemoveEffect();
                return false;
            }

            CurrentUnit = (BaseUnit)targetable;
            CurrentUnit.Configuration.died += DiedEvent;

            Caster = caster;
            Ability = ability;
            Effect = ability.EffectParam;

            if (!Ability.EffectParam.BaseEffect.AddUp)
            {
                foreach (var shield in CurrentUnit.GetComponentsInChildren<TAbilityEffect>())
                {
                    if (shield != this) shield.RemoveEffect();
                }
            }

            if (_particleSystemPrefab) _photonView.RPC("RpcPlayAbilityEffect", RpcTarget.All);

            return true;
        }

        [PunRPC]
        protected void RpcPlayAbilityEffect()
        {
            _particles = Poolable.TryGetPoolable<ParticleSystem>(_particleSystemPrefab);
            _particles.transform.position = CurrentUnit.transform.position;
            _particles.transform.SetParent(CurrentUnit.transform);
            _particles.Play();
        }

        [PunRPC]
        protected void RpcRemoveAbilityEffect()
        {
            Poolable.TryPool(_particles.gameObject);
        }

        public List<Poolable> GetPoolables()
        {
            var result = new List<Poolable> { GetComponent<Poolable>() };
            if (_particleSystemPrefab) result.Add(_particleSystemPrefab.GetComponent<Poolable>());
            return result;
        }

        protected virtual void DiedEvent(HealthChangeInfo obj) => RemoveEffect();

        protected virtual void RemoveEffect()
        {
            if (CurrentUnit) CurrentUnit.Configuration.died -= DiedEvent;

            if (_particles)
            {
                _photonView.RPC("RpcRemoveAbilityEffect", RpcTarget.All);
            }

            Poolable.TryPoolNet(gameObject);
        }
    }
}