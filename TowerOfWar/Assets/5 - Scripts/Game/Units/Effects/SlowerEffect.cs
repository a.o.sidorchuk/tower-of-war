﻿using ActionGameFramework.Health;
using Core.Health;
using Core.Utilities;
using Data.Models.Abilities;
using Game.Data.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Units.Effects
{
    public class SlowerEffect : AbilityEffect<SlowerEffect>
    {
        protected GameObject _slowFx;
        protected List<float> _currentEffects = new List<float>();

        public override bool ApplyEffect(Targetable targetable, UnitViewItem caster, AbilityData ability, params object[] parameters)
        {
            if (!base.ApplyEffect(targetable, caster, ability)) return false;

            var unit = CurrentUnit as NavUnit;
            if (unit == null) return false;

            var slowFactor = Convert.ToSingle(parameters[0]);
            var slowfxPrefab = (GameObject)parameters[1];

            _currentEffects.Add(slowFactor);

            var min = _currentEffects.Min(x => x);
            var originalSpeed = unit.OriginalMovementSpeed;
            var newSpeed = originalSpeed * min;

            unit.SetSpeed(newSpeed);

            if (_slowFx == null && slowfxPrefab != null)
            {
                _slowFx = Poolable.TryGetPoolable(slowfxPrefab);
                _slowFx.transform.SetParent(transform);
                _slowFx.transform.localPosition = CurrentUnit.AppliedEffectOffset;
                _slowFx.transform.localScale *= CurrentUnit.AppliedEffectScale;
            }

            CurrentUnit.removed += OnRemoved;

            return true;
        }

        public void RemoveSlow(float slowFactor)
        {
            CurrentUnit.removed -= OnRemoved;

            _currentEffects.Remove(slowFactor);
            if (_currentEffects.Count != 0) return;

            ResetAgent();
        }

        private void OnRemoved(DamageableBehaviour db)
        {
            CurrentUnit.removed -= OnRemoved;

            ResetAgent();
        }

        private void ResetAgent()
        {
            var unit = CurrentUnit as NavUnit;
            if (unit != null) unit.SetSpeed(unit.OriginalMovementSpeed);

            if (_slowFx != null)
            {
                Poolable.TryPool(_slowFx);
                _slowFx.transform.localScale = Vector3.one;
                _slowFx = null;
            }

            Destroy(this);
        }
    }
}