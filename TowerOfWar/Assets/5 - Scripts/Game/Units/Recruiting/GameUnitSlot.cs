﻿using Data.Models.Economy;
using Data.Models.Units;
using Game.Data.Units;
using Game.Level;
using Game.Network;
using UniRx.Async;
using UnityEngine;

namespace Game.Units.Recruiting
{
    /// <summary>
    /// Компонент слота для "призыва" или использования способности юнита
    /// </summary>
    public class GameUnitSlot : MonoBehaviour
    {
        protected int _currentRecruitsNumber;

        protected float _reloadingTime = 0;
        protected float _startReloadingNetTime = 0;

        public bool UnitReloading { get; private set; }

        public Currency _resource { get; private set; }

        public UnitViewItem CurrentUnit { get; private set; }
        int _squadId;

        public virtual async UniTask<bool> Init(UnitViewItem unit, int squadId, Currency currency)
        {
            if (CurrentUnit != null) return false;

            _squadId = squadId;
            CurrentUnit = unit;

            _resource = currency;
            _resource.currencyChanged += ResourceValueChange;

            _reloadingTime = CurrentUnit.ReloadingTime;

            return true;
        }

        private void Update()
        {
            if (UnitReloading)
            {
                var currentReloadingTime = NetworkController.Instance.Time - _startReloadingNetTime;

                if (currentReloadingTime >= _reloadingTime)
                {
                    UnitReloading = false;
                    Reloaded();
                }
                else
                {
                    ReloadProgressChanged(currentReloadingTime / _reloadingTime);
                }
            }
        }

        public void Activate()
        {
            if (CurrentUnit == null) return;
            if (CurrentUnit.UnitType == UnitType.Building) StartReloadProcess();
        }

        private void StartReloadProcess()
        {
            if (!UnitReloading)
            {
                ReloadStarted();

                _startReloadingNetTime = NetworkController.Instance.Time;

                UnitReloading = true;
            }
        }

        public bool ReqruitingCheck()
        {
            var recruitsNumberCheck = CurrentUnit.RecruitsNumber == 0 || _currentRecruitsNumber < CurrentUnit.RecruitsNumber;
            return (_resource.CanAfford(CurrentUnit.Cost) || CurrentUnit.Cost == 0) && recruitsNumberCheck;
        }

        public void ReqruitUnitProcess()
        {
            if (ReqruitingCheck())
            {
                _resource.TryPurchase(CurrentUnit.Cost);

                LevelManager.Instance.UnitButtonClick(CurrentUnit.UnitId, _squadId);

                //IncreaseRecruitsNumberProcess();
                StartReloadProcess();
            }
        }

        private void IncreaseRecruitsNumberProcess()
        {
            if (CurrentUnit.RecruitsNumber > 0)
            {
                _currentRecruitsNumber++;

                IncreaseRecruitsNumber();
            }
        }

        protected virtual void ReloadProgressChanged(float value) { }
        protected virtual void ReloadStarted() { }
        protected virtual void Reloaded() { }
        protected virtual void ResourceValueChange() { }
        protected virtual void IncreaseRecruitsNumber() { }
    }
}