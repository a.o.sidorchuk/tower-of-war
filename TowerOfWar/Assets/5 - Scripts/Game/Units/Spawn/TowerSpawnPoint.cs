﻿using Data.Models.Level;
using Game.Nodes;
using UnityEngine;

namespace Game.Units
{
    /// <summary>
    /// Точка базы игрока и его юнитов
    /// </summary>
    public class TowerSpawnPoint : MonoBehaviour
    {
        [SerializeField] SquadName _squadName;
        [SerializeField] Node _unitSpawnNode;
        [Space]
        [SerializeField] Transform _towerSpawnPosition;

        public SquadName SquadName => _squadName;
        public int SquadId => (int)_squadName;
        public Node UnitSpawnNode => _unitSpawnNode;
        public Transform SpawnPosition => _towerSpawnPosition;

        #region gizmo
        private void OnDrawGizmos()
        {
            var color = Gizmos.color;
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position, 0.3f);
            Gizmos.color = color;
        }
        #endregion
    }
}