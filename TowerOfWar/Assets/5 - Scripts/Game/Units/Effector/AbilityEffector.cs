﻿using ActionGameFramework.Audio;
using ActionGameFramework.Health;
using Core.Utilities;
using Game.Data.Units;
using Game.Units.AbilitiesProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Game.Effectors
{
    /// <summary> Базовая реализация способности юнита </summary>
    [RequireComponent(typeof(IAbilityProvider))]
    public class AbilityEffector : Effector, IPoolHelper
    {
        public event Action onEffectApplied;

        /// <summary> Шаблон с поведением способности </summary>
        [Space]
        [SerializeField] GameObject _abilityPrefab;
        [SerializeField] bool isMulticast;

        /// <summary> Точка откуда производится поиск противника </summary>
        [Space]
        [SerializeField] Transform _epicenter;

        [Space]
        [SerializeField] RandomAudioSource randomAudioSource;

        protected IAbilityProvider _abilityProvider;

        public Targetable CurrentTarget { get; protected set; }

        float _castTimer;

        protected override void Awake()
        {
            base.Awake();
            _abilityProvider = GetComponent<IAbilityProvider>();
        }

        public override void Init(int squadId, UnitViewItem unitViewItem, Transform[] originPoints)
        {
            base.Init(squadId, unitViewItem, originPoints);

            _castTimer = CastTime;

            Targetter.acquiredTarget += OnAcquiredTarget;
            Targetter.lostTarget += OnLostTarget;
        }

        void OnDestroy()
        {
            Targetter.acquiredTarget -= OnAcquiredTarget;
            Targetter.lostTarget -= OnLostTarget;
        }

        protected virtual void Update()
        {
            _castTimer -= Time.deltaTime;
            if (CurrentTarget != null && _castTimer <= 0.0f)
            {
                CastEffect();
                _castTimer = CastTime;
            }
        }

        void OnLostTarget() => CurrentTarget = null;

        void OnAcquiredTarget(Targetable acquiredTarget) => CurrentTarget = acquiredTarget;

        public override void CastEffect()
        {
            if (CurrentTarget == null) return;

            onEffectApplied?.Invoke();

            if (isMulticast)
            {
                _abilityProvider.ApplyAbility(Targetter.AllTargets, _abilityPrefab, _originPoints, SquadId, UnitViewItem, AbilityData);
            }
            else
            {
                _abilityProvider.ApplyAbility(CurrentTarget, _abilityPrefab, _originPoints, SquadId, UnitViewItem, AbilityData);
            }

            if (randomAudioSource != null) randomAudioSource.PlayRandomClip();
        }

        /// <summary> Сравнение дистанций разных таргетов </summary>
        protected virtual int ByDistance(Targetable first, Targetable second)
        {
            var firstSqrMagnitude = Vector3.SqrMagnitude(first.Position - _epicenter.position);
            var secondSqrMagnitude = Vector3.SqrMagnitude(second.Position - _epicenter.position);
            return firstSqrMagnitude.CompareTo(secondSqrMagnitude);
        }

        public List<Poolable> GetPoolables()
        {
            var result = new List<Poolable>();

            var otherHelpers = _abilityPrefab.GetComponentsInChildren<IPoolHelper>();
            foreach (var helper in otherHelpers.Where(x => x != this as IPoolHelper))
            {
                result.AddRange(helper.GetPoolables());
            }

            return result;
        }

        public void RemoveCurrentTarget() => Targetter.RemoveCurrentTarget();

#if UNITY_EDITOR
        /// <summary> Показать область поиска </summary>
        void OnDrawGizmosSelected()
        {
            if (_epicenter && Targetter) Gizmos.DrawWireSphere(_epicenter.position, Targetter.Range);
        }
#endif
    }
}