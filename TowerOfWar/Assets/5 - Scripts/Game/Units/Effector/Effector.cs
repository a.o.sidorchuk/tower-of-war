﻿using Game.Data.Ability;
using Game.Data.Units;
using Game.Targetting;
using UnityEngine;

namespace Game.Effectors
{
    /// <summary>
    /// Базовый компонент для системы способностей (эффектов) юнитов
    /// </summary>
    [RequireComponent(typeof(Targetter))]
    public abstract class Effector : MonoBehaviour
    {
        [SerializeField] AbilityItem _abilityData;
        
        public Targetter Targetter { get; private set; }

        public UnitViewItem UnitViewItem { get; private set; }
        public AbilityItem AbilityData => _abilityData;

        public int SquadId { get; protected set; }
        public float CastTime => _abilityData.CastTime;

        protected Transform[] _originPoints { get; private set; }


        protected virtual void Awake() => Targetter = GetComponent<Targetter>();

        public virtual void Init(int squadId, UnitViewItem unitViewItem, Transform[] originPoints)
        {
            Targetter.Init(_abilityData, squadId);

            _originPoints = originPoints;

            UnitViewItem = unitViewItem;
            SquadId = squadId;
        }

        public virtual void CastEffect() { throw new System.NotImplementedException(); }
    }
}