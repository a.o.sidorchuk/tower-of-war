﻿using ActionGameFramework.Health;
using Game.Units;
using Game.Units.Effects;
using UnityEngine;

namespace Game.Effectors
{
    /// <summary>
    /// Реализация пассивной способности замедлять рядом находящихся врагов
    /// </summary>
    public class PassiveSlowEffector : Effector
    {
        [SerializeField] [Range(0, 1)] float slowFactor;
        [Space]
        [SerializeField] GameObject _slowFxPrefab;
        [Space]
        [SerializeField] ParticleSystem _enterParticleSystem;
        [SerializeField] AudioSource _audioSource;

        protected override void Awake()
        {
            base.Awake();
            Targetter.targetEntersRange += OnTargetEntersRange;
            Targetter.targetExitsRange += OnTargetExitsRange;
        }

        void OnDestroy()
        {
            Targetter.targetEntersRange -= OnTargetEntersRange;
            Targetter.targetExitsRange -= OnTargetExitsRange;
        }

        protected void OnTargetEntersRange(Targetable other)
        {
            var unit = other as NavUnit;
            if (unit == null) return;

            AttachSlowComponent(unit);
        }

        protected void OnTargetExitsRange(Targetable other)
        {
            var unit = other as NavUnit;
            if (unit == null) return;

            RemoveSlowComponent(unit);
        }

        protected void AttachSlowComponent(NavUnit target)
        {
            var slower = target.GetComponent<SlowerEffect>();
            if (slower == null) slower = target.gameObject.AddComponent<SlowerEffect>();

            //slower.Init(UnitViewItem, AbilityData, slowFactor, _slowFxPrefab);

            if (_enterParticleSystem != null) _enterParticleSystem.Play();
            if (_audioSource != null) _audioSource.Play();
        }

        protected void RemoveSlowComponent(NavUnit target)
        {
            if (target == null) return;

            var slowComponent = target.gameObject.GetComponent<SlowerEffect>();
            if (slowComponent != null) slowComponent.RemoveSlow(slowFactor);
        }
    }
}