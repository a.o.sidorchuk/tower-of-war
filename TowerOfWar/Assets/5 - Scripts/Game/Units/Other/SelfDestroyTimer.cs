﻿using Core.Utilities;
using System;
using UnityEngine;

namespace Game.Units
{
    /// <summary> Компонент самоуничтожения объекта по таймеру с отправкой его в пул объектов </summary>
    public class SelfDestroyTimer : MonoBehaviour
    {
        public event Action death;

        [SerializeField] float _time = 5f;
        [SerializeField] NetTimer _timer;

        protected virtual void OnEnable()
        {
            if (_timer == null)
                _timer = new NetTimer(_time, OnTimeEnd);
            else
                _timer.Reset();
        }

        protected virtual void OnTimeEnd()
        {
            death?.Invoke();
            Poolable.TryPool(gameObject);
            _timer.Reset();
        }

        protected virtual void Update()
        {
            if (_timer == null) return;
            _timer.Tick();
        }
    }
}