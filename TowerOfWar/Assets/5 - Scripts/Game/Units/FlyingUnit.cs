﻿using Game.Data.Units;
using UnityEngine;
using UnityEngine.AI;

namespace Game.Units
{
    /// <summary> Вариант юнита, перемещающегося по воздуху </summary>
    public class FlyingUnit : Unit
    {
        protected override void PathUpdate()
        {
            switch (CurrentState)
            {
                case State.OnCompletePath: OnCompletePathUpdate(); break;
                case State.OnPartialPath: OnPartialPathUpdate(); break;
                case State.PathComplete: Remove(); break;
            }
        }

        protected override void OnPartialPathUpdate()
        {
            if (!TargetFound)
            {
                CurrentState = State.OnCompletePath;
                return;
            }

            AbilityEffectorUpdate();
        }
    }
}