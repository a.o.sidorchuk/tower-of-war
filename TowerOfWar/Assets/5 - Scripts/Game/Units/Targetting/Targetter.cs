﻿using System;
using System.Collections.Generic;
using ActionGameFramework.Health;
using Core.Health;
using Data.Models.Abilities;
using Game.Data.Ability;
using Game.Units;
using UnityEngine;

namespace Game.Targetting
{
    /// <summary> Компонент отслеживания целей для взаимодействия </summary>
    public class Targetter : MonoBehaviour
    {
        public event Action<Targetable> targetEntersRange;
        public event Action<Targetable> targetExitsRange;

        public event Action<Targetable> acquiredTarget;
        public event Action lostTarget;

        /// <summary> Текущая цель </summary>
        protected Targetable _сurrentTargetable;
        /// <summary> Список целей попавших в триггерную зону </summary>
        protected List<Targetable> _targetsInRange = new List<Targetable>();

        /// <summary> Была ли цель в последнем кадре </summary>
        protected bool _hadTarget;

        public int SquadId { get; private set; }

        public float Range { get; private set; }
        public SphereCollider AttachedCollider { get; private set; }

        public Targetable Target => _сurrentTargetable;
        public List<Targetable> AllTargets => _targetsInRange;

        AbilityItem _abilityData;

        public virtual void Init(AbilityItem abilityData, int squadId)
        {
            Reset();

            _abilityData = abilityData;

            SquadId = squadId;
            Range = _abilityData.Range;

            GenerateCollider();

            // добавить себя в список целей, если эта способность может быть применена к себе
            if (_abilityData.DirectionCheck(DirectionType.Self))
            {
                _targetsInRange.Add(GetComponentInParent<Targetable>());
            }
        }

        private void GenerateCollider()
        {
            if (AttachedCollider == null)
            {
                AttachedCollider = gameObject.AddComponent<SphereCollider>();
                AttachedCollider.isTrigger = true;
                AttachedCollider.radius = Range;
            }
        }

        /// <summary> Проверка возможности воздействия на таргет </summary>
        protected virtual bool IsTargetableValid(Targetable targetable)
        {
            if (!targetable) return false;

            var unit = targetable as BaseUnit;

            if (!unit) return false;

            var unitData = unit.UnitData;
            var targetTypeCheck = unitData.UnitTypeCheck(_abilityData.UnitTargetType);

            if (!targetTypeCheck) return false;

            var isEnemy = SquadId != targetable.Configuration.SquadId;
            var negativeImpact = _abilityData.ImpactType == ImpactType.Attack || _abilityData.ImpactType == ImpactType.Debuff;

            return (isEnemy && negativeImpact) || (!isEnemy && !negativeImpact);
        }

        /// <summary> Получить ближайшую из доступных целей </summary>
        protected virtual Targetable GetNearestTargetable()
        {
            var length = _targetsInRange.Count;

            if (length == 0) return null;

            Targetable nearest = null;
            var distance = float.MaxValue;
            var priority = int.MaxValue;

            for (var i = length - 1; i >= 0; i--)
            {
                var targetable = _targetsInRange[i];
                if (targetable == null || targetable.IsDead)
                {
                    _targetsInRange.RemoveAt(i);
                    continue;
                }

                var currentDistance = Vector3.Distance(transform.position, targetable.Position);
                var currentPriority = targetable.Priority;

                if (currentDistance <= distance && currentPriority <= priority)
                {
                    priority = currentPriority;
                    distance = currentDistance;
                    nearest = targetable;
                }
            }

            return nearest;
        }


        protected virtual void OnTriggerExit(Collider other)
        {
            var targetable = other.GetComponent<Targetable>();

            if (!IsTargetableValid(targetable)) return;

            _targetsInRange.Remove(targetable);

            targetExitsRange?.Invoke(targetable);

            if (targetable == _сurrentTargetable)
            {
                OnTargetRemoved(targetable);
            }
            else
            {
                targetable.removed -= OnTargetRemoved;
            }
        }

        protected virtual void OnTriggerEnter(Collider other)
        {
            var targetable = other.GetComponent<Targetable>();

            if (!IsTargetableValid(targetable)) return;

            targetable.removed += OnTargetRemoved;

            _targetsInRange.Add(targetable);

            targetEntersRange?.Invoke(targetable);
        }

        protected virtual void Update()
        {
            if (_targetsInRange.Count > 0)
            {
                var newTarget = GetNearestTargetable();

                if (newTarget != null && newTarget != _сurrentTargetable)
                {
                    _сurrentTargetable = newTarget;
                    acquiredTarget?.Invoke(_сurrentTargetable);
                }
            }

            _hadTarget = _сurrentTargetable != null;
        }

        public void RemoveCurrentTarget() => OnTargetRemoved(Target);

        void OnTargetRemoved(DamageableBehaviour target)
        {
            target.removed -= OnTargetRemoved;

            if (_сurrentTargetable && target.Configuration == _сurrentTargetable.Configuration)
            {
                lostTarget?.Invoke();

                _targetsInRange.Remove(_сurrentTargetable);

                _сurrentTargetable = null;

                _hadTarget = false;
            }
            else
            {
                var index = _targetsInRange.FindIndex(x => x.Configuration == target.Configuration);
                if (index != -1) _targetsInRange.RemoveAt(index);
            }
        }

        public void Reset()
        {
            _targetsInRange.Clear();
            _сurrentTargetable = null;

            targetEntersRange = null;
            targetExitsRange = null;
            acquiredTarget = null;
            lostTarget = null;
        }
    }
}