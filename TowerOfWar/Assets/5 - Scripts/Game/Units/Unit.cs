﻿using ActionGameFramework.Health;
using Core.Health;
using Data.Models.Abilities;
using Game.Data.Units;
using Game.Effectors;
using System.Linq;
using UnityEngine;

namespace Game.Units
{
    /// <summary> Компонент для реализации основных видов юнитов </summary>
    /// <remarks> 
    /// Данный тип юнитов может применять способности наносящие урон врагам и востанавливающе здоровье союзникам.
    /// Таже среди способносей могут быть бафы и дебафы.
    /// </remarks>
    public class Unit : NavUnit
    {
        AbilityEffector _attackEffector;
        Targetable _currentTarget;

        public override void Init(int squadId, UnitViewItem unit)
        {
            base.Init(squadId, unit);

            _attackEffector = (AbilityEffector)Effectors.FirstOrDefault(x =>
            {
                var effector = x as AbilityEffector;
                if (!effector) return false;
                return effector.AbilityData.ImpactType == ImpactType.Attack ||
                       effector.AbilityData.ImpactType == ImpactType.Healing;
            });

            _attackEffector.enabled = false;
        }

        protected override void PathUpdate()
        {
            switch (CurrentState)
            {
                case State.OnCompletePath:
                case State.PathComplete: OnCompletePathUpdate(); break;
                case State.OnPartialPath: OnPartialPathUpdate(); break;
                case State.ApplyAbility: AbilityUpdate(); break;
            }
        }

        protected override void OnPartialPathUpdate()
        {
            if (!TargetFound)
            {
                CurrentState = State.OnCompletePath;
                return;
            }

            AbilityEffectorUpdate();

            CurrentState = State.ApplyAbility;
        }

        protected virtual void AbilityEffectorUpdate()
        {
            var newTarget = _attackEffector.Targetter.Target;

            if (newTarget == null) return;

            // Проверка ближайшей цели и текущей
            if (newTarget != _currentTarget)
            {
                // отписка от предыдущего объекта
                if (_currentTarget != null)
                {
                    _currentTarget.removed -= OnTargetUnitDestroyed;
                }

                _currentTarget = newTarget;

                // если новая цель есть, то подписка на ее уничтожение
                if (_currentTarget != null)
                {
                    _currentTarget.removed += OnTargetUnitDestroyed;
                }
            }

            if (_currentTarget == null) return;

            if (!_attackEffector.enabled) _attackEffector.enabled = true;

            _attackEffector.onEffectApplied += Animator.SetApplyAbilityState;
            
            Animator.SetReadyState();
        }

        protected void AbilityUpdate()
        {
            if (_currentTarget)
            {
                var _currentRange = _attackEffector.Targetter.Range;

                var distance = Position - _currentTarget.Position;
                var range = Mathf.Clamp(_currentRange, 2, distance.magnitude);

                var destination = _currentTarget.Position + (Position - _currentTarget.Position).normalized * range;

                SetDestination(destination);

                if (destination != Position)
                {
                    StartMovement();
                }
                else
                {
                    StopMovement();

                    if (NavMeshAgent.enabled)
                    {
                        var targetPosition = _currentTarget.Position;
                        targetPosition.y = transform.position.y;
                        var direction = targetPosition - transform.position;
                        transform.rotation = Quaternion.LookRotation(direction, Vector3.up);
                    }
                }

                return;
            }

            MoveToNode();

            // Продолжить перемещение
            _attackEffector.onEffectApplied -= Animator.SetApplyAbilityState;
            _attackEffector.enabled = false;

            StartMovement();

            CurrentState = TargetFound ? State.OnPartialPath : State.OnCompletePath;
        }

        protected virtual void OnTargetUnitDestroyed(DamageableBehaviour unit)
        {
            _currentTarget.removed -= OnTargetUnitDestroyed;
            _currentTarget = null;
        }

        public override void Remove()
        {
            base.Remove();

            if (_currentTarget)
            {
                _currentTarget.removed -= OnTargetUnitDestroyed;
                _currentTarget = null;
            }

            _attackEffector.enabled = false;
        }
    }
}