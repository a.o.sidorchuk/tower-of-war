﻿using Core.Utilities;
using Game.Playables;
using Photon.Pun;
using System;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;

namespace Game.Units
{
    /// <summary> Компонент агригирующий все визуальные элементы юнита </summary>
    /// <remarks> 
    /// Помимо визуальных элементов, определяются точки каста способностей и установки доп. модулей для данного юнита.
    /// Точки определяются в данной компоненте из-за различных пропорций моделей юнитов
    /// </remarks>
    public class UnitVisualComponent : MonoBehaviour, IPoolHelper, IPunInstantiateMagicCallback
    {
        [SerializeField] Collider _triggerCollider;
        [Space]
        [SerializeField] CharacterAnimation _animator;
        [Space]
        [SerializeField] Transform[] _castPoints;
        [SerializeField] Transform[] _modulePoints;

        PhotonView _photonView;

        #region public fields
        public Collider TriggerCollider => _triggerCollider;
        public CharacterAnimation Animator => _animator;
        public Transform[] CastPoints => _castPoints;
        public Transform[] ModulePoints => _modulePoints;
        #endregion

        private void Awake() => _photonView = GetComponent<PhotonView>();

        public List<Poolable> GetPoolables() => new List<Poolable> { GetComponent<Poolable>() };

        public void OnPhotonInstantiate(PhotonMessageInfo info)
        {
            // по сохраненному в InstantiationData индексу юнита, находим и закрепляемся за ним
            var unitPV = PhotonNetwork.GetPhotonView(Convert.ToInt32(info.photonView.InstantiationData[0]));
            var unit = unitPV.GetComponent<BaseUnit>();
            if (unit) unit.SetVisualComponent(this);
        }

        private void OnValidate()
        {
            if (_triggerCollider && !_triggerCollider.isTrigger) _triggerCollider.isTrigger = true;
        }

        public void SetDeathState() => _photonView.RPC("RpcResetVisualComponent", RpcTarget.All);

        [PunRPC]
        protected void RpcResetVisualComponent()
        {
            transform.SetParent(null);

            _animator.RpcSetCharacterState((int)CharacterState.D_Death);

            UniTask.ToCoroutine(async () =>
            {
                await UniTask.Delay(Mathf.RoundToInt(Mathf.Clamp(Animator.CurrentClipTime * 1000f + 250, 250, float.MaxValue)));

                var pos = transform.position;
                pos.x = 45;
                transform.position = pos;

                _animator.RpcSetCharacterState((int)CharacterState.M_Stand);

                await UniTask.Delay(500);

                if (PhotonNetwork.IsMasterClient) Poolable.TryPoolNet(gameObject);
            });
        }
    }
}