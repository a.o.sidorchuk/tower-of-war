﻿using Data.Models.Level;
using Game.Data.Level;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Menu.Map
{
    public class DungeonItemButton : MonoBehaviour
    {
        public event Action<DungeonItemButton> onClick;

        [SerializeField] Image _background;
        [SerializeField] Image _borders;
        [Space]
        [SerializeField] Text _title;
        [SerializeField] Image _levelCompleteImage;
        [SerializeField] Image _towerUndamagedImage;
        [SerializeField] Image _timeLimitImage;

        Button _button;

        ContentLevelInfo _levelInfo;
        LevelSaveData _progressData;

        public ContentLevelInfo LevelInfo => _levelInfo;

        private void Awake()
        {
            _button = GetComponent<Button>();
            SetActive(false);
        }

        public void Init(ContentLevelInfo levelInfo, LevelSaveData progressData)
        {
            // _TODO отметить прогресс этого данжа на иконках
            _title.text = levelInfo.Title;

            _levelInfo = levelInfo;
            _progressData = progressData;

            _button.onClick.AddListener(OnClickEvent);
        }

        public void OnClickEvent()
        {
            onClick?.Invoke(this);
            SetActive(true);
        }

        public void SetActive(bool state)
        {
            _borders.gameObject.SetActive(state);
        }
    }
}