﻿using Game.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Menu.Map
{
    public class MapPanel : ClosingPanel
    {
        [Space]
        [SerializeField] ScrollRect _dungeonListScroll;
        [SerializeField] DungeonItemButton _dungeonItemPrefab;

        [Space]
        [SerializeField] Text _titleText;
        [SerializeField] Text _descriptionText;

        [Space]
        [SerializeField] Button _startLevelButton;

        // _TODO добавить отображение списка вражеской команды на уровне

        List<DungeonItemButton> _dungeonItemButtons = new List<DungeonItemButton>();
        DungeonItemButton _currentDungeonItemButton;

        protected override void Awake()
        {
            base.Awake();

            CreateDungeonList();

            _startLevelButton.onClick.AddListener(StartLevelEvent);
        }

        private void CreateDungeonList()
        {
            if (_dungeonItemButtons.Any()) return;

            var dungeonList = GameManager.Instance.Dungeons;
            var dungeonProgressList = GameManager.Instance.Store.PlayerData.CurrentFractionProgress.CompleteLevelData;

            var count = dungeonList.Count;

            var content = _dungeonListScroll.content;

            for (var i = 0; i < count; i++)
            {
                var button = Instantiate(_dungeonItemPrefab, content);

                var level = dungeonList.Levels[i];
                var progress = dungeonProgressList.FirstOrDefault(x => x.LevelId == level.Id);

                button.Init(level, progress);

                button.onClick += DungeonButtonEvent;

                _dungeonItemButtons.Add(button);
            }

            if (count > 0) _dungeonItemButtons[0].OnClickEvent();
        }

        private void DungeonButtonEvent(DungeonItemButton dButton)
        {
            if (_currentDungeonItemButton != null) _currentDungeonItemButton.SetActive(false);
            _currentDungeonItemButton = dButton;

            _titleText.text = dButton.LevelInfo.Title;
            _descriptionText.text = dButton.LevelInfo.Description;
            // _TODO показать список вражеской команды -- dButton.LevelInfo.Enemies;
        }

        private void StartLevelEvent()
        {
            GameManager.Instance.CreateDungeonGameSession(_currentDungeonItemButton.LevelInfo);
        }
    }
}

