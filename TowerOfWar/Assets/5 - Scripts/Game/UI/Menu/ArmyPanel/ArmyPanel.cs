﻿using Game.Data;
using Game.Data.Units;
using System.Collections.Generic;

namespace Game.UI.Menu.Army
{
    public class ArmyPanel : ItemRowPanel<UnitViewRow, UnitViewItemButton, UnitViewItem>
    {
        protected override List<UnitViewItem> LoadData() => GameManager.Instance.UnitViewItems;
    }
}