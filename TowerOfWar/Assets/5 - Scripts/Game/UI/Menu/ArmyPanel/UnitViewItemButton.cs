﻿using Game.Data.Units;
using System;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Game.UI.Menu.Army
{
    [RequireComponent(typeof(Button))]
    public class UnitViewItemButton : BaseViewItem<UnitViewItem>
    {
        [SerializeField] Text _levelInfoText;
        [SerializeField] Text _unitTitleText;
        [SerializeField] Image _iconImage;

        Button _button;

        private void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnClickEvent);
        }

        public override async UniTask Init(UnitViewItem unit)
        {
            await base.Init(unit);

            UpdateTitle();

            await UpdateImage();

            ViewData.onLevelUp += UpdateTitle;

            _unitTitleText.text = unit.UnitDescription.Title;
        }

        private void UpdateTitle()
        {
            _levelInfoText.text = ViewData.Level == 0 ? "Заблокирован" : $"Уровень {ViewData.Level}";
        }

        private async UniTask UpdateImage()
        {
            var sprite = await ViewData.IconSprite.CustomLoadAssetAsync<Sprite>();
            _iconImage.sprite = sprite;
        }

        private void OnClickEvent()
        {
            MessageBroker.Default.Publish(MenuMessage.Create(MenuMessage.Type.OpenUnitInfoPanel, ViewData, null));
        }

        private void OnDestroy() => ViewData.onLevelUp -= UpdateTitle;
    }
}