﻿using Data.Models.Units;
using Game.Data.Units;
using System;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Game.UI.Menu.Squad
{
    public class UnitViewItemButton : BaseViewItem<UnitViewItem>
    {
        public event Action<UnitViewItemButton> onClick;

        [SerializeField] Image _squadIndicator;
        [Space]
        [SerializeField] Image _iconImage;
        [Space]
        [SerializeField] Text _levelText;
        [Space]
        [SerializeField] GameObject _resourcePanel;
        [SerializeField] Text _costText;
        [SerializeField] Image _resourceIcon;
        [Space]
        [SerializeField] Text _unitTitleText;

        Button _button;

        #region public fields
        public Image IconImage => _iconImage;
        public Text LevelText => _levelText;
        public GameObject ResourcePanel => _resourcePanel;
        public Text CostText => _costText;
        public Image ResourceIcon => _resourceIcon;
        #endregion

        private void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(() => onClick?.Invoke(this));
        }

        public override async UniTask Init(UnitViewItem unit)
        {
            await base.Init(unit);

            UpdateLevel();
            ViewData.onLevelUp += UpdateLevel;

            _unitTitleText.text = ViewData.UnitDescription.Title;
            _iconImage.sprite = await ViewData.IconSprite.CustomLoadAssetAsync<Sprite>();

            if (ViewData.ResourcesType != ResourcesType.None)
            {
                _resourceIcon.sprite = await AddressableExtensions.CustomLoadAssetAsync<Sprite>($"resource_icon_{(int)ViewData.ResourcesType}");
                _costText.text = ViewData.Cost.ToString();
            }
            else
            {
                _resourcePanel.gameObject.SetActive(false);
            }
        }

        private void UpdateLevel() => _levelText.text = ViewData.Level.ToString();

        public void SetActiveState(bool state) => _button.interactable = state;
        public void SetSquadIndicatorState(bool state) => _squadIndicator.gameObject.SetActive(state);

        private void OnDestroy() => ViewData.onLevelUp -= UpdateLevel;
    }
}
