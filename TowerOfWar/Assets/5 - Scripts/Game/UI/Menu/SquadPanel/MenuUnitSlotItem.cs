﻿using Data.Models.Units;
using System;
using System.Collections;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Game.UI.Menu.Squad
{
    public class MenuUnitSlotItem : MonoBehaviour
    {
        [SerializeField] UnitType _unitType;
        [SerializeField] AssetReferenceSprite _bgSprite;
        [SerializeField] Image _background;
        [SerializeField] GameObject _contentRoot;
        [Space]
        [SerializeField] Image _iconImage;
        [SerializeField] Text _levelText;
        [Space]
        [SerializeField] GameObject _resourcePanel;
        [SerializeField] Text _costText;
        [SerializeField] Image _resourceIcon;
        [Space]
        [SerializeField] Text _unitTitleText;

        public UnitViewItemButton CurrentUnit { get; private set; }

        Button _button;

        private void Awake()
        {
            _contentRoot.SetActive(false);
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnClickEvent);
        }

        IEnumerator Start() => UniTask.ToCoroutine(async () =>
        {
            _background.sprite = await _bgSprite.CustomLoadAssetAsync<Sprite>();
        });

        private void OnClickEvent()
        {
            if (CurrentUnit)
            {
                _contentRoot.SetActive(false);

                CurrentUnit.SetActiveState(true);
                CurrentUnit.SetSquadIndicatorState(false);
                CurrentUnit = null;
            }
        }

        public bool AddItem(UnitViewItemButton _currentUnit)
        {
            if (CurrentUnit) return false;
            if (!_currentUnit.ViewData.UnitTypeCheck(_unitType)) return false;

            _currentUnit.SetActiveState(false);
            _currentUnit.SetSquadIndicatorState(true);

            CurrentUnit = _currentUnit;

            UpdateView();

            _contentRoot.SetActive(true);

            return true;
        }

        private void UpdateView()
        {
            _unitTitleText.text = CurrentUnit.ViewData.UnitDescription.Title;
            _iconImage.sprite = CurrentUnit.IconImage.sprite;
            _levelText.text = CurrentUnit.LevelText.text;
            if (CurrentUnit.ViewData.ResourcesType != ResourcesType.None)
            {
                _costText.text = CurrentUnit.CostText.text;
                _resourceIcon.sprite = CurrentUnit.ResourceIcon.sprite;
            }
            else
            {
                _resourcePanel.gameObject.SetActive(false);
            }
        }
    }
}