﻿using Game.Data;
using Game.Data.Units;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Async;
using UnityEngine;

namespace Game.UI.Menu.Squad
{
    public class MenuSquadPanel : ItemRowPanel<UnitViewRow, UnitViewItemButton, UnitViewItem>
    {
        [Space]
        [SerializeField] List<MenuUnitSlotItem> UnitSlots;

        IDisposable _rxMessage;

        protected override List<UnitViewItem> LoadData() => GameManager.Instance.GetOpenedUnits();

        protected override async UniTask InitProcess()
        {
            await base.InitProcess();

            foreach (var btn in ViewItems) btn.onClick += ViewItemButtonClick;

            UnitSlotsInit();

            _rxMessage = MessageBroker.Default.Receive<MenuMessage>()
                .Where(x => x.type == MenuMessage.Type.RecruitUnitEvent)
                .Subscribe(async x =>
                {
                    var newBtn = await AddNewItem((UnitViewItem)x.data);
                    newBtn.onClick += ViewItemButtonClick;
                });
        }

        protected override async UniTask ClosePanelEvent()
        {
            await base.ClosePanelEvent();

            // сохранить изменения в сторе перед выходом
            await GameManager.Instance.SaveNewSquad(UnitSlots
                .Where(x => x.CurrentUnit)
                .Select(x => x.CurrentUnit.ViewData.UnitId)
                .ToList());

            MessageBroker.Default.Publish(MenuMessage.Create(MenuMessage.Type.SquadSaveEvent, null, null));
        }

        private void ViewItemButtonClick(UnitViewItemButton unit)
        {
            foreach (var slot in UnitSlots)
            {
                if (slot.AddItem(unit)) break;
            }
        }

        private void UnitSlotsInit()
        {
            var currenSquadUnits = GameManager.Instance.Store.PlayerData.CurrentFractionProgress.CurrentSquadUnits;
            var squadList = ViewItems.Where(x => currenSquadUnits.ContainsKey(x.ViewData.UnitId)).ToList();

            // инициализировать слоты
            foreach (var unit in squadList)
            {
                foreach (var slot in UnitSlots)
                {
                    if (slot.AddItem(unit)) break;
                }
            }
        }
    }
}