﻿using Data.Models.Player;
using Game.Data;
using Game.Data.Units;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Game.UI.Menu.UnitInfo
{
    public class UnitInfoPanel : ClosingPanel
    {
        [Space]
        [SerializeField] Text _titleText;
        [SerializeField] Image _iconImage;
        [Space]
        [SerializeField] Text _levelInfoText;
        [SerializeField] Button _levelUpButton;
        [SerializeField] Text _levelUpBtnText;
        [Space]
        [SerializeField] ParameterInfoField _rarityTypeField;
        [SerializeField] ParameterInfoField _costField;
        [SerializeField] ParameterInfoField _healthField;
        [SerializeField] ParameterInfoField _strengthField;
        [SerializeField] ParameterInfoField _speedField;

        // _TODO вывести на панель абилки и эффекты юнита

        PlayerData _playerData;
        UnitViewItem _unitViewItem;

        protected override void Awake()
        {
            base.Awake();

            _playerData = GameManager.Instance.Store.PlayerData;
            _levelUpButton.onClick.AddListener(LevelUpEvent);
        }

        protected override async UniTask OpenPanelEvent()
        {
            _unitViewItem = (UnitViewItem)_menuMsgData;

            _titleText.text = _unitViewItem.UnitDescription.Title;
            _iconImage.sprite = await _unitViewItem.UnitDescription.UnitIconSprite.CustomLoadAssetAsync<Sprite>();

            _levelInfoText.text = _unitViewItem.Level.ToString();
            UpdateLevelUpButton();

            _rarityTypeField.SetValue(_unitViewItem.RarityType + " (редкость)");
            _costField.SetValue(_unitViewItem.Cost + " (стоим. призыва)");
            _healthField.SetValue(_unitViewItem.CurrentHealth + " (здоровье)");
            _strengthField.SetValue(_unitViewItem.CurrentStrength + " (сила)");
            _speedField.SetValue(_unitViewItem.SpeedType + " (скорость)");

            await base.OpenPanelEvent();
        }

        protected override async UniTask ClosePanelEvent()
        {
            await base.ClosePanelEvent();

            await GameManager.Instance.SaveData();
        }

        private void UpdateLevelUpButton()
        {
            if (_unitViewItem.Level == 0)
            {
                _levelInfoText.text = "";
                _levelUpBtnText.text = $"Нанять ({_unitViewItem.CurrentTrainingCost})";
            }
            else if (_unitViewItem.Level < _playerData.CurrentLevel)
            {
                _levelInfoText.text = $"Уровень {_unitViewItem.Level}/{_playerData.CurrentLevel}";
                _levelUpBtnText.text = _unitViewItem.CurrentTrainingCost.ToString();
                _levelUpButton.interactable = true;
            }
            else
            {
                _levelInfoText.text = $"Уровень {_unitViewItem.Level}/{_playerData.CurrentLevel}";
                _levelUpBtnText.text = "Максимум!";
                _levelUpButton.interactable = true;
            }
        }

        private void LevelUpEvent()
        {
            if (_unitViewItem.Level < _playerData.CurrentLevel)
            {
                if (GameManager.Instance.Store.TryIncreaseUnitLevel(_unitViewItem))
                {
                    UpdateLevelUpButton();

                    _healthField.SetValue(_unitViewItem.CurrentHealth);
                    _strengthField.SetValue(_unitViewItem.CurrentStrength);
                }
            }
        }
    }
}