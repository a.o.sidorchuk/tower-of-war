﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Menu.UnitInfo
{
    public class ParameterInfoField : MonoBehaviour
    {
        [SerializeField] Sprite _iconSprite;
        [SerializeField] Image _iconImage;
        [Space]
        [SerializeField] Text _valueText;

        private void Awake() => _iconImage.sprite = _iconSprite;

        public void SetValue(object value) => _valueText.text = value.ToString();
    }
}