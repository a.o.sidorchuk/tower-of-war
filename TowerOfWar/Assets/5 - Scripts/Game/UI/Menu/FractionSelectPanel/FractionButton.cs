﻿using Data.Models.Player;
using System.Collections;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Game.UI.Menu.FractionSelect
{
    [RequireComponent(typeof(Button))]
    public class FractionButton : MonoBehaviour
    {
        [SerializeField] AssetReference _iconImage;
        [SerializeField] FractionType _fractionType;

        FractionSelectPanel _fractionSelectPanel;

        IEnumerator Start() => UniTask.ToCoroutine(async () =>
        {
            var btn = GetComponent<Button>();

            btn.onClick.AddListener(ClickEvent);

            var icon = await _iconImage.CustomLoadAssetAsync<Sprite>();
            if (icon) btn.image.sprite = icon;

            _fractionSelectPanel = GetComponentInParent<FractionSelectPanel>();
        });

        public void ClickEvent()
        {
            _fractionSelectPanel.SetFraction(_fractionType);
        }
    }
}