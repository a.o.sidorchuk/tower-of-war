﻿using Data.Models.Player;
using Core.SceneControl;
using UniRx.Async;
using Game.Data;

namespace Game.UI.Menu.FractionSelect
{
    public class FractionSelectPanel : BasePanel
    {
        protected override void Awake()
        {
            base.Awake();

            var playerData = GameManager.Instance.Store.PlayerData;
            if (playerData.FractionType != FractionType.None) gameObject.SetActive(false);
        }

        public void SetFraction(FractionType fractionType) => UniTask.ToCoroutine(async () =>
        {
            await GameManager.Instance.CreateOrSelectFraction(fractionType);

            // перезагружаем сцену
            SceneController.Instance.StartMenu();
        });
    }
}