﻿using Data.Models.Level;
using Game.Data;
using Game.Network;
using Photon.Pun;
using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;
using static Game.Network.NetworkController;

namespace Game.UI.Menu.Arena
{
    public class ArenaPanel : ClosingPanel
    {
        [Space]
        [SerializeField] RectTransform _gameLobbyPanel;
        [SerializeField] Text _squadAText;
        [SerializeField] Text _squadBText;
        [SerializeField] Button _readyButton;
        [Space]
        [SerializeField] RectTransform _startGameProcessPanel;
        [SerializeField] Image _startGameProcessImage;
        [Space]
        [SerializeField] Text _infoText;
        [SerializeField] Image _loadingIndicatorImage;

        List<IDisposable> _rxMessages = new List<IDisposable>();

        Photon.Realtime.Player _playerA;
        Photon.Realtime.Player _playerB;

        protected override void Awake()
        {
            base.Awake();

            _readyButton.onClick.AddListener(ReadyEvent);

            SearchModeActivate();
        }

        protected override async UniTask OpenPanelEvent()
        {
            await base.OpenPanelEvent();

            _rxMessages.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.LevelStateChange)
                .Subscribe(LevelStateChange));
            _rxMessages.Add(MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.PlayerStateChange)
                .Subscribe(PlayerStateChange));

            GameManager.Instance.CreateArenaGameSession();
        }

        protected override async UniTask ClosePanelEvent()
        {
            if (PhotonNetwork.InRoom)
                PhotonNetwork.LeaveRoom();
            else
                PhotonNetwork.Disconnect();

            await base.ClosePanelEvent();

            OnDisable();

            SearchModeActivate();
        }

        private void ReadyEvent() => NetworkController.Instance.UpdatePlayerProperty(HashKey.playerState, PlayerState.Ready);

        private void SearchModeActivate(string msg = "Поиск соперников...")
        {
            SetIndicatorActive(true);
            SetInfoText(msg);

            _gameLobbyPanel.gameObject.SetActive(false);
            _startGameProcessPanel.gameObject.SetActive(false);
        }

        private void CheckReadyActivate()
        {
            SetIndicatorActive(false);
            SetInfoText(null);

            // _TODO отобразить списки команд игроков
            var players = PhotonNetwork.CurrentRoom.Players;
            var localPlayer = PhotonNetwork.LocalPlayer;
            var localSquadId = GameManager.Instance.Store.GameSessionData.SquadId;

            var otherPlayer = players.FirstOrDefault(x => x.Value.ActorNumber != localPlayer.ActorNumber).Value;

            _playerA = localSquadId == (int)SquadName.A ? localPlayer : otherPlayer;
            _playerB = localSquadId == (int)SquadName.B ? localPlayer : otherPlayer;

            _squadAText.text = GetSquadText(_playerA, SquadName.A);
            _squadBText.text = GetSquadText(_playerB, SquadName.B);

            _gameLobbyPanel.gameObject.SetActive(true);
        }

        private string GetSquadText(Photon.Realtime.Player player, SquadName squadName) =>
            $"{player.NickName} ({squadName}) {(player.IsLocal ? "(ты)" : "")}";

        private void ReadyStateActivate()
        {
            _gameLobbyPanel.gameObject.SetActive(false);
            _startGameProcessPanel.gameObject.SetActive(true);

            // _TODO Поставить на загрузку картинку загружаемой арены из LevelInfo в game session data

            SetInfoText("Бой начинается! Удачи!");
            SetIndicatorActive(true);
        }

        private void PlayerStateChange(GameMessage msg)
        {
            if (_playerA == null || _playerB == null) return;
            var data = (Tuple<int, PlayerState>)msg.data;

            if (data.Item2 == PlayerState.Ready)
            {
                if (_playerA.ActorNumber == data.Item1) _squadAText.text = GetSquadText(_playerA, SquadName.A) + " (ГОТОВ)";
                if (_playerB.ActorNumber == data.Item1) _squadBText.text = GetSquadText(_playerB, SquadName.B) + " (ГОТОВ)";
            }
        }

        private void LevelStateChange(GameMessage msg)
        {
            var levelState = (LevelState)msg.data;
            switch (levelState)
            {
                case LevelState.SearchMode: SearchModeActivate(); break;
                case LevelState.CheckReady: CheckReadyActivate(); break;
                case LevelState.Ready: ReadyStateActivate(); break;
                case LevelState.Intro: SearchModeActivate("Загрузка игры..."); break;
            }
        }

        public void SetInfoText(string text) => _infoText.text = text;

        public void SetIndicatorActive(bool state) => _loadingIndicatorImage.gameObject.SetActive(state);

        private void OnDisable()
        {
            foreach (var msg in _rxMessages) msg.Dispose();
            _rxMessages.Clear();
        }
    }
}