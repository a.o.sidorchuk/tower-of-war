﻿using Data.Models.Units;
using Game.Data.Units;
using Game.Units;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Game.UI.Menu.Player
{
    public class UnitDemoPoint : MonoBehaviour
    {
        [SerializeField] UnitType _unitType;

        GameObject _unitGO;

        public UnitViewItem CurrentUnit { get; private set; }
        public UnitType UnitType => _unitType;

        public async UniTask Init(UnitViewItem unit)
        {
            if (unit == null)
            {
                CurrentUnit = null;
                Destroy(_unitGO);
                return;
            }

            if (CurrentUnit != null)
            {
                if (CurrentUnit.UnitId == unit.UnitId) return;

                Destroy(_unitGO);
            }

            CurrentUnit = unit;

            var unitPrefab = await unit.VisualPrefab.CustomLoadAssetAsync<GameObject>();
            _unitGO = Instantiate(unitPrefab, transform);
        }
    }
}