﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.AddressableAssets;
using UniRx.Async;
using System;

namespace Game.UI.Menu.Player
{
    [RequireComponent(typeof(Button))]
    public class MenuButton : MonoBehaviour
    {
        public event Action onClick;

        [SerializeField] AssetReference _iconSprite;
        [SerializeField] Image _iconImage;
        [SerializeField] Text _titleText;

        Button _button;

        private void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(() => onClick?.Invoke());
        }

        IEnumerator Start() => UniTask.ToCoroutine(async () =>
        {
            var icon = await _iconSprite.CustomLoadAssetAsync<Sprite>();
            if (icon && _iconImage) _iconImage.sprite = icon;
        });
    }
}