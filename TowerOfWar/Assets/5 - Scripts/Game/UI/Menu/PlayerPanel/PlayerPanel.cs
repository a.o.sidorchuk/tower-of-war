﻿using System.Collections;
using Data.Models.Player;
using Game.Data;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Game.UI.Menu.Player
{
    public class PlayerPanel : BasePanel
    {
        [Header("Общая информация")]
        [SerializeField] Text _playerNameText;
        [Space]
        [SerializeField] Image _playerAvatarImage;
        [SerializeField] Slider _playerExperienceSlider;
        [Space]
        [SerializeField] Text _playerLevelInfoText;
        [Space]
        [SerializeField] AssetReference _goldIconSprite;
        [SerializeField] Image _goldIconImage;
        [SerializeField] Text _goldAmountText;
        [Space]
        [SerializeField] MenuButton _arenaBtn;
        [SerializeField] MenuButton _armyBtn;
        [SerializeField] MenuButton _squadBtn;
        [SerializeField] MenuButton _questsBtn;
        [SerializeField] MenuButton _mapBtn;
        [SerializeField] MenuButton _optionsBtn;

        PlayerData _playerData;

        protected override void Awake()
        {
            base.Awake();

            _playerData = GameManager.Instance.Store.PlayerData;

            if (_playerData.FractionType == FractionType.None) enabled = false;

            _arenaBtn.onClick += OpenArenaPanelEvent;
            _armyBtn.onClick += OpenArmyPanelEvent;
            _squadBtn.onClick += OpenSquadPanelEvent;
            _questsBtn.onClick += OpenQuestsPanelEvent;
            _mapBtn.onClick += OpenMapPanelEvent;
            _optionsBtn.onClick += OpenOptionsPanelEvent;

            UpdateAvatarInfo();
        }

        IEnumerator Start() => UniTask.ToCoroutine(async () =>
        {
            var icon = await _goldIconSprite.CustomLoadAssetAsync<Sprite>();
            if (icon) _goldIconImage.sprite = icon;
        });

        private void Update()
        {
            // _TODO обновлять после изменения данных, не вызывать Update
            _playerLevelInfoText.text = _playerData.CurrentLevel.ToString();

            _playerExperienceSlider.value = _playerData.GetPlayerLevelPercent();
            _playerExperienceSlider.fillRect.gameObject.SetActive(_playerExperienceSlider.value != 0);

            _goldAmountText.text = $"{Mathf.CeilToInt(_playerData.Gold.CurrentCurrency)}";
        }

        private void UpdateAvatarInfo() => UniTask.ToCoroutine(async () =>
        {
            if (Social.localUser.authenticated)
            {
                _playerNameText.text = Social.localUser.userName;

                while (Social.localUser.image == null) await UniTask.Yield();

                var tex = Social.localUser.image;
                _playerAvatarImage.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
            }
            else if (_playerData.FractionType != FractionType.None)
            {
                var fraction = _playerData.FractionType;
                var icon = await AddressableExtensions.CustomLoadAssetAsync<Sprite>($"{fraction}_icon");

                _playerNameText.text = "<Player>";
                if (icon != null) _playerAvatarImage.sprite = icon;
            }
        });

        private void OpenMapPanelEvent() => MessageBroker.Default.Publish(MenuMessage.Create(MenuMessage.Type.OpenMapPanel, null, null));
        private void OpenQuestsPanelEvent() => MessageBroker.Default.Publish(MenuMessage.Create(MenuMessage.Type.OpenQuestsPanel, null, null));
        private void OpenSquadPanelEvent() => MessageBroker.Default.Publish(MenuMessage.Create(MenuMessage.Type.OpenSquadPanel, null, null));
        private void OpenArmyPanelEvent() => MessageBroker.Default.Publish(MenuMessage.Create(MenuMessage.Type.OpenArmyPanel, null, null));
        private void OpenArenaPanelEvent() => MessageBroker.Default.Publish(MenuMessage.Create(MenuMessage.Type.OpenArenaPanel, null, null));
        private void OpenOptionsPanelEvent() => MessageBroker.Default.Publish(MenuMessage.Create(MenuMessage.Type.OpenOptionsPanel, null, null));
    }
}