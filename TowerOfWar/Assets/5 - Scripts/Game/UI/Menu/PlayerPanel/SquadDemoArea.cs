﻿using Data.Models.Units;
using Game.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Async;
using UnityEngine;

namespace Game.UI.Menu.Player
{
    public class SquadDemoArea : MonoBehaviour
    {
        [SerializeField] List<UnitDemoPoint> _unitDemoPoints = new List<UnitDemoPoint>();

        IDisposable _rxMessage;

        private void Awake()
        {
            _rxMessage = MessageBroker.Default.Receive<MenuMessage>()
                .Where(x => x.type == MenuMessage.Type.SquadSaveEvent)
                .Subscribe(_ => UniTask.ToCoroutine(InitProcess));
        }

        IEnumerator Start() => UniTask.ToCoroutine(InitProcess);

        private async UniTask InitProcess()
        {
            var currentSuad = GameManager.Instance.GetSquadUnits();

            var tower = currentSuad.FirstOrDefault(x => x.UnitType == UnitType.Building);
            var towerModule = currentSuad.FirstOrDefault(x => x.UnitType == UnitType.BuildingModule);
            var units = currentSuad.Where(x => x.UnitType == UnitType.Air || x.UnitType == UnitType.Ground);
            var unitIndex = 0;

            foreach (var demoPoint in _unitDemoPoints)
            {
                switch (demoPoint.UnitType)
                {
                    case UnitType.Building:
                        await demoPoint.Init(tower);
                        break;
                    case UnitType.BuildingModule:
                        await demoPoint.Init(towerModule);
                        break;
                    default:
                        var unit = units.ElementAtOrDefault(unitIndex);
                        await demoPoint.Init(unit);
                        unitIndex++;
                        break;
                }
            }
        }

        private void OnDestroy() => _rxMessage.Dispose();
    }
}