﻿using Data.Models.Economy;
using Data.Models.Units;
using Game.Data.Units;
using Game.Units.Recruiting;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Game.UI.Game
{
    public class GameUnitSlotButton : GameUnitSlot
    {
        [SerializeField] UnitType _unitType;
        [SerializeField] Image _unitImage;
        [SerializeField] Button _recruitButton;
        [SerializeField] Text _currentRecruitsNumberText;
        [Space]
        [SerializeField] GameObject _costPanel;
        [SerializeField] Image _resourceImage;
        [SerializeField] Text _costText;
        [Space]
        [SerializeField] GameObject _reloadPanel;
        [SerializeField] Slider _reloadProgressBar;

        private void Awake()
        {
            _recruitButton.interactable = false;
            _reloadPanel.SetActive(false);
            _costPanel.SetActive(false);
        }

        public override async UniTask<bool> Init(UnitViewItem unit, int squadId, Currency resource)
        {
            if (!unit.UnitTypeCheck(_unitType)) return false;
            if (!await base.Init(unit, squadId, resource)) return false;

            await UpdateView();

            _recruitButton.interactable = _resource.CanAfford(CurrentUnit.Cost);
            _recruitButton.onClick.AddListener(ReqruitUnitProcess);

            return true;
        }

        protected override void ReloadProgressChanged(float value)
        {
            _reloadProgressBar.value = value;
        }

        protected override void Reloaded()
        {
            _recruitButton.interactable = _resource.CanAfford(CurrentUnit.Cost);

            _reloadPanel.SetActive(false);
            _costPanel.SetActive(CurrentUnit.Cost > 0);
        }

        protected override void ResourceValueChange()
        {
            _recruitButton.interactable = !UnitReloading && _resource.CanAfford(CurrentUnit.Cost);
        }

        protected override void ReloadStarted()
        {
            _recruitButton.interactable = false;

            _reloadPanel.SetActive(true);
            _costPanel.SetActive(false);
        }

        protected override void IncreaseRecruitsNumber()
        {
            var maxCheck = _currentRecruitsNumber < CurrentUnit.RecruitsNumber;

            _currentRecruitsNumberText.text = _currentRecruitsNumber.ToString();

            if (maxCheck) _recruitButton.interactable = false;
        }

        private async UniTask UpdateView()
        {
            _unitImage.sprite = await CurrentUnit.IconSprite.CustomLoadAssetAsync<Sprite>();

            if (CurrentUnit.ResourcesType != ResourcesType.None)
            {
                _resourceImage.sprite = await AddressableExtensions
                    .CustomLoadAssetAsync<Sprite>($"resource_icon_{(int)CurrentUnit.ResourcesType}");
                _costText.text = CurrentUnit.Cost.ToString();
            }

            _costPanel.SetActive(CurrentUnit.Cost > 0);
            _reloadPanel.SetActive(false);
            _reloadProgressBar.value = 0;

            _currentRecruitsNumberText.text = null;
        }
    }
}