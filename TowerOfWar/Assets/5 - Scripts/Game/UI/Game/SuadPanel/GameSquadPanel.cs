﻿using Data.Models.Level;
using Data.Models.Units;
using Game.Data;
using System.Collections;
using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;

namespace Game.UI.Game
{
    public class GameSquadPanel : MonoBehaviour
    {
        [SerializeField] List<GameUnitSlotButton> unitSlotItems = new List<GameUnitSlotButton>();

        IEnumerator Start() => UniTask.ToCoroutine(async () =>
        {
            var gameSession = GameManager.Instance.CurrentGameSession;
            var squadId = gameSession.SquadId;

            foreach (var unit in GameManager.Instance.GetSquadUnits())
            {
                foreach (var slot in unitSlotItems)
                {
                    var resource = unit.ResourcesType == ResourcesType.Mana ? gameSession.Mana : gameSession.ResourceWar;
                    if (await slot.Init(unit, squadId, resource)) break;
                }
            }

            // до полной инициализации дождаться старта игры
            while (gameSession.LevelState != LevelState.Started) await UniTask.Yield();

            foreach (var slot in unitSlotItems) slot.Activate();
        });
    }
}