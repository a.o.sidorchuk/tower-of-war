﻿using Game.Economy;
using Game.Level;
using System.Collections;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Game.UI.Game
{
    public class ManaView : MonoBehaviour
    {
        [SerializeField] Button _upgradeButton;
        [Space]
        [SerializeField] GameObject _costPanel;
        [SerializeField] Text _costValueText;
        [Space]
        [SerializeField] Text _currentValueText;
        [SerializeField] Slider _progressBar;
        [Space]
        [SerializeField] Image _manaIcon;
        [SerializeField] AssetReferenceSprite _manaSprite;

        CurrencyGainer _manaGainer;

        IEnumerator Start() => UniTask.ToCoroutine(async () =>
        {
            _manaIcon.sprite = await _manaSprite.CustomLoadAssetAsync<Sprite>();

            _manaGainer = LevelManager.Instance.ManaGainer;

            _manaGainer.currencyChanged += ManaChangeValue;

            _upgradeButton.onClick.AddListener(UpgradeButtonClick);
        });

        private void ManaChangeValue(CurrencyChangeInfo info)
        {
            _upgradeButton.interactable = _manaGainer.Currency.CurrentCurrency > (_manaGainer.MaxValue / 2);
            _currentValueText.text = $"{Mathf.CeilToInt(_manaGainer.Currency.CurrentCurrency)}";
            _progressBar.value = _manaGainer.Normalized;
        }

        private void UpgradeButtonClick()
        {
            if (_manaGainer.GainerUpgrade())
            {
                if (!_manaGainer.MaxLevelCheck)
                    _costValueText.text = (_manaGainer.MaxValue / 2).ToString();
                else
                    _costPanel.SetActive(false);
            }
        }
    }
}