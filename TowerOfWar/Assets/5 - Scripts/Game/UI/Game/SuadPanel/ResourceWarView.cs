﻿using Data.Models.Economy;
using Game.Data;
using System.Collections;
using UniRx.Async;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace Game.UI.Game
{
    public class ResourceWarView : MonoBehaviour
    {
        [SerializeField] Text _currentValueText;
        [Space]
        [SerializeField] Image _iconImage;
        [SerializeField] AssetReferenceSprite _resourceWarSprite;

        Currency _resourceWar;

        IEnumerator Start() => UniTask.ToCoroutine(async () =>
        {
            _iconImage.sprite = await _resourceWarSprite.CustomLoadAssetAsync<Sprite>();

            _resourceWar = GameManager.Instance.CurrentGameSession.ResourceWar;
            _resourceWar.currencyChanged += ResourceWarChangeEvent;

            ResourceWarChangeEvent();
        });

        private void ResourceWarChangeEvent()
        {
            _currentValueText.text = $"{Mathf.CeilToInt(_resourceWar.CurrentCurrency)}";
        }
    }
}