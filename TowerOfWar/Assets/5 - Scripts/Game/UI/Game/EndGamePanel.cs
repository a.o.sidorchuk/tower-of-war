﻿using Data.Models.Level;
using Game.Data;
using Game.Network;
using Game.UI.Menu;
using System;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Game
{
    public class EndGamePanel : ClosingPanel
    {
        [Space]
        [SerializeField] Text _resultInfoText;
        [SerializeField] Text _expText;
        [SerializeField] Text _goldText;

        bool _isWinner;

        protected override async UniTask OpenPanelEvent()
        {
            NetworkController.Instance.UpdateRoomProperty(NetworkController.HashKey.levelState, LevelState.Finished);

            await base.OpenPanelEvent();

            var loserSquadId = Convert.ToInt32(_menuMsgData);
            var gameSession = GameManager.Instance.CurrentGameSession;
            _isWinner = gameSession.SquadId != loserSquadId;

            if (_isWinner)
            {
                gameSession.ExpDrop.AddCurrency(100);
                gameSession.GoldDrop.AddCurrency(200);
            }

            _resultInfoText.text = _isWinner ? "ПОБЕДА!" : "ПОРАЖЕНИЕ!";
            _expText.text = _isWinner ? gameSession.ExpDrop.CurrentCurrency.ToString() : Mathf.RoundToInt(gameSession.ExpDrop.CurrentCurrency * 0.05f).ToString();
            _goldText.text = _isWinner ? gameSession.GoldDrop.CurrentCurrency.ToString() : "0";
        }

        protected override async UniTask ClosePanelEvent()
        {
            await GameManager.Instance.GetReward(_isWinner); 

            Photon.Pun.PhotonNetwork.LeaveRoom();
        }
    }
}