﻿using Core.Utilities;
using Game.Units;
using Photon.Pun;
using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Game
{
    public class GamePanel : MonoBehaviour
    {
        [SerializeField] Button _menuButton;
        [Space]
        [SerializeField] Slider _towerHealthBarA;
        [SerializeField] Slider _towerHealthBarB;
        [Space]
        [SerializeField] GameSquadPanel _squadPanel;
        [Space]
        [SerializeField] ManaView _manaView;
        [SerializeField] ResourceWarView _resourceWarView;

        PhotonView _photonView;
        IDisposable _rxMessage;

        void Awake()
        {
            _menuButton.onClick.AddListener(MenuClick);
            _photonView = GetComponent<PhotonView>();

            _rxMessage = MessageBroker.Default.Receive<GameMessage>()
                .Where(x => x.type == GameMessage.Type.TowerCreated)
                .Subscribe(x => SubscribeToTowerHealthChange((BaseUnit)x.data));
        }

        private void OnDestroy() => _rxMessage.Dispose();

        private void MenuClick()
        {
            // _TODO заменить на полноценное меню
            PhotonNetwork.LeaveRoom();

            if (PhotonNetwork.OfflineMode)
            {
                // поставить игру на паузу
            }

            // открыть окно "меню"
            MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.OpenGameMenuEvent, null, null));
        }

        public void SubscribeToTowerHealthChange(Units.BaseUnit unit)
        {
            unit.Configuration.healthChanged += x =>
            {
                if (x.damageable.IsDead)
                {
                    _photonView.RPC("RpcGameFinishedEvent", RpcTarget.All, unit.Configuration.SquadId);
                }

                _photonView.RPC("RpcTowerHealthChanged", RpcTarget.All, unit.Configuration.SquadId, x.damageable.NormalizedHealth);
            };
        }

        [PunRPC]
        void RpcGameFinishedEvent(int loserSquadId)
        {
            MessageBroker.Default.Publish(MenuMessage.Create(MenuMessage.Type.OpenEndGamePanel, loserSquadId, null));
        }

        [PunRPC]
        void RpcTowerHealthChanged(int squadId, float normalizedHealth)
        {
            switch (squadId)
            {
                case 1: _towerHealthBarA.value = normalizedHealth; break;
                case 2: _towerHealthBarB.value = normalizedHealth; break;
            }
        }
    }
}