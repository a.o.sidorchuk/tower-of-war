﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace Game.Playables
{
    public enum CharacterState
    {
        // M - перемещение
        M_Stand,
        M_Walk,
        M_Run,
        M_Sprint,
        // A - применение способности
        A_ApplyAbility,
        A_Ready,
        // D - смерть
        D_Death
    }

    public class CharacterPlayable : PlayableBehaviour
    {
        Dictionary<CharacterState, int> _clipIds = new Dictionary<CharacterState, int>();
        Playable _mixer;

        CharacterState _currentState = CharacterState.M_Stand;
        CharacterState _prevState = CharacterState.M_Stand;

        public float CurrentClipTime { get; private set; } = 0;

        public void Init(List<Animation> animations, Playable owner, PlayableGraph graph)
        {
            _mixer = AnimationMixerPlayable.Create(graph, animations.Count);

            owner.SetInputCount(1);
            graph.Connect(_mixer, 0, owner, 0);
            owner.SetInputWeight(0, 1);

            for (var clipIndex = 0; clipIndex < _mixer.GetInputCount(); ++clipIndex)
            {
                var clip = animations[clipIndex];

                _clipIds.Add(clip.State, clipIndex);

                graph.Connect(AnimationClipPlayable.Create(graph, clip.AnimationClip), 0, _mixer, clipIndex);

                _mixer.SetInputWeight(clipIndex, 0);
            }

            SetState(CharacterState.M_Stand);
        }


        public void SetState(CharacterState state)
        {
            //Debug.Log($"SetState[{_name}] = {_currentState} -> {state}");

            if (!_clipIds.ContainsKey(state)) return;

            if (_clipIds.ContainsKey(_currentState))
            {
                _prevState = _currentState;
                _mixer.SetInputWeight(_clipIds[_currentState], 0);
            }

            _currentState = state;

            var clipIndex = _clipIds[_currentState];
            var currentClip = (AnimationClipPlayable)_mixer.GetInput(clipIndex);

            CurrentClipTime = currentClip.GetAnimationClip().length;

            currentClip.SetTime(0);
            _mixer.SetInputWeight(clipIndex, 1);
        }

        public void Reset()
        {
            foreach (var clipId in _clipIds)
            {
                _mixer.GetInput(clipId.Value).SetTime(0);
                _mixer.SetInputWeight(clipId.Value, 0);
            }
        }

        public override void PrepareFrame(Playable playable, FrameData info)
        {
            CurrentClipTime -= info.deltaTime;

            if (CurrentClipTime <= 0)
            {
                switch (_currentState)
                {
                    case CharacterState.A_ApplyAbility: SetState(CharacterState.A_Ready); break;
                }
            }
        }
    }
}