﻿using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace Game.Playables
{
    /// <summary>
    /// Компонент для воспроизведения очереди анимаций
    /// </summary>
    public class PlayQueueAnimation : MonoBehaviour
    {
        [SerializeField] Animator _animator;
        [SerializeField] AnimationClip[] _animations;

        PlayableGraph _playableGraph;

        void Start()
        {
            _playableGraph = PlayableGraph.Create();

            var playQueuePlayable = ScriptPlayable<PlayQueuePlayable>.Create(_playableGraph);
            var playQueue = playQueuePlayable.GetBehaviour();

            playQueue.Init(_animations, playQueuePlayable, _playableGraph);

            var playableOutput = AnimationPlayableOutput.Create(_playableGraph, "Animation", _animator);
            playableOutput.SetSourcePlayable(playQueuePlayable, 0);

            _playableGraph.Play();
        }
    }
}