﻿using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace Game.Playables
{
    public class PlayQueuePlayable : PlayableBehaviour
    {
        int _currentClipIndex = -1;
        float _timeToNextClip;
        bool _loop;

        Playable _mixer;

        public void Init(AnimationClip[] clipsToPlay, Playable owner, PlayableGraph graph, bool loop = false)
        {
            _loop = loop;

            owner.SetInputCount(1);

            _mixer = AnimationMixerPlayable.Create(graph, clipsToPlay.Length);

            graph.Connect(_mixer, 0, owner, 0);
            owner.SetInputWeight(0, 1);

            for (var i = 0; i < _mixer.GetInputCount(); i++)
            {
                graph.Connect(AnimationClipPlayable.Create(graph, clipsToPlay[i]), 0, _mixer, i);
                _mixer.SetInputWeight(i, 1.0f);
            }
        }

        override public void PrepareFrame(Playable owner, FrameData info)
        {
            var clipCount = _mixer.GetInputCount();

            if (clipCount == 0 || _currentClipIndex >= clipCount) return;

            _timeToNextClip -= info.deltaTime;

            if (_timeToNextClip <= 0.0f)
            {
                _currentClipIndex++;

                var outOfRange = _currentClipIndex >= _mixer.GetInputCount();

                if (outOfRange && _loop)
                    _currentClipIndex = 0;
                else if (outOfRange)
                    return;

                var currentClip = (AnimationClipPlayable)_mixer.GetInput(_currentClipIndex);

                currentClip.SetTime(0);

                _timeToNextClip = currentClip.GetAnimationClip().length;

                Debug.Log(_timeToNextClip);
            }

            for (var clipIndex = 0; clipIndex < _mixer.GetInputCount(); ++clipIndex)
            {
                if (clipIndex == _currentClipIndex)
                    _mixer.SetInputWeight(clipIndex, 1.0f);
                else
                    _mixer.SetInputWeight(clipIndex, 0.0f);
            }
        }
    }
}