﻿using Data.Models.Units;
using Photon.Pun;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.Playables;

namespace Game.Playables
{
    [System.Serializable]
    public class Animation
    {
        [SerializeField] CharacterState _state;
        [SerializeField] AnimationClip _animationClip;

        #region public fields
        public CharacterState State => _state;
        public AnimationClip AnimationClip => _animationClip;
        #endregion
    }

    /// <summary>
    /// Компонент управления анимациями юнитов
    /// </summary>
    public class CharacterAnimation : MonoBehaviour
    {
        [SerializeField] Animator _animator;
        [SerializeField] List<Animation> _animations = new List<Animation>();

        Dictionary<MovementSpeedType, CharacterState> _movementToStateMap = new Dictionary<MovementSpeedType, CharacterState>()
        {
            { MovementSpeedType.None, CharacterState.M_Stand },
            { MovementSpeedType.Slowly, CharacterState.M_Walk },
            { MovementSpeedType.Usually, CharacterState.M_Run },
            { MovementSpeedType.Fast, CharacterState.M_Sprint }
        };


        CharacterPlayable _characterPlayable;
        PlayableGraph _playableGraph;

        PhotonView _photonView;

        public float CurrentClipTime => _characterPlayable.CurrentClipTime;

        void Awake()
        {
            _photonView = GetComponent<PhotonView>();

            _playableGraph = PlayableGraph.Create();

            if (_animations.Count == 0) return;

            var playQueuePlayable = ScriptPlayable<CharacterPlayable>.Create(_playableGraph);
            _characterPlayable = playQueuePlayable.GetBehaviour();

            _characterPlayable.Init(_animations, playQueuePlayable, _playableGraph);

            var playableOutput = AnimationPlayableOutput.Create(_playableGraph, "Animation", _animator);
            playableOutput.SetSourcePlayable(playQueuePlayable, 0);

            _playableGraph.Play();
        }

        public void SetState(CharacterState state)
        {
            _characterPlayable.SetState(state);
            _photonView.RPC("RpcSetCharacterState", RpcTarget.Others, (int)state);
        }

        public void SetMovementState(MovementSpeedType speedType) => SetState(_movementToStateMap[speedType]);
        public void SetReadyState() => SetState(CharacterState.A_Ready);
        public void SetApplyAbilityState() => SetState(CharacterState.A_ApplyAbility);
        public void SetDeathState() => SetState(CharacterState.D_Death);

        [PunRPC]
        public void RpcSetCharacterState(int state) => _characterPlayable.SetState((CharacterState)state);
    }
}