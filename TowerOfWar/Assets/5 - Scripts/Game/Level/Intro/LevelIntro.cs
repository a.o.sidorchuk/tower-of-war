﻿using System;
using UnityEngine;

namespace Game.Level
{
	public abstract class LevelIntro : MonoBehaviour
	{
		public event Action introCompleted;
		protected void CallIntroCompleted() => introCompleted?.Invoke();
	}
}