﻿using Core.Utilities;
using UnityEngine;

namespace Game.Level
{
	public class TimedLevelIntro : LevelIntro
	{
		[SerializeField] float time = 3f;

		protected NetTimer _timer;

		protected void Awake() => _timer = new NetTimer(time, CallIntroCompleted);

		protected void Update()
		{
			if (_timer != null)
			{
				if (_timer.Tick()) _timer = null; 
			}
		}
	}
}