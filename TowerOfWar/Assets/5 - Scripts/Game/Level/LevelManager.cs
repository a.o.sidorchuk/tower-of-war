﻿using Core.Utilities;
using Data.Models.Level;
using Data.Models.Player;
using Data.Models.Units;
using Game.AI;
using Game.Data;
using Game.Data.Level;
using Game.Data.Units;
using Game.Economy;
using Game.Network;
using Game.UI.Game;
using Game.Units;
using Newtonsoft.Json;
using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UniRx.Async;
using UnityEngine;

namespace Game.Level
{
    /// <summary>
    /// Центральный элемент с информацией обо всех игровых уровнях
    /// </summary>
    public class LevelManager : MonoSingleton<LevelManager>
    {
        [SerializeField] LevelIntro _levelIntro;
        [Space]
        [SerializeField] CurrencyGainer _manaGainer = new CurrencyGainer();

        Dictionary<int, List<UnitViewItem>> _squadUnits = new Dictionary<int, List<UnitViewItem>>();
        Dictionary<int, TowerUnit> _towers = new Dictionary<int, TowerUnit>();
        Dictionary<int, TowerSpawnPoint> _towerSpawnPoints = new Dictionary<int, TowerSpawnPoint>();

        public CurrencyGainer ManaGainer => _manaGainer;

        PhotonView _photonView;

        ContentLevelInfo _currentLevelInfo;
        GameSessionData _gameSession;

        protected override void Awake()
        {
            base.Awake();
            _photonView = GetComponent<PhotonView>();
            _towerSpawnPoints = FindObjectsOfType<TowerSpawnPoint>().ToDictionary(x => x.SquadId, x => x);

            _gameSession = GameManager.Instance.Store.GameSessionData;
            _currentLevelInfo = GameManager.Instance.CurrentLevelInfo;
        }

        IEnumerator Start() => UniTask.ToCoroutine(async () =>
        {
            // включение генератора маны
            _manaGainer.Init(_gameSession.Mana);

            if (PhotonNetwork.IsMasterClient) GameInit();

            if (PhotonNetwork.OfflineMode) await AIEnemyInit();
        });

        private void Update()
        {
            if (_gameSession.LevelState == LevelState.Started)
            {
                _manaGainer.Tick(Time.deltaTime);
            }
        }

        private void GameInit()
        {
            var squadNamesJson = (string)PhotonNetwork.CurrentRoom.CustomProperties[NetworkController.HashKey.squadNames];
            var squadNames = JsonConvert.DeserializeObject<ExitGames.Client.Photon.Hashtable>(squadNamesJson);

            foreach (var player in PhotonNetwork.CurrentRoom.Players)
            {
                // создать список юнитов
                squadNames.TryGetValue(player.Value.ActorNumber.ToString(), out object squadName);

                var squadId = Convert.ToInt32(squadName);

                var fractionType = (int)player.Value.CustomProperties[NetworkController.HashKey.fraction];
                var units = JsonConvert.DeserializeObject<Dictionary<int, int>>(player.Value.CustomProperties[NetworkController.HashKey.squad].ToString());

                var unitViewItems = GameManager.Instance.GetUnitViewItems(units, (FractionType)fractionType);

                _squadUnits.Add(squadId, unitViewItems);

                // поставить башню
                var tower = unitViewItems.FirstOrDefault(x => x.UnitType == UnitType.Building);
                if (tower != null) CreateTower(squadId, tower);
            }
        }

        private void CreateTower(int squadId, UnitViewItem tower)
        {
            _towerSpawnPoints.TryGetValue(squadId, out TowerSpawnPoint towerSpawnPoint);
            var spawnTransform = towerSpawnPoint.SpawnPosition;

            try
            {
                var towerUnit = (TowerUnit)SpawnUnit(squadId, tower, spawnTransform.position, spawnTransform.rotation);

                _towers.Add(squadId, towerUnit);

                MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.TowerCreated, towerUnit, null));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async UniTask AIEnemyInit()
        {
            // добавить юнитов ИИ
            var playerLevel = GameManager.Instance.Store.PlayerData.CurrentLevel;
            var enemyUnits = _currentLevelInfo.GetEnemyUnitViewItems(playerLevel);
            var squadId = (int)SquadName.B;

            _squadUnits.Add(squadId, enemyUnits);

            CreateTower(squadId, enemyUnits.FirstOrDefault(x => x.UnitType == UnitType.Building));

            // включить ИИ противника
            var stupidAI = new GameObject("SuperStupidAI").AddComponent<SuperStupidAI>();
            stupidAI.transform.SetParent(transform);
            await stupidAI.Init(enemyUnits);
        }

        public void UnitButtonClick(int unitId, int squadId)
        {
            _photonView.RPC("RpcUnitButtonClick", RpcTarget.MasterClient, unitId, squadId);
        }

        [PunRPC]
        void RpcUnitButtonClick(int unitId, int squadId)
        {
            _squadUnits.TryGetValue(squadId, out List<UnitViewItem> units);
            _towers.TryGetValue(squadId, out TowerUnit towerUnit);
            var unit = units.FirstOrDefault(x => x.UnitId == unitId);

            switch (unit.UnitType)
            {
                case UnitType.Building: towerUnit.ApplyTowerAbility(); break;

                case UnitType.BuildingModule:
                    var moduleSpawnPoint = towerUnit.RandomModuleSpawnPoint;
                    if (moduleSpawnPoint)
                    {
                        var moduleUnit = (NavUnit)SpawnUnit(squadId, unit, moduleSpawnPoint);
                        moduleUnit.SetMovementActive(false);
                    }
                    break;

                default:
                    // создать юнита в спавн точке башни
                    _towerSpawnPoints.TryGetValue(squadId, out TowerSpawnPoint towerSpawnPoint);
                    var unitSpawnNode = towerSpawnPoint.UnitSpawnNode;

                    var spawnPosition = unitSpawnNode.GetRandomPointInNodeArea();
                    var spawnRotation = unitSpawnNode.transform.rotation;

                    var navUnit = (NavUnit)SpawnUnit(squadId, unit, spawnPosition, spawnRotation);

                    navUnit.SetNode(unitSpawnNode);
                    break;
            }
        }

        private BaseUnit SpawnUnit(int squadId, UnitViewItem unit, Transform parent)
        {
            var unitComponent = SpawnUnit(squadId, unit, parent.position, parent.rotation);
            unitComponent.transform.SetParent(parent);
            return unitComponent;
        }

        private BaseUnit SpawnUnit(int squadId, UnitViewItem unit, Vector3 position, Quaternion rotation)
        {
            var unitComponent = Poolable.TryGetPoolableNet<BaseUnit>(unit.BasePrefab.RuntimeKey.ToString(), position, rotation);
            unitComponent.Init(squadId, unit);
            return unitComponent;
        }
    }
}