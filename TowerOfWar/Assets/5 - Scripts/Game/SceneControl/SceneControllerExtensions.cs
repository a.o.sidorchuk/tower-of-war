﻿using Core.SceneControl;
using Game.Data;
using UniRx.Async;

public static class SceneControllerExtensions
{
    public static void StartGame(this SceneController sceneController, SceneInfo sceneContent) => UniTask.ToCoroutine(async () =>
    {
        sceneController.LoadGameScene(sceneContent, FadeType.Loading);
    });

    public static void StartMenu(this SceneController sceneController) => UniTask.ToCoroutine(async () =>
    {
        sceneController.LoadMenuScene(GameManager.Instance.Store.PlayerData.FractionType, FadeType.Loading);
    });
}