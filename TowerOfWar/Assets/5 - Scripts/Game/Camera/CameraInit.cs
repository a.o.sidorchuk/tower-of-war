﻿using UnityEngine;

namespace Game.Camera
{
    /// <summary>
    /// Инициализация камеры при старте сцены и настройка под разные разрешения экранов
    /// </summary>
    public class CameraInit : MonoBehaviour
    {
        private void Awake()
        {
            var camera = GetComponent<UnityEngine.Camera>();
            if (!camera) return;

            var ratio = Screen.width * 1f / Screen.height;

            transform.position += transform.forward * -2f * ratio;

            camera.fieldOfView = 1f / Mathf.Sqrt(ratio * ratio) * 88f;
        }
    }
}