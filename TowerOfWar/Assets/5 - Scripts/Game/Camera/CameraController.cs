﻿using Core.Input;
using Data.Models.Level;
using Game.Data;
using Game.Units;
using Photon.Pun;
using System.Collections;
using System.Linq;
using UniRx.Async;
using UnityEngine;

namespace Game.Camera
{
    /// <summary>
    /// Управление игровой камерой
    /// </summary>
    public class CameraController : MonoBehaviour
    {
        [SerializeField] float _smoothTime = 0.2f;
        [SerializeField] float _towerPadding = 5f;
        [Space]

        RangeInt _mapSize;
        Vector3 _startPosition, _cameraPosition, _currentVelocity;

        int _startDelay = 0;

        IEnumerator Start() => UniTask.ToCoroutine(async () =>
        {
            _startDelay = PhotonNetwork.OfflineMode ? 0 : 1500;

            await UniTask.Delay(_startDelay);

            Init();

            MoveTo(_startPosition);
        });


        private void OnEnable()
        {
            PlayerInput.Instance.moveEvent += MoveEvent;
            PlayerInput.Instance.swipeEvent += SwipeEvent;
        }

        private void OnDisable()
        {
            PlayerInput.Instance.moveEvent -= MoveEvent;
            PlayerInput.Instance.swipeEvent -= SwipeEvent;
        }

        private void Init()
        {
            if (GameManager.Instance.Store.GameSessionData == null) return;

            var currentSquadId = GameManager.Instance.Store.GameSessionData.SquadId;

            var squadStartPoints = FindObjectsOfType<TowerSpawnPoint>();

            if (squadStartPoints.Length < 2)
            {
                Debug.LogError("<b>[CameraRig]: </b>Не хватает точек спавна!");
                return;
            }

            var pointA = squadStartPoints.FirstOrDefault(x => x.SquadName == SquadName.A).transform;
            var pointB = squadStartPoints.FirstOrDefault(x => x.SquadName == SquadName.B).transform;

            if (pointA && pointB)
            {
                var xA = (int)(pointA.position.x + _towerPadding);
                var xB = (int)(pointB.position.x - _towerPadding);

                _mapSize = new RangeInt(xA, Mathf.Abs(xA) + Mathf.Abs(xB));

                var squadIdCheck = currentSquadId == (int)SquadName.A;
                _startPosition = squadIdCheck ? pointA.position : pointB.position;
                _startPosition.x = squadIdCheck ? xA : xB;
            }
            else
            {
                Debug.LogError($"<b>[CameraRig]: </b>Нет одной из точек: A({pointA}) || B({pointB})");
            }
        }

        void Update()
        {
            PositionUpdate();
        }

        private void PositionUpdate()
        {
            var position = transform.position;
            position = Vector3.SmoothDamp(position, _cameraPosition, ref _currentVelocity, _smoothTime);
            transform.position = position;
        }

        private void MoveTo(Vector3 position)
        {
            Vector3 pos = position;
            pos.x = Mathf.Clamp(pos.x, _mapSize.start, _mapSize.end);
            _cameraPosition = pos;
        }

        public void ShiftTo(Vector3 delta)
        {
            MoveTo(_cameraPosition + delta);
        }

        private void SwipeEvent(Vector2 screenDelta, Vector3 worldPos)
        {
            screenDelta.y = 0;
            ShiftTo(-screenDelta * 5f);
        }

        private void MoveEvent(Vector3 worlDelta, Vector2 screenDelta, Vector3 worldPos)
        {
            screenDelta.y = 0;
            ShiftTo(-screenDelta * .5f);
        }
    }
}

