﻿using ActionGameFramework.Projectiles;
using Core.Effects;
using Core.Utilities;
using Game.Units;
using UnityEngine;

namespace Game.Effects
{
    /// <summary> Компонент управления эффектами использования способности типа "снаряд" </summary>
    /// <remarks>
    /// Компонент предназначен для визуализации эффектов снаряда во время полета до цели
    /// </remarks>
    [RequireComponent(typeof(IProjectile))]
    public class AbilityVisualEffect : MonoBehaviour
    {
        [Tooltip("Эффект способности юнита")]
        [SerializeField] GameObject _effectPrefab;

        [Tooltip("Отслеживаемая позиция для эффекта")]
        [SerializeField] Transform _followTransform;

        GameObject _spawnedEffect;

        SelfDestroyTimer _destroyTimer;
        PoolableEffect _resetter;

        IProjectile _abilityProjectile;

        protected virtual void Awake()
        {
            _abilityProjectile = GetComponent<IProjectile>();
            _abilityProjectile.fired += OnFired;

            if (_followTransform != null) _followTransform = transform;
        }

        protected virtual void OnDestroy() => _abilityProjectile.fired -= OnFired;

        /// <summary> Запустить эффекты </summary>
        protected virtual void OnFired()
        {
            if (_effectPrefab != null)
            {
                _spawnedEffect = Poolable.TryGetPoolable(_effectPrefab);

                _spawnedEffect.transform.SetParent(null);

                _spawnedEffect.transform.position = _followTransform.position;
                _spawnedEffect.transform.rotation = _followTransform.rotation;

                _destroyTimer = _spawnedEffect.GetComponent<SelfDestroyTimer>();
                if (_destroyTimer != null) _destroyTimer.enabled = false;

                _resetter = _spawnedEffect.GetComponent<PoolableEffect>();
                if (_resetter != null) _resetter.TurnOnAllSystems();
            }
        }

        /// <summary> Отслеживать позицию и перемещать эффект </summary>
        protected virtual void Update()
        {
            if (_spawnedEffect != null)
            {
                _spawnedEffect.transform.position = _followTransform.position;
            }
        }

        protected virtual void OnDisable()
        {
            if (_spawnedEffect == null) return;

            if (_destroyTimer != null)
            {
                _destroyTimer.enabled = true;
                _resetter?.StopAll(); 
            }
            else
            {
                Poolable.TryPool(_spawnedEffect);
            }

            _spawnedEffect = null;
            _destroyTimer = null;
        }
    }
}