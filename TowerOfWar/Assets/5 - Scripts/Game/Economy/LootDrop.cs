﻿using Core.Health;
using Game.Units;
using Photon.Pun;
using UnityEngine;

namespace Game.Economy
{
    /// <summary> Базовый компонент для реализации выпадаемой награды с убитых юнитов </summary>
    [RequireComponent(typeof(BaseUnit))]
    public abstract class LootDrop : MonoBehaviour
    {
        public virtual int LootDropped { get; } = 1;

        protected BaseUnit _unit;
        protected PhotonView _photonView;

        protected virtual void OnEnable()
        {
            if (_photonView == null) _photonView = GetComponent<PhotonView>();
            if (_unit == null) _unit = GetComponent<BaseUnit>();
            _unit.Configuration.died += OnDeath;
        }

        protected virtual void OnDisable()
        {
            _unit.Configuration.died -= OnDeath;
        }

        private void OnDeath(HealthChangeInfo info)
        {
            _unit.Configuration.died -= OnDeath;

            if (info.squadId == _unit.Configuration.SquadId) return; 

            LootDropEvent();
        }

        protected abstract void LootDropEvent();
    }
}