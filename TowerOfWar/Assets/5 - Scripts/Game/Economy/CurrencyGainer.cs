﻿using System;
using Data.Models.Economy;
using Core.Utilities;
using UnityEngine;
using System.Collections.Generic;

namespace Game.Economy
{
    [Serializable]
    public class CurrencyGainerLevel
    {
        [Tooltip("Количество начисляемой валюты")]
        [SerializeField] float _currencyAddition;
        [Tooltip("Время интервалов начисления валюты, ms")]
        [SerializeField] float _currencyTime;
        [Tooltip("Максимальное количество валюты")]
        [SerializeField] int _maxValue;

        #region public fields
        public float CurrencyAddition => _currencyAddition;
        public float CurrencyTime => _currencyTime;
        public int MaxValue => _maxValue;
        #endregion
    }

    /// <summary> Компонент генерации валюты </summary>
    [Serializable]
    public class CurrencyGainer
    {
        public event Action<CurrencyChangeInfo> currencyChanged;

        [SerializeField] List<CurrencyGainerLevel> _currencyLevels = new List<CurrencyGainerLevel>();
        int _currentLevelIndex = 0;

        int _maxLevel;
        public bool MaxLevelCheck => _currentLevelIndex >= _maxLevel;

        public Currency Currency { get; private set; }
        public CurrencyGainerLevel CurrentLevel => _currencyLevels[_currentLevelIndex];
        public float Normalized => Currency.CurrentCurrency / MaxValue;
        public int MaxValue => CurrentLevel.MaxValue;

        public List<CurrencyGainerLevel> CurrencyLevels => _currencyLevels;

        RepeatingTimer _repeatingTimer;

        public void Init(Currency currencyController, List<CurrencyGainerLevel> currencyLevels = null)
        {
            if (currencyLevels != null) _currencyLevels = currencyLevels;
            
            _repeatingTimer = new RepeatingTimer(time: 0, ConstantGain, checkingTime: true);
            _maxLevel = _currencyLevels.Count - 1;

            Currency = currencyController;

            UpdateParams();
        }

        public void SetMaxLevel(int maxLevel) => _maxLevel = maxLevel;

        public void Tick(float deltaTime) => _repeatingTimer.Tick();

        public bool GainerUpgrade()
        {
            if (_currentLevelIndex >= _currencyLevels.Count - 1) return false;

            var result = Currency.TryPurchase(CurrentLevel.MaxValue / 2);

            if (result)
            {
                _currentLevelIndex++;

                UpdateParams();
            }

            return result;
        }

        public void UpdateParams()
        {
            _repeatingTimer.SetTime(CurrentLevel.CurrencyTime);
            Currency.UpdateMaxValue(CurrentLevel.MaxValue);
        }

        /// <summary> Увеличить значение валюты </summary>
        protected void ConstantGain()
        {
            var previousCurrency = Currency.CurrentCurrency;

            Currency.AddCurrency(CurrentLevel.CurrencyAddition);

            currencyChanged?.Invoke(new CurrencyChangeInfo(previousCurrency, Currency.CurrentCurrency));
        }
    }
}