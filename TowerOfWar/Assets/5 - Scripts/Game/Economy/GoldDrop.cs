﻿using Game.Data;
using Photon.Pun;
using UnityEngine;

namespace Game.Economy
{
    public class GoldDrop : LootDrop
    {
        protected override void LootDropEvent()
        {
            // _TODO добавить зависимоти от параметров юнита
            var randomGold = Random.Range(0, 100);
            _photonView.RPC("RpcGoldDrop", RpcTarget.All, _unit.Configuration.SquadId, randomGold);
        }

        [PunRPC]
        void RpcGoldDrop(int squadId, int gold)
        {
            if (GameManager.Instance.CurrentGameSession.SquadId == squadId)
                GameManager.Instance.CurrentGameSession.GoldDrop.AddCurrency(gold);
        }
    }
}