﻿using Game.Data;
using Photon.Pun;
using UnityEngine;

namespace Game.Economy
{
    public class ExpDrop : LootDrop
    {
        protected override void LootDropEvent()
        {
            var randomExp = Random.Range(10, 50);
            _photonView.RPC("RpcExpDrop", RpcTarget.All, _unit.Configuration.SquadId, randomExp);
        }

        [PunRPC]
        void RpcExpDrop(int squadId, int gold)
        {
            if (GameManager.Instance.CurrentGameSession.SquadId == squadId)
                GameManager.Instance.CurrentGameSession.ExpDrop.AddCurrency(gold);
        }
    }
}