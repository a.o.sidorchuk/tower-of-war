﻿using UnityEngine;

namespace Game.Economy
{
	/// <summary> Структура для хранения данных об изменении валюты </summary>
	public struct CurrencyChangeInfo
	{
		public readonly float previousCurrency;
		public readonly float currentCurrency;

		public readonly float difference;
		public readonly float absoluteDifference;

		public CurrencyChangeInfo(float previous, float current)
		{
			previousCurrency = previous;
			currentCurrency = current;
			difference = currentCurrency - previousCurrency;
			absoluteDifference = Mathf.Abs(difference);
		}
	}
}