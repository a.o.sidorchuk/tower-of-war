﻿using Game.Data;
using Photon.Pun;
using UnityEngine;

namespace Game.Economy
{
    public class ResourceWarDrop : LootDrop
    {
        protected override void LootDropEvent()
        {
            var randomRW = Random.Range(0, 50);
            _photonView.RPC("RpcResourceWarDrop", RpcTarget.All, _unit.Configuration.SquadId, randomRW);
        }

        [PunRPC]
        void RpcResourceWarDrop(int squadId, int gold)
        {
            if (GameManager.Instance.CurrentGameSession.SquadId == squadId)
                GameManager.Instance.CurrentGameSession.ResourceWar.AddCurrency(gold);
        }
    }
}