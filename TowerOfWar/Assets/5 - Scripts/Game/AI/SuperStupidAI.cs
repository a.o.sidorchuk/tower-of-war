﻿using Data.Models.Economy;
using Data.Models.Level;
using Data.Models.Units;
using Game.Data;
using Game.Data.Units;
using Game.Economy;
using Game.Level;
using Game.Units.Recruiting;
using System.Collections.Generic;
using System.Linq;
using UniRx.Async;
using UnityEngine;

namespace Game.AI
{
    /// <summary>
    /// Максимально простой ИИ
    /// </summary>
    public class SuperStupidAI : MonoBehaviour
    {
        CurrencyGainer _manaGainer = new CurrencyGainer();
        Currency _resourceWar = new Currency(0);    // _TODO получать лут с юнитов игрока

        int _maxManaLevel;

        GameSessionData _gameSession;

        List<GameUnitSlot> _enemyUnits = new List<GameUnitSlot>();
        GameUnitSlot _tower;
        GameUnitSlot _towerModule;

        float _timer = 5;

        float _manaUpgradeChance = 0.2f;
        float _towerUseChance = 0.35f;

        bool _initialized;

        private void Awake()
        {
            _gameSession = GameManager.Instance.Store.GameSessionData;
            _manaGainer.Init(new Currency(0), LevelManager.Instance.ManaGainer.CurrencyLevels);
        }

        public async UniTask Init(List<UnitViewItem> enemyUnits)
        {
            // создаем слоты юнитов для ИИ
            _tower = await CreateUnitSlot(enemyUnits.FirstOrDefault(x => x.UnitType == UnitType.Building));
            _towerModule = await CreateUnitSlot(enemyUnits.FirstOrDefault(x => x.UnitType == UnitType.BuildingModule));

            foreach (var unit in enemyUnits.Where(x => x.UnitType != UnitType.Building && x.UnitType != UnitType.BuildingModule))
            {
                _enemyUnits.Add(await CreateUnitSlot(unit));
            }

            // максимальный уровень маны необходимый этому ИИ для призыва всех юнитов
            var maxManaCost = _enemyUnits.Max(x => x.CurrentUnit.Cost);
            _maxManaLevel = _manaGainer.CurrencyLevels.FindIndex(x => x.MaxValue > maxManaCost);
            _manaGainer.SetMaxLevel(_maxManaLevel + 1); // +1 для чуть более быстрого накопления маны

            _initialized = true;

            while (_gameSession.LevelState != LevelState.Started) await UniTask.Yield();
            _tower.Activate();
        }

        private async UniTask<GameUnitSlot> CreateUnitSlot(UnitViewItem unitViewItem)
        {
            if (unitViewItem == null) return null;

            var slot = new GameObject(unitViewItem.UnitId.ToString()).AddComponent<GameUnitSlot>();
            slot.transform.SetParent(transform);

            await slot.Init(unitViewItem, (int)SquadName.B, unitViewItem.ResourcesType == ResourcesType.Mana ? _manaGainer.Currency : _resourceWar);

            return slot;
        }

        private void Update()
        {
            if (_gameSession.LevelState == LevelState.Started)
            {
                _manaGainer.Tick(Time.deltaTime);

                if (_initialized) Tick(Time.deltaTime); 
            }
        }

        private void Tick(float deltaTime)
        {
            _timer -= deltaTime;

            if (_timer < 0 && _gameSession.LevelState == LevelState.Started)
            {
                var rndValue = Random.value;
                var currentMana = _manaGainer.Currency.CurrentCurrency;

                // попробовать поднять уровень маны
                if (rndValue <= _manaUpgradeChance)
                {
                    _manaGainer.GainerUpgrade();
                }

                // попытка увеличить количество модулей
                if (_towerModule && !_towerModule.UnitReloading) _towerModule.ReqruitUnitProcess();

                // попытка использовать башню
                if (_tower && !_tower.UnitReloading && rndValue <= _towerUseChance) 
                    _tower.ReqruitUnitProcess();

                // попробовать призвать юнит
                var units = _enemyUnits.Where(x => !x.UnitReloading && x.ReqruitingCheck()).ToList();
                if (units.Count > 0 && rndValue > _manaUpgradeChance)
                {
                    var rndUnit = units[Random.Range(0, units.Count)];
                    rndUnit.ReqruitUnitProcess();
                }

                _timer = Random.Range(4, 7);
            }
        }
    }
}