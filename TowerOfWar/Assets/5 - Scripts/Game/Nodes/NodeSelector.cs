﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Nodes
{
    /// <summary>Базовая реализация способа выборки узла для перемещения юнитов</summary>
    public abstract class NodeSelector : MonoBehaviour
    {
        [SerializeField] protected List<Node> _nextNodes;
        [SerializeField] protected List<Node> _prevNodes;

        public abstract Node NextNode { get; }
        public abstract Node PrevNode { get; }

        #region gizmo
#if UNITY_EDITOR
        /// <summary>Отображение связей между узлами</summary>
        protected virtual void OnDrawGizmos()
        {
            PrintNodes(_nextNodes, Color.green);
        }

        private void PrintNodes(List<Node> _nodes, Color color)
        {
            if (_nodes == null) return;

            var c = Gizmos.color;
            Gizmos.color = color;

            var count = _nodes.Count;
            for (var i = 0; i < count; i++)
            {
                var node = _nodes[i];
                if (node != null)
                {
                    Gizmos.DrawLine(transform.position, node.transform.position);
                }
            }

            Gizmos.color = c;
        }
#endif
        #endregion
    }
}