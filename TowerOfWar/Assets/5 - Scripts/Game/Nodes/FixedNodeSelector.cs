﻿using Core.Extensions;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEngine;
#endif

namespace Game.Nodes
{
    /// <summary>Выбор узла из заданного списка</summary>
    public class FixedNodeSelector : NodeSelector
    {
        protected int _nodeIndex;

        public override Node NextNode => GetNode(_nextNodes);
        public override Node PrevNode => GetNode(_prevNodes);

        protected Node GetNode(List<Node> list)
        {
            if (list.Next(ref _nodeIndex, true))
            {
                return list[_nodeIndex];
            }
            return null;
        }

        #region gizmo
#if UNITY_EDITOR
        protected override void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            base.OnDrawGizmos();
        }
#endif
        #endregion
    }
}