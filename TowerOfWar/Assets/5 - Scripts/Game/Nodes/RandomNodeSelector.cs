﻿using Core.Extensions;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEngine;
#endif

namespace Game.Nodes
{
    /// <summary>Выбор случайного узла из списка</summary>
    public class RandomNodeSelector : NodeSelector
    {
        /// <summary> Сумма следующих узлов </summary>
        protected int _weightSumNext;
        /// <summary> Сумма предыдущих узлов </summary>
        protected int _weightSumPrev;

        public override Node NextNode => GetNode(_nextNodes, _weightSumNext);
        public override Node PrevNode => GetNode(_prevNodes, _weightSumPrev);

        protected Node GetNode(List<Node> nodes, int weightSum)
        {
            if (nodes == null) return null;
            return nodes.WeightedSelection(weightSum, t => t.weight);
        }

        /// <summary> Суммирует веса связанных узлов для случайного выбора </summary>
        protected int TotalLinkedNodeWeights(List<Node> list) => list.Sum(x => x.weight);

        protected void Awake()
        {
            _weightSumNext = TotalLinkedNodeWeights(_nextNodes);
            _weightSumPrev = TotalLinkedNodeWeights(_prevNodes);
        }

        #region gizmo
#if UNITY_EDITOR
        protected override void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            base.OnDrawGizmos();
        }
#endif
        #endregion
    }
}