﻿using Game.Nodes.MeshCreator;
using Game.Units;
using UnityEngine;

namespace Game.Nodes
{
    /// <summary> Область на пути юнитов из которой выбирается следующая позиция для перемещения </summary>
    [RequireComponent(typeof(Collider))]
    public class Node : MonoBehaviour
    {
        public AreaMeshCreator AreaMesh { get; private set; }

        /// <summary> Вес данного узла при выборе направления движения </summary>
        public int weight = 1;

        /// <summary>Получить следующий узел</summary>
        public Node GetNextNode()
        {
            var selector = GetComponent<NodeSelector>();
            return selector == null ? null : selector.NextNode;
        }

        /// <summary>Получить предыдущий узел</summary>
        public Node GetPrevNode()
        {
            var selector = GetComponent<NodeSelector>();
            return selector == null ? null : selector.PrevNode;
        }

        /// <summary> Получить случайную точку в текущем узле </summary>
        /// <returns>A random point within the MeshObject's area</returns>
        public Vector3 GetRandomPointInNodeArea()
        {
            return AreaMesh == null ? transform.position : AreaMesh.GetRandomPointInside();
        }

        /// <summary> Когда юнит входит в тригер, выдать следующую цель </summary>
        public virtual void OnTriggerEnter(Collider other)
        {
            var agent = other.gameObject.GetComponent<NavUnit>();
            if (agent != null) agent.GetNextNode(this);
        }

#if UNITY_EDITOR
        /// <summary> Проверить и переключить коллайдер в режим тригера </summary>
        protected void OnValidate()
        {
            var trigger = GetComponent<Collider>();

            if (trigger != null) trigger.isTrigger = true;

            if (AreaMesh == null)
            {
                AreaMesh = GetComponentInChildren<AreaMeshCreator>();
            }
        }
#endif
    }
}