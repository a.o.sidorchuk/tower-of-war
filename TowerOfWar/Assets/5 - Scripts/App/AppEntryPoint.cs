﻿using Core.SceneControl;
using UniRx.Async;
using UnityEngine;
using System.Collections;
using Game.Data;
using GooglePlayGames;

namespace Game.App
{
    /// <summary> Точка входа и инициализации приложения </summary>
    public class AppEntryPoint : MonoBehaviour
    {
        IEnumerator Start() => UniTask.ToCoroutine(async () =>
        {
            var loadingScreenFader = ScreenFader.Instance.GetScreenFaderByType(FadeType.Loading);
            var loadingScreen = loadingScreenFader.gameObject.GetComponent<LoadingScreen>();

            #region подключение к Google Play Services
            loadingScreen.SetText("Подключаемся к Google Play Services");

            await GooglePlayUtils.GooglePlayServicesConnect();

            Debug.Log($"[Google Play Services] - Connected: {PlayGamesPlatform.Instance.IsAuthenticated()}");
            #endregion

            #region инициализация GameManager
            loadingScreen.SetText("Проверка сохранений");

            while (!GameManager.Instance.Initialized) await UniTask.Yield();
            #endregion

            #region запуск игрового меню
            loadingScreen.SetText(null);

            SceneController.Instance.StartMenu();
            #endregion
        });
    }
}