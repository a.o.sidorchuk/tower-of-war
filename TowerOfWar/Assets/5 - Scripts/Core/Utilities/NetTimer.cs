﻿using System;
using UnityEngine;

namespace Core.Utilities
{
    /// <summary> Таймер с синхронизацией по сети </summary>
    public class NetTimer
    {
        readonly Action _callback;

        float _time, _startTime;
        bool _checkingTime;

        public float NormalizedProgress => Mathf.Clamp(_startTime / _time, 0f, 1f);

        public NetTimer(float newTime, Action onElapsed = null, bool checkingTime = false)
        {
            SetTime(newTime);
            _checkingTime = checkingTime;
            _startTime = Convert.ToSingle(Photon.Pun.PhotonNetwork.Time);
            _callback += onElapsed;
        }

        public virtual bool Tick() => AssessTime();

        protected bool AssessTime()
        {
            var currentReloadingTime = Convert.ToSingle(Photon.Pun.PhotonNetwork.Time) - _startTime;
            if (currentReloadingTime >= _time)
            {
                var count = Mathf.FloorToInt(currentReloadingTime / _time);

                if (count < 1 || !_checkingTime) count = 1;

                for (var i = 0; i < count; i++) FireEvent();
                return true;
            }
            return false;
        }

        public void Reset() => _startTime = Convert.ToSingle(Photon.Pun.PhotonNetwork.Time);

        public void FireEvent() => _callback.Invoke();

        public void SetTime(float newTime)
        {
            _time = newTime;
            if (newTime <= 0) _time = 0.1f;
        }
    }
}