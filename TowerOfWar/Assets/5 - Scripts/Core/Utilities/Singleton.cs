﻿namespace Core.Utilities
{
    public class Singleton<T> where T : new()
    {
        static T _instance;
        public static T Instance => (_instance == null) ? _instance = new T() : _instance;
    }
}
