﻿using System;

namespace Core.Utilities
{
	public class RepeatingTimer : NetTimer
	{
		public RepeatingTimer(float time, Action onElapsed = null, bool checkingTime = false) : base(time, onElapsed, checkingTime) { }

		public override bool Tick()
		{
			if (AssessTime()) Reset(); 
			return false;
		}
	}
}