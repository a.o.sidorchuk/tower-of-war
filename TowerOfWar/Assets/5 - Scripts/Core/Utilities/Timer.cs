﻿using System;
using UnityEngine;

namespace Core.Utilities
{
	public class Timer
    {
		readonly Action _callback;

		float _time, _currentTime;

		public float NormalizedProgress => Mathf.Clamp(_currentTime / _time, 0f, 1f);

		public Timer(float newTime, Action onElapsed = null)
		{
			SetTime(newTime);
			_currentTime = 0f;
			_callback += onElapsed;
		}

		public virtual bool Tick(float deltaTime) => AssessTime(deltaTime);

		protected bool AssessTime(float deltaTime)
		{
			_currentTime += deltaTime;
			if (_currentTime >= _time)
			{
				FireEvent();
				return true;
			}
			return false;
		}

		public void Reset() => _currentTime = 0;

		public void FireEvent() => _callback.Invoke();

		public void SetTime(float newTime)
		{
			_time = newTime;
			if (newTime <= 0) _time = 0.1f; 
		}
	}
}