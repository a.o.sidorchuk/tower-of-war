﻿using UnityEngine;

namespace Core.Utilities
{
    public class DontDestroyOnLoad : MonoBehaviour
    {
        void Start()
        {
            gameObject.transform.SetParent(null);
            DontDestroyOnLoad(gameObject);
        }
    }
}