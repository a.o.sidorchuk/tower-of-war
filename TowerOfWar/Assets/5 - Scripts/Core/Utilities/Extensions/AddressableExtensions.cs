﻿using System.Collections.Generic;
using System.Linq;
using UniRx.Async;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace UnityEngine.AddressableAssets
{
    public static class AddressableExtensions
    {
        public static async UniTask LoadSceneAsync(string sceneName, LoadSceneMode sceneMode = LoadSceneMode.Single)
        {
            var loadProcess = true;
            Addressables.LoadSceneAsync(sceneName, sceneMode).Completed += (x) => { loadProcess = false; };
            while (loadProcess) await UniTask.Yield();
        }

        public static async UniTask<T> CustomLoadAssetAsync<T>(this AssetReference reference) where T : Object
        {
            T asset = null;

            if (string.IsNullOrEmpty(reference.RuntimeKey.ToString())) return null;

            var process = true;
            reference.LoadAssetAsync<T>().Completed += (result) =>
            {
                asset = result.Result;
                process = false;
            };

            while (process) await UniTask.Yield();

            return asset;
        }

        public static async UniTask<T> CustomLoadAssetAsync<T>(string assetName) where T : Object
        {
            T asset = null;

            var process = true;
            Addressables.LoadAssetAsync<T>(assetName).Completed += (result) =>
            {
                asset = result.Result;
                process = false;
            };

            while (process) await UniTask.Yield();

            return asset;
        }

        public static async UniTask<List<T>> CustomLoadAssetsAsync<T>(string assetName) where T : Object
        {
            List<T> assets = null;

            var process = true;
            Addressables.DownloadDependenciesAsync(assetName).Completed += (resource) =>
            {
                var bundleResource = (ICollection<IAssetBundleResource>)resource.Result;
                var bundle = bundleResource.FirstOrDefault();

                if (bundle != null)
                {
                    assets = bundle.GetAssetBundle().LoadAllAssets<T>().ToList();
                }

                process = true;
            };

            while (process) await UniTask.Yield();

            return assets;
        }
    }
}

