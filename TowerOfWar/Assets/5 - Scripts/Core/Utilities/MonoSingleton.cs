﻿using UnityEngine;

namespace Core.Utilities
{
    public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
    {
        public static T Instance { get; private set; }

        public static bool InstanceExists => Instance != null;

        protected virtual void Awake()
        {
            if (InstanceExists) Destroy(gameObject); else Instance = (T)this;
        }

        protected virtual void OnDestroy()
        {
            if (Instance == this) Instance = null;
        }
    }
}