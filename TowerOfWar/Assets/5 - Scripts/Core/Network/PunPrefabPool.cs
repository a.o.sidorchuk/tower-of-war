﻿using Core.Utilities;
using Photon.Pun;
using UnityEngine;

namespace Core.Network
{
    /// <summary> Реализация пула сетевых объектов. Связан с локальным пулом каждого игрока. </summary>
    public class PunPrefabPool : IPunPrefabPool
    {
        public GameObject Instantiate(string prefabId, Vector3 position, Quaternion rotation)
        {
            var instance = PoolManager.Instance.GetPoolable(prefabId);
            instance.transform.position = position;
            instance.transform.rotation = rotation;
            return instance.gameObject;
        }

        public void Destroy(GameObject gameObject)
        {
            PoolManager.Instance.ReturnPoolable(gameObject.GetComponent<Poolable>());
        }
    }
}