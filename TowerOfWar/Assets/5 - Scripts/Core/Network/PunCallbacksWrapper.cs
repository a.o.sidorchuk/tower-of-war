﻿using Photon.Pun;
using Photon.Realtime;
using System;
using ExitGames.Client.Photon;

namespace Core.Network
{
    /// <summary> Обертка для собйтий Photon Network </summary>
    public class PunCallbacksWrapper : MonoBehaviourPunCallbacks
    {
        public event Action onConnectedToServer;
        public event Action onJoinedRoom;
        public event Action onLeftRoom;
        public event Action<Player> onPlayerEnteredRoom;
        public event Action<Player> onPlayerLeftRoom;
        public event Action<Hashtable> onRoomPropertiesUpdate;
        public event Action<Player, Hashtable> onPlayerPropertiesUpdate;

        public event Action<short, string> onJoinRandomFailed;
        public event Action<short, string> onJoinRoomFailed;
        public event Action<DisconnectCause> onDisconnected;

        public override void OnConnectedToMaster() => onConnectedToServer?.Invoke();
        public override void OnJoinedRoom() => onJoinedRoom?.Invoke();

        public override void OnDisconnected(DisconnectCause cause) => onDisconnected?.Invoke(cause);
        public override void OnJoinRandomFailed(short returnCode, string message) => onJoinRandomFailed?.Invoke(returnCode, message);
        public override void OnJoinRoomFailed(short returnCode, string message) => onJoinRoomFailed?.Invoke(returnCode, message);
        public override void OnLeftRoom() => onLeftRoom?.Invoke();

        public override void OnPlayerEnteredRoom(Player newPlayer) => onPlayerEnteredRoom?.Invoke(newPlayer);
        public override void OnPlayerLeftRoom(Player otherPlayer) => onPlayerLeftRoom?.Invoke(otherPlayer);
        public override void OnRoomPropertiesUpdate(Hashtable properties) => onRoomPropertiesUpdate?.Invoke(properties);
        public override void OnPlayerPropertiesUpdate(Player player, Hashtable properties) => onPlayerPropertiesUpdate?.Invoke(player, properties);
    }
}