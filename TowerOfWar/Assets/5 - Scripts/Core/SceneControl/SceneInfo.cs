﻿using Data.Abstractions;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Core.SceneControl
{
    public enum SceneType
    {
        Content,
        Menu,
        Game
    }

    /// <summary>
    /// Данные о сцене с сылкой на AssetReference
    /// </summary>
    [CreateAssetMenu(menuName = "Levels/Base Scene", order = 2)]
    public class SceneInfo : BaseScriptableObjectEntity<SceneInfo>
    {
        [SerializeField] SceneType _type;
        [SerializeField] AssetReference _scene;

        #region public fields
        public SceneType Type => _type;
        public AssetReference Scene => _scene;
        #endregion
    }
}