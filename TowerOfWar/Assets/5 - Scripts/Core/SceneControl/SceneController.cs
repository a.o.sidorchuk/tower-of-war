﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data.Models.Player;
using Core.Utilities;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AddressableAssets;

namespace Core.SceneControl
{
    [Serializable]
    public class FractionMenuItem
    {
        public FractionType FractionType;
        public SceneInfo SceneInfo;
    }

    /// <summary>
    /// Управление загрузкой сцен
    /// </summary>
    public class SceneController : MonoSingleton<SceneController>
    {
        [Tooltip("Основные сцены с логикой")]
        [SerializeField] List<SceneInfo> _scenesWithLogic = new List<SceneInfo>();

        [Tooltip("Сцены меню для определенной фракции")]
        [SerializeField] List<FractionMenuItem> _fractionMenuScenes = new List<FractionMenuItem>();

        public void LoadMenuScene(FractionType fractionType, FadeType fadeType)
        {
            var fractionMenuScene = _fractionMenuScenes.FirstOrDefault(x => x.FractionType == fractionType);
            var menuScene = Instance._scenesWithLogic.FirstOrDefault(x => x.Type == SceneType.Menu);

            Instance.LoadScene(menuScene, fractionMenuScene?.SceneInfo, fadeType);
        }

        public void LoadGameScene(SceneInfo sceneContent, FadeType fadeType)
        {
            var gameLogicScene = Instance._scenesWithLogic.FirstOrDefault(x => x.Type == SceneType.Game);

            Instance.LoadScene(gameLogicScene, sceneContent, fadeType);
        }

        public void LoadScene(SceneInfo sceneLogic = null, SceneInfo sceneContent = null, FadeType fadeType = FadeType.Default) => UniTask.ToCoroutine(async () =>
        {
            await ScreenFader.FadeSceneOut(fadeType);

            if (sceneContent != null && !string.IsNullOrEmpty(sceneContent.Scene.RuntimeKey.ToString()))
            {
                await AddressableExtensions.LoadSceneAsync(sceneContent.Scene.RuntimeKey.ToString());

                if (!string.IsNullOrEmpty(sceneLogic.Scene.RuntimeKey.ToString()))
                {
                    await AddressableExtensions.LoadSceneAsync(sceneLogic.Scene.RuntimeKey.ToString(), LoadSceneMode.Additive);

                    MessageBroker.Default.Publish(GameMessage.Create(GameMessage.Type.LoadLevelComplete, null, null));
                }
            }
            else
            {
                await AddressableExtensions.LoadSceneAsync(sceneLogic.Scene.RuntimeKey.ToString());
            }

            await ScreenFader.FadeSceneIn();
        });
    }
}