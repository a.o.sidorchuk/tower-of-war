﻿using UniRx.Async;
using UnityEngine;

namespace Core.SceneControl
{
    /// <summary>
    /// Компонент для реализации окна "затемнения" и других загрузочных окон
    /// </summary>
    public class CanvasFader : MonoBehaviour
    {
        [SerializeField] float _fadeDuration = 1f;
        [SerializeField] CanvasGroup _canvasGroup;

        public CanvasGroup CanvasGroup => _canvasGroup;

        public async UniTask FadeProcess(float finalAlpha)
        {
            await FadeProcess(finalAlpha, _fadeDuration);
        }

        public async UniTask FadeProcess(float finalAlpha, float fadeDuration)
        {
            var isVisible = finalAlpha != 0;

            if (isVisible)
            {
                _canvasGroup.blocksRaycasts = true;
            }
            else
            {
                _canvasGroup.interactable = false;
            }

            var fadeSpeed = Mathf.Abs(_canvasGroup.alpha - finalAlpha) / fadeDuration;

            while (!Mathf.Approximately(_canvasGroup.alpha, finalAlpha))
            {
                _canvasGroup.alpha = Mathf.MoveTowards(_canvasGroup.alpha, finalAlpha, fadeSpeed * Time.deltaTime);

                await UniTask.Yield();
            }

            _canvasGroup.alpha = finalAlpha;

            if (isVisible)
            {
                _canvasGroup.interactable = true;
            }
            else
            {
                _canvasGroup.blocksRaycasts = false;
            }
        }
    }
}