﻿using UnityEngine;
using UnityEngine.UI;

namespace Core.SceneControl
{
    /// <summary>
    /// Компонент окна загрузки
    /// </summary>
    public class LoadingScreen : MonoBehaviour
    {
        [SerializeField] Image _backgroundImage;
        [SerializeField] Sprite _dafaultBackgroundSprite;

        [SerializeField] Text _loadingText;
        [SerializeField] Image _loadingIndicatorImage;

        private void Awake() => SetBackgroundImage(_dafaultBackgroundSprite);

        public void SetBackgroundImage(Sprite sprite)
        {
            _backgroundImage.sprite = sprite;
            _backgroundImage.rectTransform.sizeDelta = new Vector2(_backgroundImage.sprite.texture.width, _backgroundImage.sprite.texture.height);
        }

        public void SetText(string text) => _loadingText.text = text;

        public void SetIndicatorActive(bool state) => _loadingIndicatorImage.gameObject.SetActive(state);

        private void OnDisable()
        {
            SetBackgroundImage(_dafaultBackgroundSprite);
        }
    }
}