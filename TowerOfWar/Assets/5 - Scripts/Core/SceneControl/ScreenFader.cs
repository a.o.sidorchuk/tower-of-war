﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Utilities;
using UniRx.Async;
using UnityEngine;

namespace Core.SceneControl
{
    public enum FadeType
    {
        Default,
        Loading
    }

    [Serializable]
    public class ScreenFaderParam
    {
        public FadeType fadeType;
        public CanvasFader canvasFader;
    }

    /// <summary>
    /// Основной узел управления загрузочными окнами
    /// </summary>
    public class ScreenFader : MonoSingleton<ScreenFader>
    {
        [SerializeField] FadeType _currentType = FadeType.Default;
        [SerializeField] List<ScreenFaderParam> _screenFaders = new List<ScreenFaderParam>();

        public bool IsFading { get; private set; }

        public static async UniTask FadeSceneIn()
        {
            var canvasGroup = Instance.GetCurrentScreenFader();

            if (!canvasGroup) return;

            await Instance.Fade(0f, canvasGroup);

            canvasGroup.gameObject.SetActive(false);
        }

        public static async UniTask FadeSceneOut(FadeType fadeType = FadeType.Default)
        {
            Instance._currentType = fadeType;

            var canvasGroup = Instance.GetCurrentScreenFader();

            if (!canvasGroup) return;

            canvasGroup.gameObject.SetActive(true);

            await Instance.Fade(1f, canvasGroup);
        }


        private async UniTask Fade(float finalAlpha, CanvasFader canvasFader)
        {
            IsFading = true;

            canvasFader.CanvasGroup.blocksRaycasts = true;

            await canvasFader.FadeProcess(finalAlpha);

            canvasFader.CanvasGroup.blocksRaycasts = finalAlpha == 0 ? false : true;

            IsFading = false;
        }

        private CanvasFader GetCurrentScreenFader() => GetScreenFaderByType(_currentType);
        public CanvasFader GetScreenFaderByType(FadeType type) => _screenFaders.FirstOrDefault(x => x.fadeType == type).canvasFader;
    }
}
