﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Core.Health
{
	[Serializable]
	public class HealthChangeEvent : UnityEvent<HealthChangeInfo> { }

	[Serializable]
	public class HitEvent : UnityEvent<HitInfo> { }

	public class DamageableListener : MonoBehaviour
	{
		public HealthChangeEvent damaged, healed, healthChanged;
		public UnityEvent died, reachedMaxHealth;
		public HitEvent hit;

		[SerializeField] DamageableBehaviour damageableBehaviour;

		protected virtual void Awake()
		{
			if (damageableBehaviour != null) return;
			damageableBehaviour = GetComponent<DamageableBehaviour>();
		}

		protected virtual void OnEnable()
		{
			damageableBehaviour.Configuration.died += OnDeath;
			damageableBehaviour.Configuration.reachedMaxHealth += OnReachedMaxHealth;
			damageableBehaviour.Configuration.healed += OnHealed;
			damageableBehaviour.Configuration.damaged += OnDamaged;
			damageableBehaviour.Configuration.healthChanged += OnHealthChanged;
			damageableBehaviour.hit += OnHit;
		}

		protected virtual void OnDisable()
		{
			damageableBehaviour.Configuration.died -= OnDeath;
			damageableBehaviour.Configuration.reachedMaxHealth -= OnReachedMaxHealth;
			damageableBehaviour.Configuration.healed -= OnHealed;
			damageableBehaviour.Configuration.damaged -= OnDamaged;
			damageableBehaviour.Configuration.healthChanged -= OnHealthChanged;
			damageableBehaviour.hit -= OnHit;
		}

		protected virtual void OnDeath(HealthChangeInfo info) => died.Invoke();

		protected virtual void OnReachedMaxHealth() => reachedMaxHealth.Invoke();

		protected virtual void OnHealed(HealthChangeInfo info) => healed.Invoke(info);

		protected virtual void OnDamaged(HealthChangeInfo info) => damaged.Invoke(info);

		protected virtual void OnHealthChanged(HealthChangeInfo info) => healthChanged.Invoke(info);

		protected virtual void OnHit(HitInfo info) => hit.Invoke(info);
	}
}