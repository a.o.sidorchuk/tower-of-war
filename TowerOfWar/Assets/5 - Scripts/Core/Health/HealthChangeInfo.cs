﻿using UnityEngine;

namespace Core.Health
{
	public struct HealthChangeInfo
	{
		public float oldHealth;
		public float newHealth;
		public Damageable damageable;
		public int squadId;

		public float HealthDifference => newHealth - oldHealth;
		public float AbsHealthDifference => Mathf.Abs(HealthDifference);
	}
}