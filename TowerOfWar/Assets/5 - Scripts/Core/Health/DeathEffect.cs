﻿using Core.Utilities;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Health
{
    /// <summary> Запуск визуальных эффектов для смерти </summary>
    public class DeathEffect : MonoBehaviour, IPoolHelper
    {
        [SerializeField] DamageableBehaviour _damageableBehaviour;
        [Space]
        [SerializeField] ParticleSystem _deathParticleSystemPrefab;
        [SerializeField] Vector3 _deathEffectOffset;

        protected Damageable _damageable;

        protected virtual void Awake()
        {
            if (_damageableBehaviour != null) AssignDamageable(_damageableBehaviour.Configuration);
        }

        public void AssignDamageable(Damageable damageable)
        {
            if (_damageable != null) _damageable.died -= OnDied;

            _damageable = damageable;
            _damageable.died += OnDied;
        }

        void OnDied(HealthChangeInfo healthChangeInfo)
        {
            if (_deathParticleSystemPrefab == null) return; 

            var pfx = Poolable.TryGetPoolable<ParticleSystem>(_deathParticleSystemPrefab.gameObject);

            pfx.transform.position = transform.position + _deathEffectOffset;
            pfx.Play();
        }

        public List<Poolable> GetPoolables()
        {
            var result = new List<Poolable>();
            if (_deathParticleSystemPrefab) result.Add(_deathParticleSystemPrefab.GetComponent<Poolable>());
            return result;
        }
    }
}
