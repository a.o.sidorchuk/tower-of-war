﻿using Photon.Pun;
using UnityEngine;

namespace Core.Health
{
    /// <summary> Компонент визуализации "полоски" здоровья </summary>
    public class HealthVisualizer : MonoBehaviour
    {
        [SerializeField] protected DamageableBehaviour _damageableBehaviour;

        [SerializeField] protected Transform _healthBar;
        [SerializeField] protected Transform _backgroundBar;

        [SerializeField] protected bool _showWhenFull;

        protected Transform _cameraToFace;
        protected Damageable _damageable;

        PhotonView _photonView;

        protected virtual void Awake()
        {
            _photonView = GetComponent<PhotonView>();
            if (_damageableBehaviour != null) AssignDamageable(_damageableBehaviour.Configuration);
            SetVisible(_showWhenFull);
        }

        protected virtual void Start()
        {
            _cameraToFace = Camera.main.transform;
        }

        protected virtual void Update()
        {
            transform.forward = -_cameraToFace.transform.forward;
        }


        public void AssignDamageable(Damageable damageable)
        {
            if (_damageable != null) _damageable.healthChanged -= OnHealthChanged;

            _damageable = damageable;
            _damageable.healthChanged += OnHealthChanged;
        }

        void OnHealthChanged(HealthChangeInfo healthChangeInfo) => _photonView.RPC("RpcUpdateVisibleUnitHealth", RpcTarget.All, _damageable.NormalizedHealth);
        
        [PunRPC]
        void RpcUpdateVisibleUnitHealth(float normalizedHealth)
        {
            var scale = Vector3.one;

            if (_healthBar != null)
            {
                scale.x = normalizedHealth;
                _healthBar.transform.localScale = scale;
            }

            if (_backgroundBar != null)
            {
                scale.x = 1 - normalizedHealth;
                _backgroundBar.transform.localScale = scale;
            }

            SetVisible((_showWhenFull || normalizedHealth < 1.0f) && (normalizedHealth != 0));
        }

        public void SetVisible(bool visible) => gameObject.SetActive(visible);
    }
}