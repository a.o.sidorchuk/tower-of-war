﻿using System;
using UnityEngine;

namespace Core.Health
{
    /// <summary> Конфигурация повреждаемого объекта </summary>
    public class Damageable
    {
        public event Action reachedMaxHealth;
        public event Action<HealthChangeInfo> damaged, healed, died, healthChanged;

        public int SquadId { get; private set; }

        public float CurrentHealth { get; protected set; }

        public float NormalizedHealth
        {
            get
            {
                if (Math.Abs(_maxHealth) <= Mathf.Epsilon)
                {
                    Debug.LogError("Max Health is 0");
                    _maxHealth = 1f;
                }
                return CurrentHealth / _maxHealth;
            }
        }

        public bool IsDead => CurrentHealth <= 0f;
        public bool IsAtMaxHealth => Mathf.Approximately(CurrentHealth, _maxHealth);

        float _maxHealth = 1;

        float _damageScale = 1;
        float _healScale = 1;

        public virtual void Init(int squadId) => SquadId = squadId;

        public void SetDamageScale(float scale) => _damageScale = scale;
        public void SetHealScale(float scale) => _healScale = scale;

        public void SetMaxHealth(int maxHealth)
        {
            _maxHealth = maxHealth;
            SetHealth(maxHealth);
        }

        public void SetHealth(float health)
        {
            CurrentHealth = health;

            healthChanged?.Invoke(new HealthChangeInfo
            {
                damageable = this,
                newHealth = health,
                oldHealth = CurrentHealth
            });
        }

        public bool CanDamage(int squadId) => squadId != SquadId;
        public bool CanHealth(int squadId) => squadId == SquadId;

        /// <summary> Получение урона </summary>
        public bool TakeDamage(float damageValue, int squadId, out HealthChangeInfo output)
        {
            output = new HealthChangeInfo
            {
                squadId = squadId,
                damageable = this,
                newHealth = CurrentHealth,
                oldHealth = CurrentHealth
            };

            damageValue = CalcImpactValue(damageValue, _damageScale);

            if (IsDead || !CanDamage(squadId)) return false;

            ChangeHealth(-damageValue, squadId, out output);

            damaged?.Invoke(output);

            if (IsDead) died?.Invoke(output);

            return true;
        }

        /// <summary> Получение здоровья </summary>
        public bool IncreaseHealth(float healValue, int squadId, out HealthChangeInfo output)
        {
            healValue = CalcImpactValue(healValue, _healScale);

            output = new HealthChangeInfo
            {
                squadId = squadId,
                damageable = this,
                newHealth = CurrentHealth,
                oldHealth = CurrentHealth
            };

            if (IsDead || !CanHealth(squadId)) return false;

            ChangeHealth(healValue, squadId, out output);

            healed?.Invoke(output);

            if (IsAtMaxHealth) reachedMaxHealth?.Invoke();

            return true;
        }

        private float CalcImpactValue(float value, float scale)
        {
            value = UnityEngine.Random.Range(value * 0.9f, value * 1.1f);
            return value * scale;
        }

        protected void ChangeHealth(float healthIncrement, int squadId, out HealthChangeInfo info)
        {
            info = new HealthChangeInfo
            {
                damageable = this,
                squadId = squadId
            };

            info.oldHealth = CurrentHealth;

            CurrentHealth = Mathf.Clamp(CurrentHealth + healthIncrement, 0f, _maxHealth);

            info.newHealth = CurrentHealth;

            healthChanged?.Invoke(info);
        }
    }
}