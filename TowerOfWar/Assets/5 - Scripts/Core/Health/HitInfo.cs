﻿using UnityEngine;

namespace Core.Health
{
	/// <summary> Информаци о повреждении </summary>
	public struct HitInfo
	{
		readonly HealthChangeInfo _healthChangeInfo;
		readonly Vector3 _damagePoint;

		public HealthChangeInfo HealthChangeInfo => _healthChangeInfo;
		public Vector3 DamagePoint => _damagePoint;

		public HitInfo(HealthChangeInfo info, Vector3 damageLocation)
		{
			_damagePoint = damageLocation;
			_healthChangeInfo = info;
		}
	}
}