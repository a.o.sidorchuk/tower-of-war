﻿using System;
using UnityEngine;

namespace Core.Health
{
	/// <summary> Базовое поведение объекта имеющего здоровье </summary>
	/// <remarks> Данное поведение обрабатывает возмодность получения как урона, так и здоровья </remarks>
	public abstract class DamageableBehaviour : MonoBehaviour
	{
		public event Action<HitInfo> hit;
		public event Action<DamageableBehaviour> removed, died;

		public bool IsDead => Configuration.IsDead;
		public virtual Vector3 Position => transform.position;

		public Damageable Configuration { get; } = new Damageable();

		public virtual void Init(int squadId)
		{
			Configuration.Init(squadId);
			Configuration.died += OnConfigurationDied;
		}

		private void OnDisable()
		{
			Configuration.died -= OnConfigurationDied;
		}

		public virtual bool TakeDamage(float damageValue, Vector3 damagePoint, int squadId)
		{
			var result = Configuration.TakeDamage(damageValue, squadId, out HealthChangeInfo info);

			hit?.Invoke(new HitInfo(info, damagePoint));

			return result;
		}

		public virtual bool IncreaseHealth(float healValue, Vector3 damagePoint, int squadId)
		{
		 	var result = Configuration.IncreaseHealth(healValue, squadId, out HealthChangeInfo info);

			hit?.Invoke(new HitInfo(info, damagePoint));

			return result;
		}

		protected virtual void Kill()
		{
			Configuration.TakeDamage(Configuration.CurrentHealth, 0, out HealthChangeInfo healthChangeInfo);
		}

		public virtual void Remove()
		{
			Configuration.SetHealth(0);
			OnRemoved();
		}

		void OnDeath() => died?.Invoke(this);

		void OnRemoved() => removed?.Invoke(this);

		void OnConfigurationDied(HealthChangeInfo changeInfo)
		{
			OnDeath();
			Remove();
		}
	}
}