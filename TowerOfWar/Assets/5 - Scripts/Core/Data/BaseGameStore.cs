﻿using Newtonsoft.Json;

namespace Core.Data
{
    /// <summary> Данные о настройках приложения </summary>
    public class AppOptions
    {
        public float MasterVolume { get; private set; } = 1;
        public float SfxVolume { get; private set; } = 1;
        public float MusicVolume { get; private set; } = 1;

        public void SetValues(float master, float sfx, float music)
        {
            MasterVolume = master;
            SfxVolume = sfx;
            MusicVolume = music;
        }
    }

    /// <summary> Базовый класс для реализации хранилища данных </summary>
    public abstract class BaseGameStore
    {
        [JsonProperty] public AppOptions Options { get; private set; } = new AppOptions();
    }
}