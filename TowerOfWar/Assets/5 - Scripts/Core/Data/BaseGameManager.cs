﻿using Core.Utilities;
using System.Collections;
using UniRx.Async;
using UnityEngine;
using UnityEngine.Audio;

namespace Core.Data
{
    /// <summary>
    /// Базовая реализация игрового менеджера
    /// </summary>
    public abstract class BaseGameManager<TGManager, TStore> : MonoSingleton<TGManager>
        where TGManager : BaseGameManager<TGManager, TStore>
        where TStore : BaseGameStore, new()
    {
        protected const string _userDataFileName = "user.data";
        protected string _userDataFullName;

        protected TStore _store;

        bool _initialized = false;

        #region public fields
        public TStore Store => _store;
        public bool Initialized => _initialized;
        #endregion

        #region audio params
        [Header("Sound Params")]
        public AudioMixer gameMixer;
        public string masterVolumeParameter = "MasterVolume";
        public string sfxVolumeParameter = "SFXVolume";
        public string musicVolumeParameter = "MusicVolume";
        #endregion

        protected override void Awake()
        {
            _userDataFullName = FileSaver.GetFullFileName(_userDataFileName);
            base.Awake();
        }

        IEnumerator Start() => UniTask.ToCoroutine(async () =>
        {
            await LoadData();

            SetVolumes(_store.Options.MasterVolume, _store.Options.SfxVolume, _store.Options.MusicVolume, false);
        });

        protected virtual async UniTask LoadData()
        {
            var userDataFullName = _userDataFullName;

            if (!FileSaver.FileExistCheck(userDataFullName))
            {
                _store = new TStore();

                await SaveData();
            }
            else
            {
                _store = await FileSaver.LoadFromJson<TStore>(userDataFullName);
            }

            _initialized = true;
        }

        public virtual async UniTask SaveData()
        {
            var userDataFullName = _userDataFullName;
            await FileSaver.SaveToJson(userDataFullName, _store);
        }


        #region настройка звука
        public virtual void GetVolumes(out float master, out float sfx, out float music)
        {
            master = _store.Options.MasterVolume;
            sfx = _store.Options.SfxVolume;
            music = _store.Options.MusicVolume;
        }

        public virtual void SetVolumes(float master, float sfx, float music, bool save)
        {
            if (gameMixer == null) return;

            if (!string.IsNullOrEmpty(masterVolumeParameter))
            {
                gameMixer.SetFloat(masterVolumeParameter, LogarithmicDbTransform(Mathf.Clamp01(master)));
            }
            if (!string.IsNullOrEmpty(sfxVolumeParameter))
            {
                gameMixer.SetFloat(sfxVolumeParameter, LogarithmicDbTransform(Mathf.Clamp01(sfx)));
            }
            if (!string.IsNullOrEmpty(musicVolumeParameter))
            {
                gameMixer.SetFloat(musicVolumeParameter, LogarithmicDbTransform(Mathf.Clamp01(music)));
            }

            if (save)
            {
                _store.Options.SetValues(master, sfx, music);

                UniTask.ToCoroutine(SaveData);
            }
        }

        protected static float LogarithmicDbTransform(float volume)
        {
            volume = Mathf.Log(89 * volume + 1) / Mathf.Log(90) * 80;
            return volume - 80;
        }
        #endregion
    }
}