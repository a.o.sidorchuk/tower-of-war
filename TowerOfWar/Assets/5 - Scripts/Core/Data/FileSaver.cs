﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using UniRx.Async;
using UnityEngine;

namespace Core.Data
{
    /// <summary> Управление локальными файлами </summary>
    public static class FileSaver
    {
        public static string GetFullFileName(string fileName) => $"{Application.persistentDataPath}/{fileName}";

        public static async UniTask SaveToJson<T>(string fileName, T data)
        {
            try
            {
                Debug.Log($"SaveToJson: {fileName}");

                var json = JsonConvert.SerializeObject(data);
                var bData = Encoding.Default.GetBytes(json);

                await Save(fileName, bData);
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
            }
        }

        public static async UniTask<T> LoadFromJson<T>(string fileName)
        {
            try
            {
                var bData = await Load(fileName);

                if (bData == null || bData.Length == 0) return default;

                var json = Encoding.Default.GetString(bData);

                return JsonConvert.DeserializeObject<T>(json);
            }
            catch (Exception ex)
            {
                Debug.Log(ex);

                return default;
            }
        }


        public static async UniTask Save(string fileName, byte[] data)
        {
            using (var sw = new FileStream(fileName, FileMode.Create))
            {
                await sw.WriteAsync(data, 0, data.Length);
            }
        }

        public static async UniTask<byte[]> Load(string fileName)
        {
            using (var sw = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                var data = new byte[sw.Length];

                await sw.ReadAsync(data, 0, data.Length);

                return data;
            }
        }


        public static void RemoveFile(string filePath)
        {
            if (FileExistCheck(filePath)) File.Delete(filePath);
        }


        public static bool FileExistCheck(string fileName) => new FileInfo(fileName).Exists;
    }
}