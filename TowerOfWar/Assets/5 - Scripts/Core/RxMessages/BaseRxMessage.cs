﻿namespace UniRx
{
    /// <summary>
    /// Базовая реализация сообщения для системы сообщейни UniRx
    /// </summary>
    public abstract class BaseRxMessage<T, TMessageType, TSender, TData> where T : BaseRxMessage<T, TMessageType, TSender, TData>, new()
    {
        public TMessageType type { get; protected set; }
        public TSender sender { get; protected set; }
        public TData data { get; protected set; }

        public static T Create(TMessageType type, TData data, TSender sender)
        {
            return new T { type = type, sender = sender, data = data };
        }
    }
}
