﻿using UnityEngine;

namespace UniRx
{
    /// <summary>
    /// Общие события меню и игрового интерфеса
    /// </summary>
    public class MenuMessage : BaseRxMessage<MenuMessage, MenuMessage.Type, MonoBehaviour, object>
    {
        public enum Type
        {
            OpenFractionSelectPanel,
            OpenOptionsPanel,
            OpenMapPanel,
            OpenQuestsPanel,
            OpenSquadPanel,
            OpenArmyPanel,
            OpenArenaPanel,
            OpenUnitInfoPanel,
            SquadSaveEvent,
            RecruitUnitEvent,
            OpenEndGamePanel
        }
    }
}