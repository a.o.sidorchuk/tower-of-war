﻿using UnityEngine;

namespace UniRx
{
    /// <summary>
    /// События игровой сессии
    /// </summary>
    public class GameMessage : BaseRxMessage<GameMessage, GameMessage.Type, MonoBehaviour, object>
    {
        public enum Type
        {
            LevelStateChange,   // _TODO отработать все сообщения изменения состояния комнаты на UI
            PlayerStateChange,
            OpenGameMenuEvent,
            LoadLevelComplete,
            TowerCreated
        }
    }
}