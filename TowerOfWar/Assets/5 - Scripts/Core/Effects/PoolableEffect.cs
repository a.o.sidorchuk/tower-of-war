﻿using System.Collections.Generic;
using Core.Utilities;
using UnityEngine;

namespace Core.Effects
{
	/// <summary> Компонент эффектов сохраняемых в пуле объектов, для повторного использования </summary>
	public class PoolableEffect : Poolable
	{
		protected List<ParticleSystem> _systems;
		protected List<TrailRenderer> _trails;

		bool _effectsEnabled;

		protected virtual void Awake()
		{
			_effectsEnabled = true;
			
			_systems = new List<ParticleSystem>();
			_trails = new List<TrailRenderer>();

			foreach (var system in GetComponentsInChildren<ParticleSystem>())
			{
				if (system.emission.enabled && system.gameObject.activeSelf)
				{
					_systems.Add(system);
				}
			}
			
			foreach (var trail in GetComponentsInChildren<TrailRenderer>())
			{
				if (trail.enabled && trail.gameObject.activeSelf)
				{
					_trails.Add(trail);
				}
			}

			TurnOffAllSystems();
		}

		public void StopAll() => _systems.ForEach(x => x.Stop());

		public void TurnOffAllSystems()
		{
			if (!_effectsEnabled) return;

			foreach (var particleSystem in _systems)
			{
				particleSystem.Clear();
				var emission = particleSystem.emission;
				emission.enabled = false;
			}

			foreach (var trailRenderer in _trails)
			{
				trailRenderer.Clear();
				trailRenderer.enabled = false;
			}

			_effectsEnabled = false;
		}

		public void TurnOnAllSystems()
		{
			if (_effectsEnabled) return;

			foreach (var particleSystem in _systems)
			{
				particleSystem.Clear();
				var emission = particleSystem.emission;
				emission.enabled = true;
			}

			foreach (var trailRenderer in _trails)
			{
				trailRenderer.Clear();
				trailRenderer.enabled = true;
			}

			_effectsEnabled = true;
		}

		protected override void Repool()
		{
			base.Repool();
			TurnOffAllSystems();
		}
	}
}