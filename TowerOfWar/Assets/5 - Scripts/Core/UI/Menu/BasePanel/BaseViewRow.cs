﻿using System.Collections.Generic;
using UniRx.Async;
using UnityEngine;

namespace Game.UI.Menu
{
    public interface IViewRow<TView, TData>
    {
        List<TView> ViewItems { get; }
        UniTask Init(List<TData> rowItems);
        UniTask<TView> AddItem(TData newItem);
    }

    public abstract class BaseViewRow<TViewItem, TData> : MonoBehaviour, IViewRow<TViewItem, TData>
        where TViewItem : IViewItem<TData>
    {
        [SerializeField] GameObject _itemViewPrefab;

        public List<TViewItem> ViewItems { get; protected set; } = new List<TViewItem>();

        public virtual async UniTask Init(List<TData> rowItems)
        {
            foreach (var unit in rowItems)
            {
                await AddItem(unit);
            }
        }

        public virtual async UniTask<TViewItem> AddItem(TData newItem)
        {
            var unitViewGO = Instantiate(_itemViewPrefab, transform);

            var unitView = unitViewGO.GetComponent<TViewItem>();

            await unitView.Init(newItem);

            ViewItems.Add(unitView);

            return unitView;
        }
    }
}