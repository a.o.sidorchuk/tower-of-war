﻿using System;
using UniRx;
using UniRx.Async;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI.Menu
{
    /// <summary>
    /// Базовая реализация закрываемой панели
    /// </summary>
    public abstract class ClosingPanel : BasePanel
    {
        [Space]
        [SerializeField] MenuMessage.Type _openMsgType;
        [SerializeField] Button _closeButton;

        IDisposable _menuMsgReceive;

        protected object _menuMsgData;

        protected override void Awake()
        {
            base.Awake();

            if (_closeButton)
            {
                _closeButton.gameObject.SetActive(true);
                _closeButton.onClick.AddListener(() => UniTask.ToCoroutine(ClosePanelEvent));
            }

            _menuMsgReceive = MessageBroker.Default.Receive<MenuMessage>()
                .Where(x => x.type == _openMsgType)
                .Subscribe(x => {
                    _menuMsgData = x.data;
                    UniTask.ToCoroutine(OpenPanelEvent);
                });
        }

        protected virtual async UniTask OpenPanelEvent() => await _canvasFader.FadeProcess(1);

        protected virtual async UniTask ClosePanelEvent() => await _canvasFader.FadeProcess(0);

        protected virtual void OnDestroy() => _menuMsgReceive?.Dispose();
    }
}