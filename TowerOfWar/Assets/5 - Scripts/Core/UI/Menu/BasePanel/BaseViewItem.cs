﻿using UniRx.Async;
using UnityEngine;

namespace Game.UI.Menu
{
    public interface IViewItem<TData> { UniTask Init(TData item); }

    public abstract class BaseViewItem<TData> : MonoBehaviour, IViewItem<TData>
    {
        public TData ViewData { get; protected set; }
        public virtual async UniTask Init(TData item) => ViewData = item;
    }
}