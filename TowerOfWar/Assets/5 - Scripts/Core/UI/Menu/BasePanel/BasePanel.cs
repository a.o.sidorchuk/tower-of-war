﻿using Core.SceneControl;
using UnityEngine;

namespace Game.UI.Menu
{
    /// <summary>
    /// Базовый компонент для реализации UI панелей
    /// </summary>
    [RequireComponent(typeof(CanvasFader))]
    public abstract class BasePanel : MonoBehaviour
    {
        protected CanvasFader _canvasFader;

        protected virtual void Awake()
        {
            _canvasFader = GetComponent<CanvasFader>();
        }
    }
}