﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx.Async;
using UnityEngine;

namespace Game.UI.Menu
{
    public abstract class ItemRowPanel<TViewRow, TViewItem, TViewData> : ClosingPanel
        where TViewRow : IViewRow<TViewItem, TViewData>
        where TViewItem : IViewItem<TViewData>
    {
        [Space]
        [SerializeField] RectTransform _rowParentTransform;
        [SerializeField] GameObject _rowPrefab;
        [Space]
        [SerializeField] int _itemInRow = 6;

        bool _initialized = false;

        List<TViewRow> _rows = new List<TViewRow>();

        public List<TViewItem> ViewItems { get; private set; } = new List<TViewItem>();

        IEnumerator Start() => UniTask.ToCoroutine(InitProcess);

        protected abstract List<TViewData> LoadData();

        protected virtual async UniTask InitProcess()
        {
            if (!_initialized)
            {
                var dataList = LoadData();

                var count = dataList.Count;
                var rowCount = Mathf.CeilToInt(count * 1f / _itemInRow);

                for (var i = 0; i < rowCount; i++)
                {
                    var rowItems = dataList.Skip(i * _itemInRow).Take(_itemInRow).ToList();
                    var rowGO = Instantiate(_rowPrefab, _rowParentTransform);

                    var row = rowGO.GetComponent<TViewRow>();

                    await row.Init(rowItems);

                    _rows.Add(row);
                }

                ViewItems.AddRange(_rows.SelectMany(x => x.ViewItems));

                _initialized = true;
            }
        }

        protected virtual async UniTask<TViewItem> AddNewItem(TViewData newItem)
        {
            var lastRow = _rows.Last();
            
            if (lastRow.ViewItems.Count < _itemInRow)
            {
                return await lastRow.AddItem(newItem);
            }
            else
            {
                var rowGO = Instantiate(_rowPrefab, _rowParentTransform);
                var row = rowGO.GetComponent<TViewRow>();

                await row.Init(new List<TViewData> { newItem });

                _rows.Add(row);

                return row.ViewItems.Last();
            }
        }
    }
}