﻿using Lean.Touch;
using System;
using Core.Utilities;
using UnityEngine;

namespace Core.Input
{
    public class PlayerInput : MonoSingleton<PlayerInput>
    {
        /// <summary> tap (мировая координата) </summary>
        public event Action<Vector3> tapEvent;

        /// <summary> finger down </summary>
        public event Action fingerDownEvent;
        /// <summary> finger up </summary>
        public event Action fingerUpEvent;

        /// <summary> move (мировое смещение, смещение на экране, мировая координата) </summary>
        public event Action<Vector3, Vector2, Vector3> moveEvent;
        /// <summary> swipe (смещение на экране, мировая координата) </summary>
        public event Action<Vector2, Vector3> swipeEvent;

        /// <summary> zoom (смещение с последнего кадра) </summary>
        public event Action<float> zoomEvent;

        private void TapEvent(LeanFinger finger)
        {
            tapEvent?.Invoke(finger.GetLastWorldPosition(0));
        }

        private void FingerSetEvent(LeanFinger finger)
        {
            if (LeanTouch.Fingers.Count == 1 && finger.Old)
                moveEvent?.Invoke(finger.GetWorldDelta(0), finger.ScreenDelta, finger.GetWorldPosition(0));
        }

        private void ZoomEvent(LeanFinger finger)
        {
            if (LeanTouch.Fingers.Count == 2)
                zoomEvent?.Invoke(1 - LeanGesture.GetPinchScale());
        }

        private void SwipeEvent(LeanFinger finger)
        {
            swipeEvent?.Invoke(finger.SwipeScreenDelta.normalized, finger.GetWorldPosition(0));
        }

        private void FingerUpEvent(LeanFinger finger)
        {
            fingerUpEvent?.Invoke();
        }

        private void FingerDownEvent(LeanFinger finger)
        {
            fingerDownEvent?.Invoke();
        }

        void OnEnable()
        {
            LeanTouch.OnFingerUp += FingerUpEvent;
            LeanTouch.OnFingerDown += FingerDownEvent;
            LeanTouch.OnFingerTap += TapEvent;
            LeanTouch.OnFingerSwipe += SwipeEvent;
            LeanTouch.OnFingerSet += FingerSetEvent;
            LeanTouch.OnFingerSet += ZoomEvent;
        }

        void OnDisable()
        {
            LeanTouch.OnFingerUp -= FingerUpEvent;
            LeanTouch.OnFingerDown -= FingerDownEvent;
            LeanTouch.OnFingerTap -= TapEvent;
            LeanTouch.OnFingerSwipe -= SwipeEvent;
            LeanTouch.OnFingerSet -= FingerSetEvent;
            LeanTouch.OnFingerSet -= ZoomEvent;
        }
    }
}