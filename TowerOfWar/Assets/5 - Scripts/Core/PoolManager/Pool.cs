using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Core.Utilities
{
    /// <summary> ������� ���������� ���� ��������� </summary>
    public class Pool<T>
    {
        /// <summary> ��������� ������� </summary>
        protected Func<T> _factory;
        /// <summary> ������� ������ ��������� �������� </summary>
        protected readonly Action<T> _reset;

        /// <summary> ������ ��������� ��������� </summary>
        protected readonly List<T> _available;
        /// <summary> ������ ���� ����������� ��������� </summary>
        protected readonly List<T> _all;

        /// <summary>
        /// �������� ������ ���� � �������� ����������� ���������
        /// </summary>
        /// <param name="factory">������� ������� ��������</param>
        /// <param name="reset">������� ������ �������� ��� ����������� � ���</param>
        /// <param name="initialCapacity">��������� ���������� ��������� � ����</param>
        public Pool(Func<T> factory, Action<T> reset, int initialCapacity)
        {
            if (factory == null) throw new ArgumentNullException("factory == null");

            _available = new List<T>();
            _all = new List<T>();
            _factory = factory;
            _reset = reset;

            if (initialCapacity > 0) Grow(initialCapacity);
        }

        /// <summary>
        /// �������� ������ ����
        /// </summary>
        /// <param name="factory">������� �������� ��������</param>
        public Pool(Func<T> factory) : this(factory, null, 0) { }

        /// <summary>
        /// �������� ������ ���� � �������� ����������� ���������
        /// </summary>
        /// <param name="factory">������� ������� ��������</param>
        /// <param name="initialCapacity">��������� ���������� ��������� � ����</param>
        public Pool(Func<T> factory, int initialCapacity) : this(factory, null, initialCapacity) { }

        /// <summary> ��������� �������� �� ����, � ������� ������ </summary>
        public virtual T Get() => Get(_reset);

        /// <summary>
        /// ��������� �������� �� ����. ��� ������������� ����������� ������ ���� � ���������� �������
        /// </summary>
        /// <param name="resetOverride">��������������� ������� ������</param>
        public virtual T Get(Action<T> resetOverride)
        {
            if (_available.Count == 0) Grow(1);

            if (_available.Count == 0) throw new InvalidOperationException("�� ������� ������� ���");

            var itemIndex = _available.Count - 1;
            var item = _available[itemIndex];

            _available.RemoveAt(itemIndex);

            resetOverride?.Invoke(item);

            return item;
        }

        /// <summary> �������� ������� �������� � ���� </summary>
        public virtual bool Contains(T pooledItem) => _all.Contains(pooledItem);

        /// <summary> ������� ������� � ��� </summary>
        public virtual void Return(T pooledItem)
        {
            if (_all.Contains(pooledItem) && !_available.Contains(pooledItem))
            {
                ReturnToPoolInternal(pooledItem);
            }
            else
            {
                throw new InvalidOperationException($"������� ������� ������, ������� �� ���������� ����� ���: {pooledItem}, {this}");
            }
        }

        /// <summary> ������� ��� �������� � ��� </summary>
        public virtual void ReturnAll() => ReturnAll(null);

        /// <summary> ������� ��� �������� � ��� � ������� �������� ��� ������� summary>
        public virtual void ReturnAll(Action<T> preReturn)
        {
            foreach (var item in _all)
            {
                if (!_available.Contains(item))
                {
                    preReturn?.Invoke(item);
                    ReturnToPoolInternal(item);
                }
            }
        }

        /// <summary> �������� ������� � ������ ��������� ��������� </summary>
        protected virtual void ReturnToPoolInternal(T element) => _available.Add(element);

        /// <summary> ��������� ��� �� ������� ���������� ��������� </summary>
        public void Grow(int amount)
        {
            for (var i = 0; i < amount; ++i) AddNewElement();
        }

        /// <summary> ������� ����� ������� </summary>
        protected virtual T AddNewElement()
        {
            var newElement = _factory();

            _all.Add(newElement);
            _available.Add(newElement);

            return newElement;
        }

        /// <summary> ������� ������� ��-��������� </summary>		
        protected static T DummyFactory() => default;
    }

    /// <summary> ������� ���������� ���� ��� GameObject � �������������� ���/���� </summary>
    public class GameObjectPool : Pool<GameObject>
    {
        public GameObjectPool(Func<GameObject> factory) : base(factory) { }
        public GameObjectPool(Func<GameObject> factory, int initialCapacity) : base(factory, initialCapacity) { }
        public GameObjectPool(Func<GameObject> factory, Action<GameObject> reset, int initialCapacity) : base(factory, reset, initialCapacity) { }

        public override GameObject Get(Action<GameObject> resetOverride)
        {
            var element = base.Get(resetOverride);
            element.SetActive(true);
            return element;
        }

        protected override void ReturnToPoolInternal(GameObject element)
        {
            element.SetActive(false);
            base.ReturnToPoolInternal(element);
        }

        protected override GameObject AddNewElement()
        {
            var newElement = base.AddNewElement();
            newElement.SetActive(false);
            return newElement;
        }
    }

    /// <summary> ������� ���������� ���� ��� Unity ����������� � �������������� ���/���� </summary>
    public class UnityComponentPool<T> : Pool<T> where T : Component
    {
        public UnityComponentPool(Func<T> factory) : base(factory) { }
        public UnityComponentPool(Func<T> factory, int initialCapacity) : base(factory, initialCapacity) { }
        public UnityComponentPool(Func<T> factory, Action<T> reset, int initialCapacity) : base(factory, reset, initialCapacity) { }

        public override T Get(Action<T> resetOverride)
        {
            var element = base.Get(resetOverride);
            element.gameObject.SetActive(true);
            return element;
        }

        protected override void ReturnToPoolInternal(T element)
        {
            element.gameObject.SetActive(false);
            base.ReturnToPoolInternal(element);
        }

        protected override T AddNewElement()
        {
            var newElement = base.AddNewElement();
            newElement.gameObject.SetActive(false);
            return newElement;
        }
    }

    /// <summary> ������� ���������� ���� � �������������� �������� GameObject �� ������� </summary>
    public class AutoGameObjectPrefabPool : GameObjectPool
    {
        /// <summary> ������� ����� ���� ������� </summary>
        GameObject PrefabFactory()
        {
            var newElement = Object.Instantiate(_prefab);
            initialize?.Invoke(newElement);
            return newElement;
        }

        protected readonly GameObject _prefab;

        /// <summary> ����� ��� ������������� ������� </summary>
        protected readonly Action<GameObject> initialize;

        public AutoGameObjectPrefabPool(GameObject prefab) : this(prefab, null, null, 0) { }
        public AutoGameObjectPrefabPool(GameObject prefab, int initialCapacity) : this(prefab, null, null, initialCapacity) { }
        public AutoGameObjectPrefabPool(GameObject prefab, Action<GameObject> initialize) : this(prefab, initialize, null, 0) { }
        public AutoGameObjectPrefabPool(GameObject prefab, Action<GameObject> initialize, Action<GameObject> reset) : this(prefab, initialize, reset, 0) { }
        public AutoGameObjectPrefabPool(GameObject prefab, Action<GameObject> initialize, Action<GameObject> reset, int initialCapacity) : base(DummyFactory, reset, 0)
        {
            this.initialize = initialize;

            _prefab = prefab;
            _factory = PrefabFactory;

            if (initialCapacity > 0) Grow(initialCapacity);
        }
    }

    /// <summary> ������� ���������� ���� � �������������� �������� Unity ����������� �� ������� </summary>
    public class AutoComponentPrefabPool<T> : UnityComponentPool<T> where T : Component
    {
        T PrefabFactory()
        {
            var newElement = Object.Instantiate(_prefab);
            initialize?.Invoke(newElement);
            return newElement;
        }

        protected readonly T _prefab;

        /// <summary> ������� ������������� �������� </summary>
        protected readonly Action<T> initialize;

        public AutoComponentPrefabPool(T prefab) : this(prefab, null, null, 0) { }
        public AutoComponentPrefabPool(T prefab, int initialCapacity) : this(prefab, null, null, initialCapacity) { }
        public AutoComponentPrefabPool(T prefab, Action<T> initialize) : this(prefab, initialize, null, 0) { }
        public AutoComponentPrefabPool(T prefab, Action<T> initialize, Action<T> reset) : this(prefab, initialize, reset, 0) { }
        public AutoComponentPrefabPool(T prefab, Action<T> initialize, Action<T> reset, int initialCapacity) : base(DummyFactory, reset, 0)
        {
            this.initialize = initialize;

            _prefab = prefab;
            _factory = PrefabFactory;

            if (initialCapacity > 0) Grow(initialCapacity);
        }
    }
}