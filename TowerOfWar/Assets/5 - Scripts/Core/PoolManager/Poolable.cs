﻿using System.Collections.Generic;
using UnityEngine;

namespace Core.Utilities
{
    public interface IPoolHelper
    {
        List<Poolable> GetPoolables();
    }

    /// <summary> Компонент объекта объединяемого в пул </summary>
    public class Poolable : MonoBehaviour
    {
        [SerializeField] int _initialPoolCapacity = 10;

        public int InitialPoolCapacity => _initialPoolCapacity;

        public Pool<Poolable> Pool { get; private set; }

        public void SetPool(Pool<Poolable> pool) => Pool = pool;

        /// <summary> Вернуть объект в пул </summary>
        protected virtual void Repool()
        {
            transform.SetParent(PoolManager.Instance.transform, false);
            Pool.Return(this);
        }

        #region возврат объекта в пул
        public static void TryPool(GameObject gameObject)
        {
            var poolable = gameObject.GetComponent<Poolable>();

            if (poolable && poolable.Pool != null && PoolManager.InstanceExists)
            {
                poolable.Repool();
            }
            else
            {
                Destroy(gameObject);
            }
        }
        #endregion

        #region возврат объекта в сетевой пул
        public static void TryPoolNet(GameObject gameObject)
        {
            Photon.Pun.PhotonNetwork.Destroy(gameObject);
        }
        #endregion

        #region получение объекта или компонента из пула
        public static T TryGetPoolable<T>(GameObject prefab) where T : Component
        {
            return TryGetPoolable(prefab).GetComponent<T>();
        }

        public static T TryGetPoolable<T>(string prefabName, Vector3 position, Quaternion rotation)
        {
            var instance = PoolManager.Instance.GetPoolable(prefabName);
            instance.transform.position = position;
            instance.transform.rotation = rotation;
            return instance.GetComponent<T>();
        }

        public static GameObject TryGetPoolable(GameObject prefab)
        {
            var poolable = prefab.GetComponent<Poolable>();

            var instance = poolable && PoolManager.InstanceExists ?
                PoolManager.Instance.GetPoolable(poolable).gameObject : Instantiate(prefab);

            return instance;
        }
        #endregion

        #region получение объекта или компонента из сетевого пула
        public static T TryGetPoolableNet<T>(string prefabName, Transform transform, object[] data = null) where T : Component
        {
            return TryGetPoolableNet(prefabName, transform, data).GetComponent<T>();
        }

        public static T TryGetPoolableNet<T>(string prefabName, Vector3 position, Quaternion rotation, object[] data = null) where T : Component
        {
            return TryGetPoolableNet(prefabName, position, rotation, data).GetComponent<T>();
        }

        public static GameObject TryGetPoolableNet(string prefabName, Transform transform, object[] data = null)
        {
            return TryGetPoolableNet(prefabName, transform.position, transform.rotation, data);
        }

        public static GameObject TryGetPoolableNet(string prefabName, Vector3 position, Quaternion rotation, object[] data = null)
        {
            return Photon.Pun.PhotonNetwork.InstantiateSceneObject(prefabName, position, rotation, data: data);
        }
        #endregion
    }
}