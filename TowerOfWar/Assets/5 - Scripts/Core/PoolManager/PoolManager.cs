﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Core.Utilities
{
    /// <summary> Менеджер пулов объектов. Получение и возврат объектов. </summary>
    public class PoolManager : MonoSingleton<PoolManager>
    {
        /// <summary> Список пулов </summary>
        [SerializeField] List<Poolable> _poolables;

        protected Dictionary<Poolable, AutoComponentPrefabPool<Poolable>> _poolDictionary;

        public void Init(List<Poolable> poolables)
        {
            _poolables = poolables;
            _poolDictionary = new Dictionary<Poolable, AutoComponentPrefabPool<Poolable>>();

            foreach (var poolable in _poolables)
            {
                if (poolable == null) continue;

                _poolDictionary.Add(poolable, new AutoComponentPrefabPool<Poolable>(poolable, Initialize, null, poolable.InitialPoolCapacity));
            }
        }


        /// <summary> Получить объект из пула по его имени</summary>
        public Poolable GetPoolable(string prefabName)
        {
            var poolablePrefab = _poolDictionary.Keys.FirstOrDefault(x => x.gameObject.name == prefabName);

            if (!poolablePrefab) throw new ArgumentNullException($"Не найден пул объектов для: {prefabName}");

            var pool = _poolDictionary[poolablePrefab];

            var spawnedInstance = pool.Get();

            spawnedInstance.SetPool(pool);

            return spawnedInstance;
        }

        /// <summary> Получить объект из пула </summary>
        public Poolable GetPoolable(Poolable poolablePrefab)
        {
            if (!_poolDictionary.ContainsKey(poolablePrefab))
            {
                _poolDictionary.Add(poolablePrefab, new AutoComponentPrefabPool<Poolable>(poolablePrefab, Initialize, null, poolablePrefab.InitialPoolCapacity));
            }

            var pool = _poolDictionary[poolablePrefab];
            var spawnedInstance = pool.Get();

            spawnedInstance.SetPool(pool);

            return spawnedInstance;
        }

        /// <summary> Возвращает объект в его пул </summary>
        public void ReturnPoolable(Poolable poolable)
        {
            poolable.transform.SetParent(transform, false);
            poolable.Pool.Return(poolable);
        }

        void Initialize(Component poolable) => poolable.transform.SetParent(transform, false);
    }
}