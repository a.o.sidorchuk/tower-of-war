# Tower of War

Проект создан в рамках задания [Black Bears Coding Challenge 2020](https://docs.google.com/document/d/1954_Vd3EB_elWdR0qhMkM4C-r15z9fgto5id7s1gdc0/edit)

# Инструкция по запуску проекта и сброрке под Android

1. Требуемая версия движка: Unity 2019.3.7f1. Установлены модули: Android Build Support (Android SDK & NDK Tools, OpenJDK)
2. Target Platform при запуске: Android
3. Keystore (файл в корне репозитория) password: idplatform2020
4. Настроить Photon в файле PhotonServerSettings ([скрин настроек](https://i.imgur.com/wHODTVS.png)): - App Id: 74d0bbba-a2b8-4aa1-87b4-38b0f7809df2
5. Открыть окно Addressable (Window -> Asset Management) и выполнить Build -> Build Player Content

# Используемые фреймворки и структура проекта

**Используемые фреймворки и т.п.**

1. Photon Networking
2. UniRx, UniRx.Async
3. Unity Addressable Asset System
4. Google Play Games

**Структура папок проекта:**

1. Art - Все визуальные и звуковые ассеты (модели, анимации, спрайты, текстуры, звуки и музыка);
2. Data - Игровые данные созданные на основе **Scriptable Object** (юниты, способности, их дополнительные эффекты, а также информация обо всех сценах);
3. Prefabs - Все игровые шаблоны (юниты, UI, управление сценой и т.д.);
4. Scenes - Игровые сцены
5. Scripts - Собраны все сценарии. Сценарии разделены несколькими **Assembly Definition** в отдельные подпроекты;

| Assembly Definition | Описание |
| ------ | ------ |
| Core | Базовый функционал. Содержит сценарии и базовые реализации для локального пула объектов, игрового менеджера и store, обертку для Photon Network, систему пользовательского ввода и т.п. |
| Data | Содержит Scriptable Object модели данных. Среди которых: юниты, способности, эффекты, сцены | 
| Game | Все игровые сценарии по подразделам: юниты, игровой менеджер, управление сценой, сетью, эффектами, звуком и т.д. | 
| ActionGameFramework | Фреймворк для реализации стрельбы, повреждений и т.п. |

6. _Fonts - Шрифты используемые в игре
7. _Settings - Хранятся общие настройки и пресеты
8. Другие папки автоматически сгенерированы при импортировании ассетов и инструменов в проект.